/*
 * membrane.c
 *
 *   Copyright (c) 2008, Ueda Laboratory LMNtal Group <lmntal@ueda.info.waseda.ac.jp>
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are
 *   met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *
 *    3. Neither the name of the Ueda Laboratory LMNtal Groupy LMNtal
 *       Group nor the names of its contributors may be used to
 *       endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id: membrane.c,v 1.34 2008/10/16 18:12:27 sasaki Exp $
 */

#include "membrane.h"
#include "atom.h"
#include "rule.h"
#include "dumper.h" /* for debug */
#include "functor.h"
#include "st.h"
#include "mhash.h"
#include "error.h"
#include "util.h"
#include "visitlog.h"
#include <ctype.h>
#include <limits.h>

#ifdef PROFILE
#  include "runtime_status.h"
#endif

static void lmn_mem_copy_cells_sub(LmnMembrane *destmem,
                                   LmnMembrane *srcmem,
                                   ProcessTbl  atoms,
                                   BOOL        hl_nd);

/* 繝ｫ繝ｼ繝ｫ繧ｻ繝�繝�add_rs繧偵Ν繝ｼ繝ｫ繧ｻ繝�繝磯�榊�耀rc_v縺ｸ霑ｽ蜉�縺吶ｋ.
 * 繧ｰ繝ｩ繝募酔蝙区�ｧ蛻､螳壼�ｦ逅�縺ｪ縺ｩ縺ｮ縺溘ａ縺ｫ謨ｴ謨ｰID縺ｮ譏�鬆�繧堤ｶｭ謖√☆繧九ｈ縺�霑ｽ蜉�縺吶ｋ. */
void lmn_mem_add_ruleset_sort(Vector *src_v, LmnRuleSet add_rs)
{
  int i, j, n;
  LmnRulesetId add_id;
  add_id = lmn_ruleset_get_id(add_rs);
  n = vec_num(src_v);
  for (i = 0; i < n; i++) {
    LmnRuleSet rs_i;
    LmnRulesetId dst_id;

    rs_i   = (LmnRuleSet)vec_get(src_v, i);
    dst_id = lmn_ruleset_get_id(rs_i);

    if (dst_id == add_id && !lmn_ruleset_has_uniqrule(add_rs)) {
      /* 蜷後§髫主ｱ､縺ｫuniq縺ｧ縺ｪ縺�蜷御ｸ�縺ｮ繝ｫ繝ｼ繝ｫ繧ｻ繝�繝医′譌｢縺ｫ蟄伜惠縺吶ｋ縺ｪ繧峨�ｰ霑ｽ蜉�縺吶ｋ蠢�隕√�ｯ縺ｪ縺� */
      break;
    } else if (dst_id >= add_id) {
      LmnRuleSet prev = add_rs;
      vec_push(src_v, 0);

      for (j = i; j < (n + 1); j++) {
        LmnRuleSet tmp = (LmnRuleSet)vec_get(src_v, j);
        vec_set(src_v, j, (LmnWord)prev);
        prev = tmp;
      }
      break;
    }
  }
  if (i == n) {
    vec_push(src_v, (LmnWord)add_rs);
  }
}


/*----------------------------------------------------------------------
 * Atom Set
 */

static inline AtomListEntry *make_atomlist(void);
static inline void free_atomlist(AtomListEntry *as);

/* 譁ｰ縺励＞繧｢繝医Β繝ｪ繧ｹ繝医ｒ菴懊ｋ */
static inline AtomListEntry *make_atomlist()
{
  AtomListEntry *as = LMN_MALLOC(struct AtomListEntry);
  as->record = NULL; /* 蜈ｨ縺ｦ縺ｮ繧｢繝医Β縺ｮ遞ｮ鬘槭↓蟇ｾ縺励※findatom2逕ｨ繝上ャ繧ｷ繝･陦ｨ縺悟ｿ�隕√↑繧上￠縺ｧ縺ｯ縺ｪ縺�縺ｮ縺ｧ蜍慕噪縺ｫmalloc縺輔○繧� */
  atomlist_set_empty(as);

  return as;
}

/* 繧｢繝医Β繝ｪ繧ｹ繝医�ｮ隗｣謾ｾ蜃ｦ逅� */
static inline void free_atomlist(AtomListEntry *as)
{
  /* lmn_mem_move_cells縺ｧ繧｢繝医Β繝ｪ繧ｹ繝医�ｮ蜀榊茜逕ｨ繧定｡後▲縺ｦ縺�縺ｦ
   * 繝昴う繝ｳ繧ｿ縺君ULL縺ｫ縺ｪ繧句�ｴ蜷医′縺ゅｋ縺ｮ縺ｧ縲∵､懈渊繧定｡後≧蠢�隕√′縺ゅｋ縲�*/
  if (as) {
    if (as->record) {
      hashtbl_free(as->record);
    }
    LMN_FREE(as);
  }
}


/*----------------------------------------------------------------------
 * Membrane
 */

LmnMembrane *lmn_mem_make(void)
{
  LmnMembrane *mem;

  mem = LMN_MALLOC(LmnMembrane);
  mem->parent        =  NULL;
  mem->child_head    =  NULL;
  mem->prev          =  NULL;
  mem->next          =  NULL;
  mem->max_functor   =  0U;
  mem->atomset_size  =  32;
  mem->is_activated  =  TRUE;
  mem->atom_symb_num =  0U;
  mem->atom_data_num =  0U;
  mem->name          =  ANONYMOUS;
  mem->id            =  0UL;
#ifdef TIME_OPT
  mem->atomset       =  LMN_CALLOC(struct AtomListEntry *, mem->atomset_size);
#else
  hashtbl_init(&mem->atomset, mem->atomset_size);
#endif
  vec_init(&mem->rulesets, 1);
  lmn_mem_set_id(mem, env_gen_next_id());


  return mem;
}




/* 閹徇em縺ｮ隗｣謾ｾ繧定｡後≧.
 * 閹徇em縺ｫ謇�螻槭☆繧句ｭ占�懊→繧｢繝医Β縺ｮ繝｡繝｢繝ｪ邂｡逅�縺ｯ蜻ｼ縺ｳ蜃ｺ縺怜�ｴ縺ｧ陦後≧. */
void lmn_mem_free(LmnMembrane *mem)
{
  AtomListEntry *ent;

  /* free all atomlists  */
  EACH_ATOMLIST(mem, ent, ({
    free_atomlist(ent);
  }));

  lmn_mem_rulesets_destroy(&mem->rulesets);
#ifdef TIME_OPT
  env_return_id(lmn_mem_id(mem));
  LMN_FREE(mem->atomset);
#else
  hashtbl_destroy(&mem->atomset);
#endif
  LMN_FREE(mem);
}


/* 閹徇em蜀�縺ｮ繧｢繝医Β, 蟄占��, 繝ｫ繝ｼ繝ｫ繧ｻ繝�繝医�ｮ繝｡繝｢繝ｪ繧定ｧ｣謾ｾ縺吶ｋ.
 * 蟄占�懊′蟄伜惠縺吶ｋ蝣ｴ蜷医�ｯ, 縺昴�ｮ蟄占�懊↓蜀榊ｸｰ縺吶ｋ. */
void lmn_mem_drop(LmnMembrane *mem)
{
  AtomListEntry *ent;
  LmnMembrane *m, *n;

  /* drop and free child mems */
  m = mem->child_head;
  while (m) {
    n = m;
    m = m->next;
    lmn_mem_free_rec(n);
  }
  mem->child_head = NULL;

  EACH_ATOMLIST(mem, ent, ({
    LmnSAtom a;
    //printf("step1 ok\n");
    a = atomlist_head(ent);
    //printf("step2 ok\n");
    if (LMN_IS_HL(a)) {
      continue; /* hyperlink縺ｯbuddy symbol atom縺ｨ荳�邱偵↓蜑企勁縺輔ｌ繧九◆繧� */
    }
    //printf("step3 ok\n");
    while (a != lmn_atomlist_end(ent)) {
      LmnSAtom b = a;
      a = LMN_SATOM_GET_NEXT_RAW(a);
      free_symbol_atom_with_buddy_data(b);
    }
    atomlist_set_empty(ent);
    //printf("step4 ok\n");
  }));

  mem->atom_symb_num =  0U;
  mem->atom_data_num =  0U;
}


/* 繝ｫ繝ｼ繝ｫ繧ｻ繝�繝磯�榊�羊ulesets繧定ｧ｣謾ｾ縺吶ｋ */
void lmn_mem_rulesets_destroy(Vector *rulesets)
{
  unsigned int i, n = vec_num(rulesets);

  for (i = 0; i < n; i++) {
    LmnRuleSet rs = (LmnRuleSet)vec_get(rulesets, i);

    if (lmn_ruleset_is_copy(rs)) {
      lmn_ruleset_copied_free(rs);
    }
  }
  vec_destroy(rulesets);
}

void move_symbol_atom_to_atomlist_tail(LmnSAtom a, LmnMembrane *mem){
  LmnFunctor f = LMN_SATOM_GET_FUNCTOR(a);
  AtomListEntry *ent = lmn_mem_get_atomlist(mem, f);

  LMN_SATOM_SET_PREV(LMN_SATOM_GET_NEXT_RAW(a), LMN_SATOM_GET_PREV(a));
  LMN_SATOM_SET_NEXT(LMN_SATOM_GET_PREV(a),     LMN_SATOM_GET_NEXT_RAW(a));

  LMN_SATOM_SET_NEXT(a, ent);
  LMN_SATOM_SET_PREV(a, ent->tail);
  LMN_SATOM_SET_NEXT(ent->tail, a);
  ent->tail = (LmnWord)a;
}

void move_symbol_atom_to_atomlist_head(LmnSAtom a, LmnMembrane *mem){
  LmnFunctor f = LMN_SATOM_GET_FUNCTOR(a);
  AtomListEntry *ent = lmn_mem_get_atomlist(mem, f);

  LMN_SATOM_SET_PREV(LMN_SATOM_GET_NEXT_RAW(a), LMN_SATOM_GET_PREV(a));
  LMN_SATOM_SET_NEXT(LMN_SATOM_GET_PREV(a),     LMN_SATOM_GET_NEXT_RAW(a));


  LMN_SATOM_SET_NEXT(a, ent->head);
  LMN_SATOM_SET_PREV(a, ent);
  LMN_SATOM_SET_PREV(ent->head, a);
  ent->head = (LmnWord)a;
}
void move_symbol_atomlist_to_atomlist_tail(LmnSAtom a, LmnMembrane *mem){
  LmnFunctor f = LMN_SATOM_GET_FUNCTOR(a);
  AtomListEntry *ent = lmn_mem_get_atomlist(mem, f);
  
  if(!((LmnWord)a == ent->head)){
    LMN_SATOM_SET_NEXT(ent->tail, ent->head);
    LMN_SATOM_SET_PREV(ent->head, ent->tail);
    ent->tail = (LmnWord)LMN_SATOM_GET_PREV(a);
    LMN_SATOM_SET_NEXT(LMN_SATOM_GET_PREV(a), ent);
    LMN_SATOM_SET_PREV(a,ent);
    ent->head = (LmnWord)a;
    }
}
void move_symbol_atom_to_atom_tail(LmnSAtom a, LmnSAtom a1, LmnMembrane *mem){
  LmnFunctor f = LMN_SATOM_GET_FUNCTOR(a);
  AtomListEntry *ent = lmn_mem_get_atomlist(mem, f);
  
  if(ent->tail == (LmnWord)a1)ent->tail = (LmnWord)LMN_SATOM_GET_PREV(a1);
  else if(ent->head == (LmnWord)a1)ent->head = (LmnWord)LMN_SATOM_GET_NEXT_RAW(a1);

  LMN_SATOM_SET_PREV(LMN_SATOM_GET_NEXT_RAW(a1), LMN_SATOM_GET_PREV(a1));
  LMN_SATOM_SET_NEXT(LMN_SATOM_GET_PREV(a1),     LMN_SATOM_GET_NEXT_RAW(a1));

  if(ent->tail == (LmnWord)LMN_SATOM_GET_NEXT_RAW(a))ent->tail=(LmnWord)a1;

  LMN_SATOM_SET_PREV(LMN_SATOM_GET_NEXT_RAW(a), a1);
  LMN_SATOM_SET_NEXT(a1, LMN_SATOM_GET_NEXT_RAW(a));
  LMN_SATOM_SET_PREV(a1, a);
  LMN_SATOM_SET_NEXT(a, a1);
  
}

void mem_push_symbol_atom(LmnMembrane *mem, LmnSAtom atom)
{
  AtomListEntry *as;
  LmnFunctor f = LMN_SATOM_GET_FUNCTOR(atom);

  if (LMN_SATOM_ID(atom) == 0) { /* 閹懊↓push縺励◆縺ｪ繧峨�ｰid繧貞牡繧雁ｽ薙※繧� */
    LMN_SATOM_SET_ID(atom, env_gen_next_id());
  }

  as = lmn_mem_get_atomlist(mem, f);
  if (!as) { /* 譛ｬ閹懷��縺ｫ蛻昴ａ縺ｦ繧｢繝医Βatom縺訓USH縺輔ｌ縺溷�ｴ蜷� */
#ifdef TIME_OPT
    LMN_ASSERT(mem->atomset); /* interpreter蛛ｴ縺ｧ蛟､縺後が繝ｼ繝舌�ｼ繝輔Ο繝ｼ縺吶ｋ縺ｨ逋ｺ逕溘☆繧九�ｮ縺ｧ, 縺薙％縺ｧ豁｢繧√ｋ */
    if (mem->max_functor < f + 1)
    {
      mem->max_functor = f + 1;
      while (mem->atomset_size - 1 < mem->max_functor)
      {
        int org_size = mem->atomset_size;
        mem->atomset_size *= 2;
        LMN_ASSERT(mem->atomset_size > 0);
        mem->atomset = LMN_REALLOC(struct AtomListEntry*, mem->atomset, mem->atomset_size);
        memset(mem->atomset + org_size, 0, (mem->atomset_size - org_size) * sizeof(struct AtomListEntry *));
      }
    }
    as = mem->atomset[f] = make_atomlist();
#else
    as = make_atomlist();
    hashtbl_put(&mem->atomset, (HashKeyType)f, (HashValueType)as);
#endif
  }

  if (LMN_IS_PROXY_FUNCTOR(f))
  {
    LMN_PROXY_SET_MEM(atom, mem);
  }
  else if (LMN_FUNC_IS_HL(f))
  {
    LMN_HL_MEM(lmn_hyperlink_at_to_hl(atom)) = mem;
    lmn_mem_symb_atom_inc(mem);
  }
  else if (f != LMN_UNIFY_FUNCTOR)
  {
    /* symbol atom except proxy and unify */
    lmn_mem_symb_atom_inc(mem);
  }

  push_to_atomlist(atom, as);
}


unsigned long lmn_mem_space(LmnMembrane *mem)
{
  AtomListEntry *ent;
  unsigned long ret;
  unsigned int i;

  ret = 0;
  ret += sizeof(struct LmnMembrane);
#ifdef TIME_OPT
  ret += sizeof(struct AtomListEntry*) * mem->atomset_size;
#else
  ret += internal_hashtbl_space_inner(&mem->atomset);
#endif
  /* atomset */
  EACH_ATOMLIST(mem, ent, ({
    LmnSAtom atom;
    ret += sizeof(struct AtomListEntry);
    if (ent->record) {
      ret += internal_hashtbl_space(ent->record);
    }
    EACH_ATOM(atom, ent, ({
      ret += sizeof(LmnWord) * LMN_SATOM_WORDS(LMN_FUNCTOR_ARITY(LMN_SATOM_GET_FUNCTOR(atom)));
    }));
  }));

  /* ruleset */
  ret += vec_space_inner(&mem->rulesets);
  for (i = 0; i < vec_num(&mem->rulesets); i++) {
    LmnRuleSet rs = (LmnRuleSet)vec_get(&mem->rulesets, i);
    if (lmn_ruleset_is_copy(rs)) {
      ret += lmn_ruleset_space(rs);
    }
  }

  return ret;
}

unsigned long lmn_mem_root_space(LmnMembrane *src)
{
  unsigned long ret;
  LmnMembrane *ptr;

  ret = lmn_mem_space(src);
  for (ptr = src->child_head; ptr != NULL; ptr = ptr->next) {
    ret += lmn_mem_root_space(ptr);
  }

  return ret;
}


void lmn_mem_link_data_atoms(LmnMembrane *mem,
                             LmnAtom d0,
                             LmnLinkAttr attr0,
                             LmnAtom d1,
                             LmnLinkAttr attr1)
{
  LmnSAtom ap = lmn_new_atom(LMN_UNIFY_FUNCTOR);

  LMN_SATOM_SET_LINK(ap, 0, d0);
  LMN_SATOM_SET_LINK(ap, 1, d1);
  LMN_SATOM_SET_ATTR(ap, 0, attr0);
  LMN_SATOM_SET_ATTR(ap, 1, attr1);
  mem_push_symbol_atom(mem, ap);
}

/* atom1, atom2繧偵す繝ｳ繝懊Ν繧｢繝医Β縺ｫ髯仙ｮ壹＠縺� unify link */
void lmn_mem_unify_symbol_atom_args(LmnSAtom atom1, int pos1,
                                    LmnSAtom atom2, int pos2)
{
  LmnAtom ap1, ap2;
  LmnLinkAttr attr1, attr2;

  ap1   = LMN_SATOM_GET_LINK(atom1, pos1);
  attr1 = LMN_SATOM_GET_ATTR(atom1, pos1);
  ap2   = LMN_SATOM_GET_LINK(atom2, pos2);
  attr2 = LMN_SATOM_GET_ATTR(atom2, pos2);

  LMN_SATOM_SET_LINK(ap2, attr2, ap1);
  LMN_SATOM_SET_ATTR(ap2, attr2, attr1);
  LMN_SATOM_SET_LINK(ap1, attr1, ap2);
  LMN_SATOM_SET_ATTR(ap1, attr1, attr2);
}

/* atom1, atom2縺ｯ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺ｮ縺ｯ縺� */
void lmn_mem_unify_atom_args(LmnMembrane *mem,
                             LmnSAtom atom1, int pos1,
                             LmnSAtom atom2, int pos2)
{
  LmnAtom ap1, ap2;
  LmnLinkAttr attr1, attr2;

  ap1   = LMN_SATOM_GET_LINK(atom1, pos1);
  attr1 = LMN_SATOM_GET_ATTR(atom1, pos1);
  ap2   = LMN_SATOM_GET_LINK(atom2, pos2);
  attr2 = LMN_SATOM_GET_ATTR(atom2, pos2);

  if (LMN_ATTR_IS_DATA(attr1) && LMN_ATTR_IS_DATA(attr2)) {
    lmn_mem_link_data_atoms(mem, ap1, attr1, ap2, attr2);
  }
  else if (LMN_ATTR_IS_DATA(attr1)) {
    LMN_SATOM_SET_LINK(ap2, attr2, ap1);
    LMN_SATOM_SET_ATTR(ap2, attr2, attr1);

    /*below two line: alimjan yasin 2016-3-8*/
    LMN_SATOM_SET_LINK(ap1, 0, ap2);
    LMN_SATOM_SET_ATTR(ap1, 0, attr2);
  }
  else if (LMN_ATTR_IS_DATA(attr2)) {
    LMN_SATOM_SET_LINK(ap1, attr1, ap2);
    LMN_SATOM_SET_ATTR(ap1, attr1, attr2);

    /*below two line: alimjan yasin 2016-3-8*/
    LMN_SATOM_SET_LINK(ap2, 0, ap1);
    LMN_SATOM_SET_ATTR(ap2, 0, attr1);
  }
  else {
    LMN_SATOM_SET_LINK(ap2, attr2, ap1);
    LMN_SATOM_SET_ATTR(ap2, attr2, attr1);
    LMN_SATOM_SET_LINK(ap1, attr1, ap2);
    LMN_SATOM_SET_ATTR(ap1, attr1, attr2);
  }
}

/* 繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺ｫ髯仙ｮ壹＠縺殤ewlink */
void lmn_newlink_in_symbols(LmnSAtom atom0, int pos0,
                            LmnSAtom atom1, int pos1)
{
  LMN_SATOM_SET_LINK(atom0, pos0, atom1);
  LMN_SATOM_SET_LINK(atom1, pos1, atom0);
  LMN_SATOM_SET_ATTR(atom0, pos0, LMN_ATTR_MAKE_LINK(pos1));
  LMN_SATOM_SET_ATTR(atom1, pos1, LMN_ATTR_MAKE_LINK(pos0));
}


void lmn_newlink_with_ex(LmnMembrane *mem,
                         LmnSAtom atom0, LmnLinkAttr attr0, int pos0,
                         LmnSAtom atom1, LmnLinkAttr attr1, int pos1)
{
  /* both symbol */
  LMN_SATOM_SET_LINK(atom0, pos0, atom1);
  LMN_SATOM_SET_LINK(atom1, pos1, atom0);

  if (LMN_ATTR_IS_EX(attr0)) {
    if (LMN_ATTR_IS_EX(attr1)) { /* 0, 1 are ex */
//      LMN_SATOM_SET_ATTR(atom0, pos0, attr1);
//      LMN_SATOM_SET_ATTR(atom1, pos1, attr0);
      /* 迴ｾ迥ｶ縺ｧ縺ｯ縲”yperlink繧｢繝医Β蜷悟｣ｫ縺梧磁邯壹＆繧後ｋ縺ｨ豸亥悉縺輔ｌ繧� */
      //      lmn_mem_delete_atom(mem, LMN_ATOM(atom0), attr0);
      //      lmn_mem_delete_atom(mem, LMN_ATOM(atom1), attr1);
      lmn_fatal("Two hyperlinks cannot be connected using = .");
    } else { /* 0 is ex */
      LMN_SATOM_SET_ATTR(atom0, pos0, LMN_ATTR_MAKE_LINK(pos1));
      LMN_SATOM_SET_ATTR(atom1, pos1, attr0);
    }
  }
  else { /* 1 is ex */
    LMN_SATOM_SET_ATTR(atom0, pos0, attr1);
    LMN_SATOM_SET_ATTR(atom1, pos1, LMN_ATTR_MAKE_LINK(pos0));
  }

}

/* 繧ｷ繝ｳ繝懊Ν繧｢繝医Βatom0縺ｨ縲√す繝ｳ繝懊Νor繝�繝ｼ繧ｿ繧｢繝医Βatom1縺ｮ髢薙↓繝ｪ繝ｳ繧ｯ繧貞ｼｵ繧九��
 * 縺薙�ｮ繧ｳ繝ｼ繝峨′驥崎､�縺励※迴ｾ繧後◆縺ｮ縺ｧ縲�髢｢謨ｰ縺ｫ蛻�蜑ｲ縺励◆ */
static inline void newlink_symbol_and_something(LmnSAtom atom0,
                                                int pos,
                                                LmnAtom atom1,
                                                LmnLinkAttr attr)
{
  LMN_SATOM_SET_LINK(atom0, pos, atom1);
  LMN_SATOM_SET_ATTR(atom0, pos, attr);
  if (!LMN_ATTR_IS_DATA(attr)) {
    LMN_SATOM_SET_LINK(LMN_SATOM(atom1), LMN_ATTR_GET_VALUE(attr), atom0);
    LMN_SATOM_SET_ATTR(LMN_SATOM(atom1), LMN_ATTR_GET_VALUE(attr), LMN_ATTR_MAKE_LINK(pos));
  }
}

/* 繧ｷ繝ｳ繝懊Ν繧｢繝医Βatom縺ｨ縲√ワ繧､繝代�ｼ繝ｪ繝ｳ繧ｯ繧｢繝医ΒhlAtom縺ｮ髢薙↓繝ｪ繝ｳ繧ｯ繧貞ｼｵ繧九��
 * 繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｯ謗･邯壹�ｮ蠑墓焚縺�0縺�縺｣縺溘ｊ縺ｨ迚ｹ谿翫↑縺ｮ縺ｧ髢｢謨ｰ繧ら畑諢上＠縺溘��*/
static inline void newlink_symbol_and_hlink(LmnSAtom atom,
                                            int pos,
                                            LmnSAtom hlAtom)
{
  LMN_SATOM_SET_LINK(atom, pos, hlAtom);
  LMN_SATOM_SET_LINK(hlAtom, 0, atom);
}


void lmn_mem_newlink(LmnMembrane *mem,
                     LmnAtom atom0, LmnLinkAttr attr0, int pos0,
                     LmnAtom atom1, LmnLinkAttr attr1, int pos1)
{
  if (LMN_ATTR_IS_DATA_WITHOUT_EX(attr0)) {
    if (LMN_ATTR_IS_DATA_WITHOUT_EX(attr1)) { /* both data */
      lmn_mem_link_data_atoms(mem, atom0, attr0, atom1, attr1);
    }
    else { /* atom0 data, atom1 symbol */
      LMN_SATOM_SET_LINK(LMN_SATOM(atom1), pos1, atom0);
      LMN_SATOM_SET_ATTR(LMN_SATOM(atom1), pos1, attr0);
    }
  }
  else if (LMN_ATTR_IS_DATA_WITHOUT_EX(attr1)) { /* atom0 symbol, atom1 data */
    LMN_SATOM_SET_LINK(LMN_SATOM(atom0), pos0, atom1);
    LMN_SATOM_SET_ATTR(LMN_SATOM(atom0), pos0, attr1);
  }
  else { /* both symbol */
    if (!LMN_ATTR_IS_EX(attr0) && !LMN_ATTR_IS_EX(attr1))
      lmn_newlink_in_symbols(LMN_SATOM(atom0), pos0, LMN_SATOM(atom1), pos1);
    else
      lmn_newlink_with_ex(mem, LMN_SATOM(atom0), attr0, pos0, LMN_SATOM(atom1), attr1, pos1);
  }
}


void lmn_relink_symbols(LmnSAtom atom0, int pos0,
                        LmnSAtom atom1, int pos1)
{
  newlink_symbol_and_something(LMN_SATOM(atom0),
                               pos0,
                               LMN_SATOM_GET_LINK(LMN_SATOM(atom1), pos1),
                               LMN_SATOM_GET_ATTR(LMN_SATOM(atom1), pos1));
}


void lmn_mem_relink_atom_args(LmnMembrane *mem,
                              LmnAtom atom0, LmnLinkAttr attr0, int pos0,
                              LmnAtom atom1, LmnLinkAttr attr1, int pos1)
{
  /* TODO: relink縺ｧ縺ｯatom0,atom1縺後ョ繝ｼ繧ｿ縺ｫ縺ｪ繧九％縺ｨ縺ｯ縺ｪ縺�縺ｯ縺�
   *       縺薙�ｮ縺薙→繧堤｢ｺ隱阪☆繧� */
  LMN_ASSERT(!LMN_ATTR_IS_DATA(attr0) &&
             !LMN_ATTR_IS_DATA(attr1));

  newlink_symbol_and_something(LMN_SATOM(atom0),
                               pos0,
                               LMN_SATOM_GET_LINK(LMN_SATOM(atom1), pos1),
                               LMN_SATOM_GET_ATTR(LMN_SATOM(atom1), pos1));
}

void lmn_mem_move_cells(LmnMembrane *destmem, LmnMembrane *srcmem)
{
  AtomListEntry *srcent;
  LmnMembrane *m, *next;
  int dst_data_atom_n, src_data_atom_n;

  dst_data_atom_n = lmn_mem_data_atom_num(destmem);
  src_data_atom_n = lmn_mem_data_atom_num(srcmem);

  /* move atoms */
  EACH_ATOMLIST(srcmem, srcent, ({
    LmnSAtom a, next;

    for (a  = atomlist_head((srcent));
         a != lmn_atomlist_end((srcent));
         a  = next) {
      next = LMN_SATOM_GET_NEXT_RAW(a);
      if (LMN_SATOM_GET_FUNCTOR((a)) != LMN_RESUME_FUNCTOR) {
        int i, arity;

        mem_remove_symbol_atom(srcmem, a);
        mem_push_symbol_atom(destmem, a);
        arity = LMN_SATOM_GET_LINK_NUM(a);
        for (i = 0; i < arity; i++) {
          if (LMN_ATTR_IS_DATA_WITHOUT_EX(LMN_SATOM_GET_ATTR(a, i))) {
            lmn_mem_push_atom(destmem,
                              LMN_SATOM_GET_LINK(a, i),
                              LMN_SATOM_GET_ATTR(a, i));
          }
        }
      }
    }
  }));

  /* move membranes */
  for (m = srcmem->child_head; m; m = next) {
    next = m->next;
    lmn_mem_remove_mem(srcmem, m);
    lmn_mem_add_child_mem(destmem, m);
  }

  if (src_data_atom_n > lmn_mem_data_atom_num(destmem) - dst_data_atom_n) {
    lmn_mem_data_atom_set(destmem, dst_data_atom_n + src_data_atom_n);
  }
}

//#define REMOVE            1
//#define STATE(ATOM)        (LMN_SATOM_GET_ATTR((ATOM), 2))
//#define SET_STATE(ATOM,S)  (LMN_SATOM_SET_ATTR((ATOM), 2, (S)))

/* cf. Java蜃ｦ逅�邉ｻ
 * TODO:
 * 縺ｨ縺ｦ繧る撼蜉ｹ邇�縺ｪ縺ｮ縺ｧ�ｼ御ｻ･蜑阪�ｮREMOVE繧ｿ繧ｰ繧剃ｽｿ縺｣縺溷ｮ溯｣�縺ｫ謌ｻ縺吶°
 * HashSet繧剃ｽｿ縺�繧医≧縺ｫ縺吶ｋ
 *
 * 2011/01/23  蜃ｦ逅�繧貞､画峩 meguro
 */
void lmn_mem_remove_proxies(LmnMembrane *mem)
{
  struct Vector remove_list_p, remove_list_m, change_list;
  AtomListEntry *ent;
  unsigned int i;

  ent = lmn_mem_get_atomlist(mem, LMN_IN_PROXY_FUNCTOR);

  vec_init(&remove_list_p, 16); /* parent逕ｨ */
  vec_init(&remove_list_m, 16); /* mem逕ｨ */
  vec_init(&change_list, 16);

  if (ent) {
    LmnSAtom ipxy;

    EACH_ATOM(ipxy, ent, ({
      LmnSAtom a0, a1;
      LmnFunctor f0, f1;

      if (!LMN_ATTR_IS_DATA(LMN_SATOM_GET_ATTR(ipxy, 1))) {
        a0 = LMN_SATOM(LMN_SATOM_GET_LINK(ipxy, 1));
        f0 = LMN_SATOM_GET_FUNCTOR(a0);

        if (f0 == LMN_STAR_PROXY_FUNCTOR) {
          /* -$*-$in- 竊� ----- */
          lmn_mem_unify_atom_args(mem, a0, 0, ipxy, 0);
          vec_push(&remove_list_m, (LmnWord)a0);
          vec_push(&remove_list_m, (LmnWord)ipxy);
        } else {
          /* -$in- 竊� -$*- */
          vec_push(&change_list, (LmnWord)ipxy);
        }
      }
      else {
        /* -$in- 竊� -$*- */
        vec_push(&change_list, (LmnWord)ipxy);
      }

      a1 = LMN_SATOM(LMN_SATOM_GET_LINK(ipxy, 0));
      f1 = LMN_SATOM_GET_FUNCTOR(a1);

      if (f1 == LMN_OUT_PROXY_FUNCTOR) {
        if (!LMN_ATTR_IS_DATA(LMN_SATOM_GET_ATTR(a1, 1))) {
          LmnSAtom a2;
          LmnFunctor f2;

          a2 = LMN_SATOM(LMN_SATOM_GET_LINK(a1, 1));
          f2 = LMN_SATOM_GET_FUNCTOR(a2);

          if (f2 == LMN_STAR_PROXY_FUNCTOR) {
            lmn_mem_unify_atom_args(mem->parent, a1, 0, a2, 0);
            vec_push(&remove_list_p, (LmnWord)a1);
            vec_push(&remove_list_p, (LmnWord)a2);
          }
          else {
            vec_push(&change_list, (LmnWord)a1);
          }
        }
      }
    }));
  }

  for (i = 0; i < vec_num(&remove_list_p); i++) {
    mem_remove_symbol_atom(mem->parent, LMN_SATOM(vec_get(&remove_list_p, i)));
    lmn_delete_atom(LMN_SATOM(vec_get(&remove_list_p, i)));
  }
  vec_destroy(&remove_list_p);

  for (i = 0; i < vec_num(&remove_list_m); i++) {
    mem_remove_symbol_atom(mem, LMN_SATOM(vec_get(&remove_list_m, i)));
    lmn_delete_atom(LMN_SATOM(vec_get(&remove_list_m, i)));
  }
  vec_destroy(&remove_list_m);

  /* change to star proxy */
  for (i = 0; i < change_list.num; i++) {
    alter_functor(LMN_PROXY_GET_MEM(LMN_SATOM(vec_get(&change_list, i))),
                  LMN_SATOM(vec_get(&change_list, i)), LMN_STAR_PROXY_FUNCTOR);
  }
  vec_destroy(&change_list);
}



/* cf. Java蜃ｦ逅�邉ｻ */
/*
 * TODO:
 * 縺ｨ縺ｦ繧る撼蜉ｹ邇�縺ｪ縺ｮ縺ｧ�ｼ御ｻ･蜑阪�ｮREMOVE繧ｿ繧ｰ繧剃ｽｿ縺｣縺溷ｮ溯｣�縺ｫ謌ｻ縺吶°
 * HashSet繧剃ｽｿ縺�繧医≧縺ｫ縺吶ｋ
 */
void lmn_mem_insert_proxies(LmnMembrane *mem, LmnMembrane *child_mem)
{
  unsigned int i;
  Vector remove_list, change_list;
  LmnSAtom star, oldstar;
  AtomListEntry *ent = lmn_mem_get_atomlist(child_mem, LMN_STAR_PROXY_FUNCTOR);

  if (!ent) return;

  vec_init(&remove_list, 16);
  vec_init(&change_list, 16); /* inside proxy 縺ｫ縺吶ｋ繧｢繝医Β */

  EACH_ATOM(star, ent, ({
    oldstar = LMN_SATOM(LMN_SATOM_GET_LINK(star, 0));
    if (LMN_PROXY_GET_MEM(oldstar) == child_mem) { /* (1) */
      if (!vec_contains(&remove_list, (LmnWord)star)) {
        lmn_mem_unify_atom_args(child_mem, star, 1, oldstar, 1);
        vec_push(&remove_list, (LmnWord)star);
        vec_push(&remove_list, (LmnWord)oldstar);
      }
    }
    else {
      vec_push(&change_list, (LmnWord)star);

      if (LMN_PROXY_GET_MEM(oldstar) == mem) { /* (2) */
        alter_functor(mem, oldstar, LMN_OUT_PROXY_FUNCTOR);
        lmn_newlink_in_symbols(star, 0, oldstar, 0);
      } else { /* (3) */
        LmnSAtom outside = lmn_mem_newatom(mem, LMN_OUT_PROXY_FUNCTOR);
        LmnSAtom newstar = lmn_mem_newatom(mem, LMN_STAR_PROXY_FUNCTOR);
        lmn_newlink_in_symbols(outside, 1, newstar, 1);
        lmn_mem_relink_atom_args(mem,
                                 LMN_ATOM(newstar),
                                 LMN_ATTR_MAKE_LINK(0),
                                 0,
                                 LMN_ATOM(star),
                                 LMN_ATTR_MAKE_LINK(0),
                                 0);
        lmn_newlink_in_symbols(star, 0, outside, 0);
      }
    }
  }));

  for (i = 0; i < vec_num(&change_list); i++) {
    alter_functor(child_mem, LMN_SATOM(vec_get(&change_list, i)), LMN_IN_PROXY_FUNCTOR);
  }
  vec_destroy(&change_list);

  for (i = 0; i < vec_num(&remove_list); i++) {
    mem_remove_symbol_atom(mem, LMN_SATOM(vec_get(&remove_list, i)));
    lmn_delete_atom(LMN_SATOM(vec_get(&remove_list, i)));
  }
  vec_destroy(&remove_list);
}

/* cf. Java蜃ｦ逅�邉ｻ */
/*
 * TODO:
 * 縺ｨ縺ｦ繧る撼蜉ｹ邇�縺ｪ縺ｮ縺ｧ�ｼ御ｻ･蜑阪�ｮREMOVE繧ｿ繧ｰ繧剃ｽｿ縺｣縺溷ｮ溯｣�縺ｫ謌ｻ縺吶°
 * HashSet繧剃ｽｿ縺�繧医≧縺ｫ縺吶ｋ
 */
void lmn_mem_remove_temporary_proxies(LmnMembrane *mem)
{
  unsigned int i;
  Vector remove_list;
  LmnSAtom star, outside;
  AtomListEntry *ent = lmn_mem_get_atomlist(mem, LMN_STAR_PROXY_FUNCTOR);

  if (!ent) return;

  vec_init(&remove_list, 16);

  EACH_ATOM(star, ent, ({
    outside = LMN_SATOM(LMN_SATOM_GET_LINK(star, 0));
    if (!vec_contains(&remove_list, (LmnWord)star)) {
      lmn_mem_unify_atom_args(mem, star, 1, outside, 1);
      vec_push(&remove_list, (LmnWord)star);
      vec_push(&remove_list, (LmnWord)outside);
    }
  }));

  for (i = 0; i < remove_list.num; i++) {
    mem_remove_symbol_atom(mem, LMN_SATOM(vec_get(&remove_list, i)));
    lmn_delete_atom(LMN_SATOM(vec_get(&remove_list, i)));
  }

  vec_destroy(&remove_list);
}

/* cf. Java蜃ｦ逅�邉ｻ */
/*
 * TODO:
 * 縺ｨ縺ｦ繧る撼蜉ｹ邇�縺ｪ縺ｮ縺ｧ�ｼ御ｻ･蜑阪�ｮREMOVE繧ｿ繧ｰ繧剃ｽｿ縺｣縺溷ｮ溯｣�縺ｫ謌ｻ縺吶°
 * HashSet繧剃ｽｿ縺�繧医≧縺ｫ縺吶ｋ
 */
void lmn_mem_remove_toplevel_proxies(LmnMembrane *mem)
{
  Vector remove_list;
  AtomListEntry *ent;
  LmnSAtom outside;
  unsigned int i;

  ent = lmn_mem_get_atomlist(mem, LMN_OUT_PROXY_FUNCTOR);
  if (!ent) return;

  vec_init(&remove_list, 16);

  EACH_ATOM(outside, ent, ({
    LmnSAtom a0;
    a0 = LMN_SATOM(LMN_SATOM_GET_LINK(outside, 0));
    if (LMN_PROXY_GET_MEM(a0) &&
        LMN_PROXY_GET_MEM(a0)->parent != mem) {
      if (!LMN_ATTR_IS_DATA(LMN_SATOM_GET_ATTR(outside, 1))) {
        LmnSAtom a1 = LMN_SATOM(LMN_SATOM_GET_LINK(outside, 1));
        if (LMN_SATOM_GET_FUNCTOR(a1) == LMN_OUT_PROXY_FUNCTOR) {
          LmnSAtom a10 = LMN_SATOM(LMN_SATOM_GET_LINK(a1, 0));
          if (LMN_PROXY_GET_MEM(a10) &&
              LMN_PROXY_GET_MEM(a10)->parent != mem) {
            if (!vec_contains(&remove_list, (LmnWord)outside)) {
              lmn_mem_unify_atom_args(mem, outside, 0, a1, 0);
              vec_push(&remove_list, (LmnWord)outside);
              vec_push(&remove_list, (LmnWord)a1);
            }
          }
        }
      }
    }
  }));

  for (i = 0; i < remove_list.num; i++) {
    mem_remove_symbol_atom(mem, LMN_SATOM(vec_get(&remove_list, i)));
    lmn_delete_atom(LMN_SATOM(vec_get(&remove_list, i)));
  }
  vec_destroy(&remove_list);
}


LmnMembrane *lmn_mem_copy(LmnMembrane *src)
{
  ProcessTbl copymap;
  LmnMembrane *copied;

  copied = lmn_mem_copy_with_map(src, &copymap);
  proc_tbl_free(copymap);
  return copied;
}

LmnMembrane *lmn_mem_copy_ex(LmnMembrane *src)
{
  ProcessTbl copymap;
  LmnMembrane *copied;

  copied = lmn_mem_copy_with_map_ex(src, &copymap);
  proc_tbl_free(copymap);
  return copied;
}

static inline
LmnMembrane *lmn_mem_copy_with_map_inner(LmnMembrane *src,
                                         ProcessTbl  *ret_copymap,
                                         BOOL        hl_nd)
{
  unsigned int i;
  ProcessTbl copymap;
  LmnMembrane *new_mem;

  /* (ueda, 2013-09-21) 繧ｳ繝溘ャ繝亥燕縺ｫ newhlink 縺ｧ菴懊ｉ繧後◆繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ
   * 縺ｮID縺ｨ縺ｮ陦晉ｪ√ｒ驕ｿ縺代ｋ縺溘ａ縺ｫ�ｼ後げ繝ｭ繝ｼ繝舌Ν繝ｫ繝ｼ繝郁�懊�ｮ繧ｳ繝斐�ｼ譎ゅ↓ID縺ｮ繝ｪ繧ｻ
   * 繝�繝医ｒ縺励↑縺�縺ｧ譁ｰ縺溘↑閹懊ｄ繧｢繝医Β繧�繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ繧剃ｽ懊▲縺ｦ繧�縺擾ｼ�
   * 譛ｬ譚･縺ｯ�ｼ後ワ繧､繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ菴懈�� (new) 繧偵Ν繝ｼ繝ｫ縺ｮ繧ｬ繝ｼ繝峨〒陦後≧縺ｮ縺ｧ縺ｪ縺擾ｼ�
   * 繧ｳ繝溘ャ繝育峩蠕後↓菴懈�舌☆繧九⊇縺�縺瑚ｫ也炊逧�縺ｫ譛帙∪縺励￥�ｼ後％縺�縺吶ｌ縺ｰID陦晉ｪ∝撫鬘�
   * 縺ｯ襍ｷ縺阪↑縺�縺鯉ｼ後さ繝ｳ繝代う繝ｩ謾ｹ險ゅ∪縺ｧ縺ｯ谺｡縺ｮ�ｼ題｡後ｒ繧ｳ繝｡繝ｳ繝医い繧ｦ繝医☆繧九％縺ｨ
   * 縺ｧ蟇ｾ蜃ｦ�ｼ�*/
  //  env_reset_proc_ids();  

  new_mem = lmn_mem_make();

  if (hl_nd) {
    copymap = lmn_mem_copy_cells_ex(new_mem, src, TRUE);
  } else {
    copymap = lmn_mem_copy_cells(new_mem, src);
  }

  for (i = 0; i < src->rulesets.num; i++) {
    vec_push(&new_mem->rulesets,
        (LmnWord)lmn_ruleset_copy((LmnRuleSet)vec_get(&src->rulesets, i)));
  }
  *ret_copymap = copymap;

  return new_mem;
}

LmnMembrane *lmn_mem_copy_with_map_ex(LmnMembrane *src, ProcessTbl *ret_copymap)
{
  return lmn_mem_copy_with_map_inner(src, ret_copymap, TRUE);
}

LmnMembrane *lmn_mem_copy_with_map(LmnMembrane *src, ProcessTbl *ret_copymap)
{
  return lmn_mem_copy_with_map_inner(src, ret_copymap, FALSE);
}


inline ProcessTbl lmn_mem_copy_cells_ex(LmnMembrane *dst,
                                        LmnMembrane *src,
                                        BOOL        hl_nd)
{
  ProcessTbl atoms = proc_tbl_make_with_size(64);
  lmn_mem_copy_cells_sub(dst, src, atoms, hl_nd);
  return atoms;
}

ProcessTbl lmn_mem_copy_cells(LmnMembrane *destmem, LmnMembrane *srcmem)
{
  return lmn_mem_copy_cells_ex(destmem, srcmem, FALSE);
}

/* srcmem縺ｮ螳溘ョ繝ｼ繧ｿ讒矩��繧壇estmem縺ｸ繧ｳ繝斐�ｼ逕滓�舌☆繧�. atoms縺ｯ險ｪ蝠乗ｸ医∩縺ｮ邂｡逅�縺ｫ逕ｨ縺�繧�.
 * hl_nd繝輔Λ繧ｰ縺檎悄縺ｮ蝣ｴ蜷�, 繧ｳ繝斐�ｼ蜑阪→繧ｳ繝斐�ｼ蠕後�ｮ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮunify繧偵＠縺ｪ縺�.  */
static void lmn_mem_copy_cells_sub(LmnMembrane *destmem,
                                  LmnMembrane  *srcmem,
                                  ProcessTbl   atoms,
                                  BOOL         hl_nd)
{
  unsigned int i;
  LmnMembrane *m;
  AtomListEntry *ent;

  /* copy child mems */
  for (m = srcmem->child_head; m; m = m->next) {
    LmnMembrane *new_mem = lmn_mem_make();
    lmn_mem_copy_cells_sub(new_mem, m, atoms, hl_nd);
    lmn_mem_add_child_mem(destmem, new_mem);

    proc_tbl_put_mem(atoms, m, (LmnWord)new_mem);
    /* copy name */
    new_mem->name = m->name;
    /* copy rulesets */
    for (i = 0; i < m->rulesets.num; i++) {
      vec_push(&new_mem->rulesets,
               (LmnWord)lmn_ruleset_copy((LmnRuleSet)vec_get(&m->rulesets, i)));
    }
  }

  /* copy atoms */
  EACH_ATOMLIST(srcmem, ent, ({
    LmnSAtom srcatom;
    LmnWord t = 0;

    EACH_ATOM(srcatom, ent, ({
      LmnSAtom newatom;
      unsigned int start, end;
      LmnFunctor f;

      LMN_ASSERT(LMN_SATOM_ID(srcatom) > 0);
      if (proc_tbl_get_by_atom(atoms, srcatom, NULL) || LMN_IS_HL(srcatom)) {
        continue;
      }

      f       = LMN_SATOM_GET_FUNCTOR(srcatom);
      newatom = lmn_mem_newatom(destmem, f);

      proc_tbl_put_atom(atoms, srcatom, (LmnWord)newatom);

      start = 0;
      end   = LMN_SATOM_GET_ARITY(srcatom);

      if (LMN_IS_PROXY_FUNCTOR(f)) {
        start = 1;
        end   = 2;
        LMN_PROXY_SET_MEM(newatom, destmem);

        if (f == LMN_OUT_PROXY_FUNCTOR) {
          LmnSAtom srcinside;
          LmnSAtom newinside;

          srcinside = LMN_SATOM(LMN_SATOM_GET_LINK(srcatom, 0));
          proc_tbl_get_by_atom(atoms, srcinside, &t);
          newinside = LMN_SATOM(t);

          /* 蠢�縺壼ｭ占�懊↓縺､縺ｪ縺後▲縺ｦ縺�繧九�ｯ縺� */
          LMN_ASSERT(LMN_SATOM_GET_FUNCTOR(srcinside) == LMN_IN_PROXY_FUNCTOR);
          LMN_ASSERT(LMN_PROXY_GET_MEM(srcinside)->parent == LMN_PROXY_GET_MEM(srcatom));
          lmn_newlink_in_symbols(newatom, 0, newinside, 0);
        }
      }

      /* 繝ｪ繝ｳ繧ｯ蜈医→謗･邯� */
      for (i = start; i < end; i++) {
        LmnLinkAttr attr = LMN_SATOM_GET_ATTR(srcatom, i);
        LmnAtom a = LMN_SATOM_GET_LINK(srcatom, i);
        if (LMN_ATTR_IS_DATA(attr)) {
          LmnAtom newargatom;

          if (LMN_ATTR_IS_HL(attr) && hl_nd) {
            /* unify縺帙★縺ｫ繧ｳ繝斐�ｼ縺吶ｋ */
            HyperLink *newhl, *orihl;
            LmnAtom ori_attr_atom;
            LmnLinkAttr ori_attr;

            orihl         = lmn_hyperlink_at_to_hl((LmnSAtom)a);
            ori_attr_atom = LMN_HL_ATTRATOM(orihl);
            ori_attr      = LMN_HL_ATTRATOM_ATTR(orihl);
            newargatom    = (LmnAtom)lmn_hyperlink_new_with_attr(ori_attr_atom, ori_attr);
            newhl         = lmn_hyperlink_at_to_hl((LmnSAtom)newargatom);

            /* 蟄舌∈縺ｮ謗･邯� */
            if (orihl->children) {
              HashSetIterator it;
              for (it = hashset_iterator(orihl->children);
                   !hashsetiter_isend(&it);
                   hashsetiter_next(&it)) {
                HyperLink *hl = (HyperLink *)hashsetiter_entry(&it);
                if (proc_tbl_get_by_hlink(atoms, hl, &t)) {
                  hyperlink_unify(newhl, (HyperLink *)t, ori_attr_atom, ori_attr);
                }
              }
            }

            /* 隕ｪ縺ｸ縺ｮ謗･邯� */
            if (orihl->parent &&
                proc_tbl_get_by_hlink(atoms, orihl->parent, &t)) {
              hyperlink_unify((HyperLink *)t, newhl, ori_attr_atom, ori_attr);
            }
            proc_tbl_put_new_hlink(atoms, orihl, (LmnWord)newhl);
          }
          else {
            newargatom = lmn_copy_data_atom(a, attr);
          }

          if (LMN_ATTR_IS_HL(attr)) {
            lmn_mem_newlink(destmem, (LmnAtom)newatom,
                            LMN_ATTR_GET_VALUE(LMN_ATOM(newatom)), i,
                            newargatom, LMN_HL_ATTR, 0);
          } else {
            newlink_symbol_and_something(newatom, i, newargatom, attr);
          }

          lmn_mem_push_atom(destmem, newargatom, attr);
        } else if (proc_tbl_get_by_atom(atoms, LMN_SATOM(a), &t)) {
          newlink_symbol_and_something(newatom, i, LMN_ATOM(t), attr);
        }
      }
    }));
  }));

  lmn_mem_natoms_copy(destmem, srcmem);

//  /* copy activated flag */
//  destmem->is_activated = srcmem->is_activated; /* MC */
  destmem->is_activated = TRUE;
}


LinkObj LinkObj_make(LmnAtom ap, LmnLinkAttr pos)
{
  LinkObj ret = LMN_MALLOC(struct LinkObj);

  ret->ap = ap;
  ret->pos = pos;

  return ret;
}

Edge make_Edge(unsigned long to,int cap)
{
	Edge edge = (Edge) malloc (sizeof(Edge));
	edge->to=to;
	edge->rev=0;
	edge->f=0;
	edge->cap=cap;
	return edge;
}

void free_edge(Edge e){free(e);}


/* lmn_copy_ground_sub縺ｧ菴ｿ縺�髢｢謨ｰ
 * root_hlAtom縺ｮ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｫ縺､縺ｪ縺後ｋ繧｢繝医Β驕斐ｒ繧ｳ繝斐�ｼ縺吶ｋ
 * root_hlAtom縺ｯ謗｢邏｢繧定｡後≧繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ繧｢繝医Β
 * copied_root_hlAtom縺ｯroot_hlAtom縺ｮ繧ｳ繝斐�ｼ縲�
 * stack縺ｯ謗｢邏｢蠕�縺｡縺ｮ繧ｷ繝ｳ繝懊Ν繧｢繝医Β
 * atommap縺ｯ繧ｳ繝斐�ｼ蜈�縺ｨ繧ｳ繝斐�ｼ蜈医�ｮ繧｢繝医Β縺ｮ蟇ｾ蠢�
 * hlinkmap縺ｯ繧ｳ繝斐�ｼ蜈�縺ｨ繧ｳ繝斐�ｼ蜈医�ｮ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ蟇ｾ蠢�
 * attr_functors縺ｯ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ螻樊�ｧ�ｼ医す繝ｳ繝懊Ν繧｢繝医Β縺ｮ蝣ｴ蜷茨ｼ�
 * attr_dataAtoms縺ｯ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ螻樊�ｧ�ｼ医ョ繝ｼ繧ｿ繧｢繝医Β�ｼ�
 * attr_dataAtom_attrs縺ｯ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ螻樊�ｧ�ｼ医ョ繝ｼ繧ｿ繧｢繝医Β縺ｮ螻樊�ｧ)
 */ 
static inline void mem_map_hlink(LmnMembrane *mem,
                             LmnSAtom root_hlAtom,
                             LmnSAtom copied_root_hlAtom,
                             Vector *stack,
							 ProcessTbl *global_hlinks,
                             ProcessTbl atommap,
                             ProcessTbl hlinkmap,
                             ProcessTbl *attr_functors,
                             Vector *attr_dataAtoms,
                             Vector *attr_dataAtom_attrs)

{
  //printf("----------------------mem_map_hlink-----------------------  \n");
  //printf("==================== root_hlAtom=%d  \n",root_hlAtom);
  //printf("==================== copied_root_hlAtom=%d  \n",copied_root_hlAtom);


  LmnWord t=0; 
  HyperLink *hl = lmn_hyperlink_at_to_hl(root_hlAtom);
  BOOL flg_search_hl=FALSE;


  BOOL flg_local_hl=TRUE;
  if(proc_tbl_get_by_hlink(*global_hlinks, lmn_hyperlink_get_root(hl), NULL))
  {//this hyperlink is a global hyperlink
	  flg_local_hl=FALSE;
  }

  if (!LMN_HL_HAS_ATTR(hl)) //if hlink has no attribute
  {
	  if(vec_num(attr_dataAtoms) == 0)  //hlground has no attribute
	  {// matched (no attribute vs no attribute)
		  flg_search_hl=TRUE;
	  }
  }

  {  //check if attribute matches with the attribute of hlground
	  LmnAtom attrAtom = LMN_HL_ATTRATOM(hl);
	  LmnLinkAttr attr = LMN_HL_ATTRATOM_ATTR(hl);
	  int i;
	  for (i = 0; i < vec_num(attr_dataAtoms); i++)
	  {
		  if (lmn_eq_func(attrAtom, attr, vec_get(attr_dataAtoms, i), vec_get(attr_dataAtom_attrs, i)))
		  {
			  flg_search_hl = TRUE;
			  break;
		  }
		  else
		  {
			  continue;
		  }
	  }
  }

  /*
  if (!LMN_HL_HAS_ATTR(hl))
  {//螻樊�ｧ繧呈戟縺｣縺ｦ縺�縺ｪ縺�蝣ｴ蜷医�ｯ辟｡譚｡莉ｶ縺ｫ謗｢邏｢
    flg_search_hl = TRUE;
  }
  else
  {
    LmnAtom attrAtom = LMN_HL_ATTRATOM(hl);
    LmnLinkAttr attr = LMN_HL_ATTRATOM_ATTR(hl);
    int i;
    if (LMN_ATTR_IS_DATA(attr))
    {
      for (i = 0; i < vec_num(attr_dataAtoms); i++)
      {
        if (lmn_eq_func(attrAtom, attr, vec_get(attr_dataAtoms, i), vec_get(attr_dataAtom_attrs, i)))
        {
          flg_search_hl = TRUE;
          break;
        }
        else
        {
          continue;
        }
      }
    }
    else
    {
      if (proc_tbl_get(*attr_functors, LMN_SATOM_GET_FUNCTOR(LMN_SATOM(attrAtom)), NULL))
      {
        flg_search_hl = TRUE;
      }
    } 
  }
  */
  if (flg_search_hl && flg_local_hl)
  {
    if (!proc_tbl_get_by_hlink(hlinkmap, lmn_hyperlink_get_root(hl), &t))
    {//蜷後§繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺梧磁邯壹＆繧後◆繧｢繝医Β縺後せ繧ｿ繝�繧ｯ縺ｫ遨阪∪繧後※繧句�ｴ蜷医′縺ゅｋ
      int j, element_num;
      proc_tbl_put_new_hlink(hlinkmap, lmn_hyperlink_get_root(hl), (LmnWord)(lmn_hyperlink_at_to_hl(copied_root_hlAtom)));
      Vector *hl_childs = vec_make(16);
      lmn_hyperlink_get_elements(hl_childs, hl);
      element_num = vec_num(hl_childs) - 1;
      for (j = 0; j < element_num; j++) {//繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｫ縺､縺ｪ縺後ｋ縺吶∋縺ｦ縺ｮ謗･邯壼�医ｒ謗｢邏｢
        if (hl->mem!=((HyperLink *)vec_get(hl_childs, j))->mem)
        {//root_hlAtom縺ｮ謇�螻櫁�懊→逡ｰ縺ｪ繧玖�懷��縺ｯ繧ｳ繝斐�ｼ縺励↑縺�
          continue;
        }
        LmnSAtom hlAtom = ((HyperLink *)vec_get(hl_childs, j))->atom;
        if (LMN_ATTR_IS_DATA(LMN_SATOM_GET_ATTR(hlAtom, 0)))
        {
          LmnSAtom copied_hlAtom = lmn_copy_satom_with_data((hlAtom), FALSE);
          lmn_hyperlink_copy(copied_hlAtom, LMN_SATOM(copied_root_hlAtom));
          lmn_mem_push_atom(mem, LMN_ATOM(copied_hlAtom), LMN_HL_ATTR);
        }
        else
        {
          LmnSAtom linked_hlAtom = LMN_SATOM(LMN_SATOM_GET_LINK(hlAtom, 0));
          if (!proc_tbl_get_by_atom(atommap, linked_hlAtom, &t)) {//繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ繧｢繝医Β蜿翫�ｳ縺昴ｌ縺ｫ縺､縺ｪ縺後ｋ繧ｷ繝ｳ繝懊Ν繧｢繝医Β繧偵さ繝斐�ｼ
	    // 縺ｾ縺壹す繝ｳ繝懊Ν繧｢繝医Β繧偵さ繝斐�ｼ
            LmnSAtom copied_linked_hlAtom = lmn_copy_satom_with_data(linked_hlAtom, TRUE);
            LmnSAtom copied_hlAtom = LMN_SATOM(LMN_SATOM_GET_LINK(copied_linked_hlAtom, LMN_SATOM_GET_ATTR(hlAtom, 0)));
	    // 縺薙％縺ｧ縺ｯ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ讒矩��菴薙�ｯ譛ｪ菴懈�舌�〕mn_hyperlink_copy縺ｧ菴懈��
            lmn_hyperlink_copy(copied_hlAtom, LMN_SATOM(copied_root_hlAtom));
	    // 縺薙％縺ｧ菴懊ｉ繧後◆ copied_hlAtom 縺ｯ縺ゅ→縺ｧ繧ｷ繝ｳ繝懊Ν繧｢繝医Β蛛ｴ縺九ｉ騾�繧｢繧ｯ繧ｻ繧ｹ縺輔ｌ繧九�ｯ縺壹↑縺ｮ縺ｧ
	    // 縺薙％縺ｧ縺ｯ push 縺励↑縺上※繧医＞�ｼ医�ｯ縺夲ｼ�
	    //            lmn_mem_push_atom(mem, LMN_ATOM(copied_hlAtom), LMN_HL_ATTR);
            mem_push_symbol_atom(mem, LMN_SATOM(copied_linked_hlAtom));
            proc_tbl_put_atom(atommap, linked_hlAtom, LMN_ATOM(copied_linked_hlAtom));
            vec_push(stack, (LmnWord)linked_hlAtom);
          }
          else
          {//縺､縺ｾ繧頑里縺ｫ繧ｹ繧ｿ繝�繧ｯ縺ｫ遨阪∪繧後※縺�繧句�ｴ蜷�
            //繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｸ縺ｮ謗･邯壹�ｯ縲√せ繧ｿ繝�繧ｯ縺ｫ遨阪∪繧後※縺�繧九い繝医Β蛛ｴ縺九ｉ縺ｮ謗｢邏｢譎ゅ↓陦後≧縲�
	    // 縺昴�ｮ縺ｨ縺阪�√い繝医Β蛛ｴ縺九ｉ縺溘←縺｣縺溘ワ繧､繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｯ縺ｾ縺� atomlist 縺ｫ逋ｻ骭ｲ縺輔ｌ縺ｦ
	    // 縺�縺ｪ縺�縺ｯ縺壹�よ眠縺溘↓ atomlist 縺ｫ逋ｻ骭ｲ縺吶ｋ蠢�隕√′縺ゅｋ縲�
          }
        }
      }
      vec_free(hl_childs);
    }
    else
    {//譌｢縺ｫ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ繧偵さ繝斐�ｼ縺励※縺�繧後�ｰunify
      lmn_hyperlink_unify(lmn_hyperlink_at_to_hl(copied_root_hlAtom),
          (HyperLink *)t,
          LMN_HL_ATTRATOM((HyperLink *)t),
          LMN_HL_ATTRATOM_ATTR((HyperLink *)t));
    }
  }
  else
  {
	//print_hlink_byhlink(hl);
    lmn_hyperlink_unify(hl, lmn_hyperlink_at_to_hl(copied_root_hlAtom), LMN_HL_ATTRATOM(hl), LMN_HL_ATTRATOM_ATTR(hl));
    //print_hlink_byhlink(hl);
  }

  //printf(" ================= pushed atom(%d,%d)  \n",copied_root_hlAtom,LMN_HL_ATTR);



  lmn_mem_push_atom(mem, LMN_ATOM(copied_root_hlAtom), LMN_HL_ATTR);

  //lmn_hyperlink_print(mem);
  //printf("----------------------               -----------------------  \n");
}


/* 閹徇em縺ｮsrcvec繧呈�ｹ縺ｫ謖√▽ground繝励Ο繧ｻ繧ｹ繧偵さ繝斐�ｼ縺吶ｋ.
 * srcvec縺ｯ繝ｪ繝ｳ繧ｯ繧ｪ繝悶ず繧ｧ繧ｯ繝医�ｮ繝吶け繧ｿ.
 * ret_dstlovec縺ｯ繧ｳ繝斐�ｼ縺輔ｌ縺滓�ｹ縺ｮ繝ｪ繝ｳ繧ｯ繧ｪ繝悶ず繧ｧ繧ｯ繝医�ｮ繝吶け繧ｿ. vector to store copies of srcvec
 * ret_atommap縺ｯ繧ｳ繝斐�ｼ蜈�縺ｨ繧ｳ繝斐�ｼ蜈医�ｮ繧｢繝医Β縺ｮ蟇ｾ蠢�
 * ret_hlinkmap縺ｯ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ繧ｳ繝斐�ｼ蜈�縺ｨ蜈医〒縺ゅｊ
 * hlground縺ｮ繝輔Λ繧ｰ繧ょ�ｼ縺ｭ縺ｦ縺�繧� */
static inline void mem_copy_ground_sub(LmnMembrane *mem,
                             Vector *srcvec,
							 ProcessTbl *global_hlink,
                             Vector **ret_dstlovec,
                             ProcessTbl *ret_atommap,
                             ProcessTbl *ret_hlinkmap,
                             ProcessTbl *attr_functors,
                             Vector *attr_dataAtoms,
                             Vector *attr_dataAtom_attrs)
{
  ProcessTbl atommap;
  ProcessTbl hlinkmap;
  Vector *stack;

  unsigned int i;
  LmnWord t = 0;

  atommap = proc_tbl_make_with_size(64);
  hlinkmap = proc_tbl_make_with_size(64);
  stack = vec_make(16);
  *ret_dstlovec = vec_make(16);

  /* 譬ｹ繧偵せ繧ｿ繝�繧ｯ縺ｫ遨阪�.
   * 繧ｹ繧ｿ繝�繧ｯ縺ｫ縺ｯ繝ｪ繝ｳ繧ｯ繧ｪ繝悶ず繧ｧ繧ｯ繝医〒縺ｯ縺ｪ縺上い繝医Β繧堤ｩ阪�縺溘ａ ,
   * 縺薙％縺ｧ譬ｹ縺ｮ蜈医�ｮ繧｢繝医Β繧偵さ繝斐�ｼ縺励せ繧ｿ繝�繧ｯ縺ｫ遨阪�蠢�隕√′縺ゅｋ */
  for (i = 0; i < vec_num(srcvec); i++)
  {
    LinkObj l = (LinkObj)vec_get(srcvec, i);
    LmnAtom cpatom;

    //printf("*** srcvec[%d]: l->ap=%d, l->pos=%d  *** \n",i,(int)l->ap,(int)l->pos);

    if (LMN_ATTR_IS_DATA(l->pos))
    {
      if (ret_hlinkmap != NULL && l->pos == LMN_HL_ATTR)
      {//hlground縺ｪ繧峨ワ繧､繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ蜈医ｒ霎ｿ縺｣縺ｦ繧ｳ繝斐�ｼ縺吶ｋ
    	//LmnAtom t_atom=  LMN_HL_ATTRATOM(lmn_hyperlink_at_to_hl(LMN_SATOM(l->ap)));
    	//LmnLinkAttr t_attr= LMN_HL_ATTRATOM_ATTR(lmn_hyperlink_at_to_hl(LMN_SATOM(l->ap)));

        cpatom = (LmnAtom)lmn_hyperlink_new_with_attr(
        											  LMN_HL_ATTRATOM(lmn_hyperlink_at_to_hl(LMN_SATOM(l->ap))),
                                                      LMN_HL_ATTRATOM_ATTR(lmn_hyperlink_at_to_hl(LMN_SATOM(l->ap)))
											          );
        //print hlink to check
        //print_hlink(l->ap);

        mem_map_hlink(mem, LMN_SATOM(l->ap),
                                     LMN_SATOM(cpatom),
                                     stack,
									 global_hlink,
                                     atommap,
                                     hlinkmap,
                                     attr_functors,
                                     attr_dataAtoms,
                                     attr_dataAtom_attrs);
        //print_hlink(l->ap);
      }
      else
      {
        cpatom = lmn_copy_data_atom(l->ap, l->pos);
        lmn_mem_push_atom(mem, cpatom, l->pos);
      }

    }
    else
    { /* symbol atom */
      /* 繧ｳ繝斐�ｼ貂医∩縺ｧ縺ｪ縺代ｌ縺ｰ繧ｳ繝斐�ｼ縺吶ｋ */
      if (!proc_tbl_get_by_atom(atommap, LMN_SATOM(l->ap), &t))
      {
        if (ret_hlinkmap != NULL)
        {//hlground縺ｪ繧�
          cpatom = LMN_ATOM(lmn_copy_satom_with_data(LMN_SATOM(l->ap), TRUE));
        }
        else
        {
          cpatom = LMN_ATOM(lmn_copy_satom_with_data(LMN_SATOM(l->ap), FALSE));
        }

        mem_push_symbol_atom(mem, LMN_SATOM(cpatom));
        proc_tbl_put_atom(atommap, LMN_SATOM(l->ap), (LmnWord)cpatom);

        /* 譬ｹ縺ｮ繝ｪ繝ｳ繧ｯ縺ｮ繝ｪ繝ｳ繧ｯ繝昴う繝ｳ繧ｿ繧�0縺ｫ險ｭ螳壹☆繧� */
        LMN_SATOM_SET_LINK(cpatom, l->pos, 0);
        vec_push(stack, l->ap);
      }
      else
      {
        /* 繧ｳ繝斐�ｼ貂医∩縺ｮ蝣ｴ蜷医�ｯ繧ｹ繧ｿ繝�繧ｯ縺ｫ縺ｯ霑ｽ蜉�縺励↑縺�  */
        cpatom = LMN_ATOM(t);
        LMN_SATOM_SET_LINK(cpatom, l->pos, 0);
      }
    }
    vec_push(*ret_dstlovec, (vec_data_t)LinkObj_make(cpatom, l->pos));
    //printf("***  copied atom=%d  *** \n",cpatom);
  }

  //printf("***        printing stack iteration             *** \n");

  while (!vec_is_empty(stack))
  {
    LmnSAtom src_atom, copied;

    src_atom = (LmnSAtom)vec_pop(stack);
    proc_tbl_get_by_atom(atommap, src_atom, &t);
    copied = LMN_SATOM(t);


    //printf("------------ src_atom = %d  *** \n",src_atom);
    //printf("------------ its copy = %d  *** \n",copied);

    for (i = 0; i < LMN_SATOM_GET_ARITY(src_atom); i++)
    {
      LmnAtom     next_src  = LMN_SATOM_GET_LINK(src_atom, i);
      LmnLinkAttr next_attr = LMN_SATOM_GET_ATTR(src_atom, i);

      //printf("---------------next_src=%d,next_attr=%d   *** \n",next_src,next_attr);

      /* LMN_SATOM_GET_LINK(copied, i)縺�0縺ｫ縺ｪ繧句�ｴ蜷医�ｯ縲∵�ｹ縺ｫ蛻ｰ驕斐＠縺溷�ｴ蜷� */
      /*if LMN_SATOM_GET_LINK(copied, i)==0, means it reached the root link*/
      if (LMN_ATTR_IS_DATA(next_attr))
      {
        if (ret_hlinkmap != NULL && next_attr == LMN_HL_ATTR)
        {//hlground縺ｪ繧峨ワ繧､繝代�ｼ繝ｪ繝ｳ繧ｯ縺ｮ蜈医ｒ霎ｿ縺｣縺ｦ繧ｳ繝斐�ｼ縺吶ｋ
          mem_map_hlink(mem,
                        LMN_SATOM(next_src),
                        LMN_SATOM(LMN_SATOM_GET_LINK(copied, i)),
                        stack,
						global_hlink,
                        atommap,
                        hlinkmap,
                        attr_functors,
                        attr_dataAtoms,
                        attr_dataAtom_attrs);
        }
        else
        {
          if (next_attr == LMN_HL_ATTR)
          {
            LmnAtom next_src_copied = LMN_ATOM(LMN_SATOM_GET_LINK(copied, i));
            lmn_mem_push_atom(mem, next_src_copied, next_attr);
          }
          else
          {
            lmn_mem_push_atom(mem, next_src, next_attr);
          }
        }
      }
      else if (LMN_SATOM_GET_LINK(copied, i) != 0)
      {
        LmnAtom next_copied;
        if (!proc_tbl_get_by_atom(atommap, LMN_SATOM(next_src), &t))
        { /* next_src縺ｯ譛ｪ險ｪ蝠� */
          if (ret_hlinkmap != NULL)
          {//hlground縺ｪ繧�
            next_copied = LMN_ATOM(lmn_copy_satom_with_data(LMN_SATOM(next_src), TRUE));
          }
          else
          {
            next_copied = LMN_ATOM(lmn_copy_satom_with_data(LMN_SATOM(next_src), FALSE));
          }
          mem_push_symbol_atom(mem, LMN_SATOM(next_copied));
          proc_tbl_put_atom(atommap, LMN_SATOM(next_src), next_copied);
          vec_push(stack, next_src);
        }
        else
        {
          next_copied = LMN_ATOM(t);
        }
        //printf("--------------next_copied=%d   \n",next_copied);
        LMN_SATOM_SET_LINK(copied, i, next_copied);
      }
    }
  }

  //printf("***         printing copy_hlground is over           *** \n");
  //printf("***                                                  *** \n");
  vec_free(stack);
  *ret_atommap = atommap;
  if (ret_hlinkmap != NULL)
  {
    *ret_hlinkmap = hlinkmap;
  }
}

void lmn_mem_copy_ground(LmnMembrane *mem,
			 Vector *srcvec,
			 Vector **ret_dstlovec,
			 ProcessTbl *ret_atommap)
{
  mem_copy_ground_sub(mem, srcvec,NULL, ret_dstlovec, ret_atommap, NULL, NULL, NULL, NULL);
}

void lmn_mem_copy_hlground(LmnMembrane *mem,
                             Vector *srcvec,
                             Vector **ret_dstlovec,
                             ProcessTbl *ret_atommap,
                             ProcessTbl *ret_hlinkmap,
                             ProcessTbl *attr_functors,
                             Vector *attr_dataAtoms,
                             Vector *attr_dataAtom_attrs)
{
	ProcessTbl hlinks=hlground_data.global_hlinks;

	//BOOL b;
	//unsigned long natoms;

	//b = ground_atoms(srcvec, NULL, &atoms, &natoms, NULL, attr_functors, attr_dataAtoms, attr_dataAtom_attrs);

	mem_copy_ground_sub(mem,
                      srcvec,
					  &hlinks,
                      ret_dstlovec,
                      ret_atommap,
                      ret_hlinkmap,
                      attr_functors,
                      attr_dataAtoms,
                      attr_dataAtom_attrs);

}

/* srcvec,dstvec縺ｯ豈碑ｼ�蜈�,豈碑ｼ�蜈�ground縺ｮ譏守､ｺ逧�閾ｪ逕ｱ繝ｪ繝ｳ繧ｯLinkObj.
 * ground讀懈渊縺ｯ縺吶ｓ縺ｧ縺�繧九ｂ縺ｮ縺ｨ縺吶ｋ. src縺ｨdst縺悟酔縺伜ｽ｢縺ｪ繧臥悄繧定ｿ斐☆.
 *
 * TODO: 讒矩��蛹� */
BOOL lmn_mem_cmp_ground(const Vector *srcvec, const Vector *dstvec)
{
  unsigned int i, j;
  BOOL ret_flag = TRUE;
  Vector stack1, stack2;
  SimpleHashtbl map; /* 豈碑ｼ�蜈�->豈碑ｼ�蜈� */
  LinkObj start1, start2;

  hashtbl_init(&map, 256);

  vec_init(&stack1, 16);
  vec_init(&stack2, 16);

  /* start縺ｯstack縺ｫ縺､縺ｾ繧後ｋ縺ｮ縺ｧ蜃ｦ逅�荳ｭ縺ｫ螢翫＆繧後ｋ縺溘ａ繧ｳ繝斐�ｼ */
  start1 = LinkObj_make(((LinkObj)vec_get(srcvec,0))->ap,
                        ((LinkObj)vec_get(srcvec,0))->pos);
  start2 = LinkObj_make(((LinkObj)vec_get(dstvec,0))->ap,
                        ((LinkObj)vec_get(dstvec,0))->pos);

  if (!LMN_ATTR_IS_DATA(start1->pos) && !LMN_ATTR_IS_DATA(start2->pos)) {
    /* 縺ｨ繧ゅ↓繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺ｮ蝣ｴ蜷� */
    vec_push(&stack1, (LmnWord)start1);
    vec_push(&stack2, (LmnWord)start2);
  }
  else { /* data atom 縺ｯ遨阪∪縺ｪ縺� */
    if (!lmn_data_atom_eq(start1->ap, start1->pos, start2->ap, start2->pos)) {
      ret_flag = FALSE;
    }
    LMN_FREE(start1);
    LMN_FREE(start2);
  }

  while (!vec_is_empty(&stack1)) { /* main loop: start */
    LinkObj l1, l2;
    BOOL contains1, contains2;

    l1 = (LinkObj )vec_pop(&stack1);
    l2 = (LinkObj )vec_pop(&stack2);
    contains1 = FALSE;
    contains2 = FALSE;

    for (i = 0; i < srcvec->num; i++) {
      LinkObj lobj = (LinkObj)vec_get(srcvec, i);
      if (l1->ap == LMN_SATOM_GET_LINK(lobj->ap, lobj->pos)
          && l1->pos == LMN_SATOM_GET_ATTR(lobj->ap, lobj->pos)) {
        contains1 = TRUE;
        break;
      }
    }
    for (j = 0; j < dstvec->num; j++) {
      LinkObj lobj = (LinkObj)vec_get(dstvec, j);
      if (l2->ap == LMN_SATOM_GET_LINK(lobj->ap, lobj->pos)
          && l2->pos == LMN_SATOM_GET_ATTR(lobj->ap, lobj->pos)) {
        contains2 = TRUE;
        break;
      }
    }
    if (i != j) { /* 譬ｹ縺ｮ菴咲ｽｮ縺碁＆縺� */
      LMN_FREE(l1); LMN_FREE(l2);
      ret_flag = FALSE;
      break;
    }
    if (contains1) { /* 譬ｹ縺ｫ蛻ｰ驕斐＠縺溷�ｴ蜷� */
      LMN_FREE(l1); LMN_FREE(l2);
      continue;
    }

    if (l1->pos != l2->pos) { /* 蠑墓焚讀懈渊 */
      LMN_FREE(l1); LMN_FREE(l2);
      ret_flag = FALSE;
      break;
    }

    if (LMN_SATOM_GET_FUNCTOR(l1->ap) != LMN_SATOM_GET_FUNCTOR(l2->ap)) {
      /* 繝輔ぃ繝ｳ繧ｯ繧ｿ讀懈渊 */
      LMN_FREE(l1); LMN_FREE(l2);
      ret_flag = FALSE;
      break;
    }

    if (!hashtbl_contains(&map, l1->ap)) {
      /* 譛ｪ蜃ｺ */
      hashtbl_put(&map, l1->ap, l2->ap);
    }
    else if (hashtbl_get(&map, l1->ap) != l2->ap) {
      /* 譌｢蜃ｺ縺ｧ荳堺ｸ�閾ｴ */
      LMN_FREE(l1); LMN_FREE(l2);
      ret_flag = FALSE;
      break;
    }
    else {
      /* 譌｢蜃ｺ縺ｧ荳�閾ｴ */
      continue;
    }

    for (i = 0; i < LMN_SATOM_GET_ARITY(l1->ap); i++) {
      LinkObj n1, n2;
      if (i == l1->pos) continue;
      if (!LMN_ATTR_IS_DATA(LMN_SATOM_GET_ATTR(l1->ap, i)) &&
          !LMN_ATTR_IS_DATA(LMN_SATOM_GET_ATTR(l1->ap, i))) {
        n1 = LinkObj_make(LMN_SATOM_GET_LINK(l1->ap, i),
                          LMN_ATTR_GET_VALUE(LMN_SATOM_GET_ATTR(l1->ap, i)));
        n2 = LinkObj_make(LMN_SATOM_GET_LINK(l2->ap, i),
                          LMN_ATTR_GET_VALUE(LMN_SATOM_GET_ATTR(l2->ap, i)));
        vec_push(&stack1, (LmnWord)n1);
        vec_push(&stack2, (LmnWord)n2);
      }
      else { /* data atom 縺ｯ遨阪∪縺ｪ縺� */
        if (!lmn_data_atom_eq(LMN_SATOM_GET_LINK(l1->ap, i),
                              LMN_SATOM_GET_ATTR(l1->ap, i),
                              LMN_SATOM_GET_LINK(l2->ap, i),
                              LMN_SATOM_GET_ATTR(l2->ap, i))) {
          LMN_FREE(l1);
          LMN_FREE(l2);
          ret_flag = FALSE;
          goto CMPGROUND_BREAK;
        }
      }
    }

    LMN_FREE(l1);
    LMN_FREE(l2);
  } /* main loop: end */

CMPGROUND_BREAK:
  for (i = 0; i < vec_num(&stack1); i++) LMN_FREE((LinkObj)vec_get(&stack1, i));
  for (i = 0; i < vec_num(&stack2); i++) LMN_FREE((LinkObj)vec_get(&stack2, i));
  vec_destroy(&stack1);
  vec_destroy(&stack2);
  hashtbl_destroy(&map);

  return ret_flag;
}

/* srcvec縺ｮ繝ｪ繝ｳ繧ｯ縺ｮ蛻励′蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｫ蛻ｰ驕�(avovec縺ｮ繝ｪ繝ｳ繧ｯ縺ｫ蛻ｰ驕斐☆繧句�ｴ
   蜷医�ｯ蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｧ縺ｯ縺ｪ縺�)縺励※縺�繧句�ｴ蜷医�∫悄繧定ｿ斐＠縲］atoms縺ｫ蝓ｺ蠎暮��繝�
   繝ｭ繧ｻ繧ｹ縺ｪ縺�縺ｮ繧｢繝医Β縺ｮ謨ｰ繧呈�ｼ邏阪☆繧九��*/
BOOL lmn_mem_is_ground(Vector *srcvec, Vector *avovec, unsigned long *natoms)
{
  ProcessTbl atoms=proc_tbl_make_with_size(64);
  BOOL b;

  b = ground_atoms(srcvec, avovec, &atoms, natoms, NULL, NULL, NULL, NULL);


  proc_tbl_free(atoms);


  return b;
}

/* hlground迚医��*/
BOOL lmn_mem_is_hlground(Vector *srcvec,
                         Vector *avovec,
                         unsigned long *natoms,
                         ProcessTbl *attr_functors,
                         Vector *attr_dataAtoms,
                         Vector *attr_dataAtom_attrs)
{

  BOOL b;
  b = hlground_atoms(srcvec, avovec, NULL, natoms, NULL, attr_functors, attr_dataAtoms, attr_dataAtom_attrs);

  if(!b)
  {
	  proc_tbl_free(hlground_data.local_atoms);
	  proc_tbl_free(hlground_data.global_hlinks);
  }

  return b;
}


#include<time.h>

#define IS_SAME(xap, xattr, yap, yattr)                                       \
      ( (xap == yap)					 		&&                             	\
        (xattr == yattr) )
/* srcvec縺九ｉ蜃ｺ繧九Μ繝ｳ繧ｯ縺ｮ繝ｪ繧ｹ繝医′蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｫ蛻ｰ驕斐＠縺ｦ縺�繧句�ｴ蜷医��
 * avovec縺ｫ蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｫ蟄伜惠縺吶ｋ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縲］atoms縺ｫ繧｢繝医Β縺ｮ謨ｰ縲∵綾繧雁�､縺ｫ逵溘ｒ霑斐☆縲�
 * 繝ｪ繧ｹ繝医′蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｫ蛻ｰ驕斐＠縺ｦ縺�縺ｪ縺�蝣ｴ蜷医↓縺ｯatoms縺ｯNULL縺ｨ縺ｪ繧翫�∝⊃繧定ｿ斐☆縲�
 * 縺溘□縺励�√Μ繝ｳ繧ｯ縺径vovec縺ｫ蛻ｰ驕斐＠縺ｦ縺�繧句�ｴ蜷医↓縺ｯ縲∝渕蠎暮��繝励Ο繧ｻ繧ｹ縺ｨ縺ｯ縺ｪ繧峨↑縺�縲�
 * hlinks縺君ULL縺ｧ縺ｪ縺代ｌ縺ｰhlground縺ｨ縺励※謗｢邏｢
 * attr_functors, attr_dataAtoms, attr_dataAtoms_attr縺ｯhlground縺ｮ螻樊�ｧ */
BOOL hlground_atoms(Vector        *srcvec, 				//store root link
                  Vector        *avovec,   				//other links of source atom
                  ProcessTbl    *atoms,    				/* collects atom within hlground */
                  unsigned long *natoms,   				/* number of atoms within hlground */
                  ProcessTbl    *hlinks,   				/* hlinks!=NULL縺ｪ繧峨�”lground縺ｨ縺励※謗｢邏｢ collects hyperlinks local to hlground  */
                  ProcessTbl    *attr_functors,			/* hlground縺ｮ螻樊�ｧ�ｼ�unary atom�ｼ� used when attribute is specified */
                  Vector        *attr_dataAtoms,		/* hlground縺ｮ螻樊�ｧ�ｼ�data atom�ｼ� used when attribute is specified */
                  Vector        *attr_dataAtom_attrs	/* hlground縺ｮ螻樊�ｧ(data atom縺ｮ螻樊�ｧ) used when attribute is specified */
                  )
{

  BOOL result= TRUE;		//TRUE if it is hlground
  *natoms = 0; 				//number of symbol atoms + local hyperlinks within hlground

  LinkObj t_link = (LinkObj)vec_get(srcvec, 0);
  LmnAtom root_ap = t_link->ap;
  LmnLinkAttr root_pos = t_link->pos;

  init_hlgrounddata();
  if(cycle_exist(srcvec,avovec,attr_functors,attr_dataAtoms,attr_dataAtom_attrs))
  {//there is at least one cycle
	  if(!purecycle_exit(srcvec,avovec))
	  {	  //hlground exist
		  build_hlink_graph(srcvec,
				  	  	  	  avovec,
				  	  	  	  attr_functors,
				  	  	  	  attr_dataAtoms,
				  	  	  	  attr_dataAtom_attrs);
		  int flow= mincut_dinic(hlground_data.source, hlground_data.target);
		  free_hlgrounddata();   //release memory immediately
	  }
	  else
	  {   //pure cycle exist, so hlground does not exist
		  result = FALSE;
	  }
  }
  if(result)
  {
	  get_local_atoms( LinkObj_make(root_ap,root_pos),
			           avovec,
					   &(hlground_data.global_hlinks),   //provide this
					   &(hlground_data.local_atoms),     //to get this
					   natoms,
					   attr_functors,
					   attr_dataAtoms,
					   attr_dataAtom_attrs);
  }
  return result;
}


/* by flow network and non-hlground hyperlinks, get global hyperlinks
 * for a saturated edge (v,w):
 *    'w' is an global hyperlinks if 'v' belongs to non-hlground hyperlinks
 * */
void get_global_hlinks(unsigned long source)
{

	vec_set(hlground_data.work, source,1); //set visited
	int i;
	Vector * vec=(Vector*)vec_get(hlground_data.graph,source);
	for(i=0;i< vec_num(vec);i++)
	{
		BOOL m_noway=FALSE;
		Edge e=(Edge)vec_get(vec,i);
		unsigned long v = e->to;

		unsigned long visit=0;
		visit=vec_get(hlground_data.work,v);
		unsigned long flow = (e->cap - e->f);
		if(e->cap == 0 )
			m_noway=TRUE;  //this is a deleted edge, don't follow it
		if( flow <=  0     &&  !visit   && e->cap)  //saturated edge
		{
			if(!proc_tbl_get(hlground_data.non_hlground_atoms,(LmnWord)v,NULL) &&
					!proc_tbl_get(hlground_data.non_hlground_hlinks,(LmnWord)v,NULL))
			{   // 'v' is not both in the non-hlground atoms and the non-hlground hlinks
				/* * several cases to consider
					 * (1) 'source'  and 'v' both are hyperlink nodes, then source is a global hyperlink
					 * (2) if only one of them are hyperlink node, then it is a global hyperlink
					 * */
				unsigned long v_value;
				unsigned long source_value;

				int v_mark=0;
				int source_mark=0;

				source_mark  = proc_tbl_get(hlground_data.id_link,(LmnWord)source,&source_value);
				if(v != hlground_data.target)
					v_mark   = proc_tbl_get(hlground_data.id_link,(LmnWord)v,&v_value);

				if( v_mark && source_mark )
				{  //both are hlink nodes, so 'v' is a global hyperlink
					proc_tbl_put(hlground_data.global_hlinks,(LmnWord)v_value,(LmnWord)v_value);
				}
				else if(v_mark && !source_mark)
				{
					proc_tbl_put(hlground_data.global_hlinks,(LmnWord)v_value,(LmnWord)v_value);
				}
				else if(!v_mark && source_mark)
				{
					proc_tbl_put(hlground_data.global_hlinks,(LmnWord)source_value,(LmnWord)source_value);
				}
				else
				{
					printf("----get_global_hlinks: constructed graph must be wrong --------\n");
					break;
				}
			}

		}
		else
		{
			if(!visit && !m_noway)
				get_global_hlinks(v);
		}
	}
}

/*from flow network get the nodes which neither represent global Hyperlinks nor local hyperlinks
 * */
void get_non_hlround_hlinks(unsigned long source)
{
	unsigned long id=source;
	unsigned long link_root_id;
	LmnWord atom;

	BOOL m_bool=FALSE;
	if(source == hlground_data.source || source == hlground_data.target )
		m_bool=TRUE;
	if(!m_bool)
	{
		if(!proc_tbl_get(hlground_data.id_atom,(LmnWord)id,&atom) )
		{// source is a hyperlink node
			if(proc_tbl_get(hlground_data.id_link,(LmnWord)id,(LmnWord)&link_root_id )  )
			{
				proc_tbl_put(hlground_data.non_hlground_hlinks,(LmnWord)id,(LmnWord)link_root_id);  //set of non-hlground (id,hlink) pairs
			}
		}
		else   //source is an atom node
		{
				proc_tbl_put(hlground_data.non_hlground_atoms,(LmnWord)id,atom);  //set of non-hlground (id,atom) pairs
		}
	}

	vec_set(hlground_data.work, source,1);

	int i;
	Vector * vec=vec_get(hlground_data.graph,source);
	for(i=0;i< vec_num(vec);i++)
	{
		Edge e=(Edge)vec_get(vec,i);

		if( (e->cap - e->f) >  0)
		{
			unsigned long v = e->to;

			unsigned long visit=0;
			visit=vec_get(hlground_data.work,v);
			if(!visit)
			{
				get_non_hlround_hlinks(v);
			}
		}

	}
}

/* by Dinic algorithm find min-cut edges in adjacency list
 * src: super source,set 1
 * dest: super sink, set 0
 * */
int mincut_dinic(unsigned long src, unsigned long dest)
{
	/*first compute flow */
	int result=0; //no interest in max flow, but useful for debugging
	while(dinic_bfs())  //build layer graph
	{
		init_workarray();
		int delta=0;
		do
		{
		   delta=dinic_dfs(src,INT_MAX);
		   result +=delta;
		} while(delta);
	}
	/* blocking flow  is over*/
	/* now find min-cuts from flow network */
	compute_global_hyperlinks();
	return result;
}

/* the whole hypergraph is 'U', as U=S+T,
 *  where 'S' is non-hlground and 'T' is hlground.
 * the global hyperlinks connect 'S' and 'T'.
 * */
void compute_global_hyperlinks()
{
	/*
	 * get non-hlround hyperlinks (may include gloabl hyperlinks)
	 * */
	init_workarray();
	get_non_hlround_hlinks(hlground_data.source);

	/*
	 * get global hyperlinks
	 * */
	init_workarray();
	get_global_hlinks(hlground_data.source);
}

/*build layer graph*/
BOOL dinic_bfs()
{
	init_layer();

	vec_set(hlground_data.dist,  hlground_data.source,  0);    //set source layer 0
	Queue *qu=new_queue();									  //queue for BFS
	enqueue(qu,hlground_data.source);

	while(!is_empty_queue(qu))
	{
		unsigned int u =dequeue(qu);
		Vector * u_vec = (Vector*)vec_get(hlground_data.graph,u);
		unsigned int size= vec_num(u_vec);
		unsigned int i;
		for(i=0;i<size;i++)
		{
			Edge e= (Edge)vec_get(u_vec,i);
			unsigned int to=e->to;
			if(to == hlground_data.source) continue;  //don't turn back to source

			int d= vec_get(hlground_data.dist,to);
			if(d == 0 && e->f < e->cap )
			{
				unsigned long u_dist=vec_get(hlground_data.dist,u);
				unsigned long new_dist;
				new_dist=u_dist+1;
				vec_set(hlground_data.dist, to, new_dist );
				enqueue(qu,to);
			}
		}
	}

	q_free(qu);

	//check if the target atom reached
	int res= vec_get(hlground_data.dist, hlground_data.target);
	if(res)
		return TRUE;
	else
		return FALSE;
}

/*blocking flow */
int dinic_dfs(unsigned int src, int f)
{
	if(src == hlground_data.target)  //reach target, return block flow
		return f;

	unsigned int i;
	Vector * src_vec=(Vector*)vec_get(hlground_data.graph,src);

	for(i= vec_get(hlground_data.work,src) ;
		i < vec_num(src_vec);
		i++,vec_set(hlground_data.work,src,i)
		)
	{
		Edge e=(Edge)vec_get(src_vec,i);
		if(e->cap <= e->f)			//advance on non-saturated edges
			continue;

		unsigned long v=e->to;
		unsigned long dis1,dis2;

		dis1=vec_get(hlground_data.dist,v);
		dis2=vec_get(hlground_data.dist,src);

		if(dis1 == (dis2 + 1))     //next node in layer graph
		{
			int min;
			if(f <=  (e->cap - e->f) )
				min=f;
			else min= e->cap - e->f;

			int df = dinic_dfs(v,min);
			if(df)
			{
				e->f += df;

				Edge e_rev=vec_get( (Vector*)vec_get(hlground_data.graph,v),e->rev);
				e_rev->f -=df;

				vec_set(vec_get(hlground_data.graph,src),i,(LmnWord)e);
				vec_set(vec_get(hlground_data.graph,v),e->rev,(LmnWord)e_rev);

				return df;
			}
		}
	}
	return 0;
}

void init_layer()
{
	if(hlground_data.dist != NULL)
	{
		vec_free(hlground_data.dist);
		hlground_data.dist=vec_make(10);
	}

	unsigned long node_num = hlground_data.counter;
	while(node_num)
	{
			vec_push(hlground_data.dist,0);
			node_num--;
	}

}

void init_workarray()
{
	if(hlground_data.work != NULL)
	{
		vec_free(hlground_data.work);
		hlground_data.work=vec_make(10);
	}

	unsigned long node_num = hlground_data.counter;
	while(node_num)
	{
		vec_push(hlground_data.work,0);
		node_num--;
	}

}

void print_layer()
{
	printf("-------------layer of nodes---------------\n");
	int size=vec_num(hlground_data.dist);
	int i;
	for(i =0; i < size;i++)
	{
		printf("(%d->%lu), ",i,vec_get(hlground_data.dist,i));
	}
	printf("\n-------------              ---------------\n");
}

void init_hlgrounddata()
{
	  /*ID are the index number of hyperlinks and atoms in adjacency list representation of the given graph*/
	  hlground_data.link_id =proc_tbl_make_with_size(64); //hlink ---> its ID
	  hlground_data.id_link =proc_tbl_make_with_size(64); //id ---> hlink
	  hlground_data.atom_id =proc_tbl_make_with_size(64); //atom ---> ID
	  hlground_data.id_atom =proc_tbl_make_with_size(64); //id ---> atom

	  hlground_data.global_hlinks =proc_tbl_make_with_size(64);
	  hlground_data.local_atoms=proc_tbl_make_with_size(64);
	  hlground_data.non_hlground_hlinks=proc_tbl_make_with_size(64);
	  hlground_data.non_hlground_atoms=proc_tbl_make_with_size(64);

	  hlground_data.graph=vec_make(5);
	  hlground_data.dist=vec_make(5);
	  hlground_data.work=vec_make(5);
}

void free_hlgrounddata()
{
	proc_tbl_free(hlground_data.link_id);
	proc_tbl_free(hlground_data.id_link);
	proc_tbl_free(hlground_data.atom_id);
	proc_tbl_free(hlground_data.id_atom);

	proc_tbl_free(hlground_data.non_hlground_hlinks);
	proc_tbl_free(hlground_data.non_hlground_atoms);

	if(!(hlground_data.dist))
		vec_free(hlground_data.dist);
	if(!(hlground_data.work))
			vec_free(hlground_data.work);

	if(!(hlground_data.graph))
	{
		while(!vec_is_empty(hlground_data.graph))
		{
			Vector * node_vec=(Vector*)vec_pop(hlground_data.graph);
			if(!node_vec)
			{
				while(!vec_is_empty(node_vec))
				{
					free((Edge)vec_pop(node_vec));
				}
			}
			vec_free(node_vec);
		}
		vec_free(hlground_data.graph);
	}

}

/*from given graph, build a graph in which hyperlinks are nodes,
 * all atoms, except these pointed by root links, will be ignored
 * */
void build_hlink_graph(Vector *src,
						Vector *avovec,
						ProcessTbl *attr_functors,
						Vector *attr_dataAtoms,
						Vector *attr_dataAtom_attrs)
{
	hlground_data.counter=0;  			//number of nodes

	ProcessTbl visited_atoms=proc_tbl_make_with_size(64);
	ProcessTbl visited_hlinks=proc_tbl_make_with_size(64);

	LinkObj p=(LinkObj)vec_get(src,0);
	LinkObj root_link= LinkObj_make(p->ap,p->pos);

	if(LMN_ATTR_IS_DATA(root_link->pos))
	{
		HyperLink * hl = lmn_hyperlink_at_to_hl(LMN_SATOM(root_link->ap));
		HyperLink * hl_root = lmn_hyperlink_get_root(hl);
		proc_tbl_put_new_hlink(visited_hlinks,hl_root,(LmnWord)hl_root);
	}
	else
	{
		proc_tbl_put_new_atom(visited_atoms, (LmnSAtom)root_link->ap, (LmnWord)root_link->ap);
	}

	//get edges
	collect_edges_dfs(root_link,
			&visited_atoms,
			&visited_hlinks,
			attr_functors,
			attr_dataAtoms,
			attr_dataAtom_attrs
	);

	proc_tbl_free(visited_atoms);
	proc_tbl_free(visited_hlinks);

	//print_graph();

	//add a super source and a super sink (imaginary )
	unsigned long source=hlground_data.counter++;   //super source
	unsigned long target=hlground_data.counter++;   //super sink

	Vector * vec1=vec_make(5);
	vec_push(hlground_data.graph,(LmnWord)vec1);
	Vector * vec2=vec_make(5);
	vec_push(hlground_data.graph,(LmnWord)vec2);

	int i,j;
	BOOL m_hlink=FALSE;
	for(i=0;i<vec_num(src);i++)
	{	//set capacity of edges as zero which are root links
		LmnWord atom=((LinkObj)vec_get(src,i))->ap;
		LmnLinkAttr attr=((LinkObj)vec_get(src,i))->pos;
		unsigned long t_ID,s_ID;  // root_edge(s_ID,t_ID)
		if(LMN_ATTR_IS_DATA(attr))
		{
			m_hlink=TRUE;
			HyperLink * hl = lmn_hyperlink_at_to_hl(LMN_SATOM(atom));
			HyperLink * hl_root = lmn_hyperlink_get_root(hl);
			proc_tbl_get_by_hlink(hlground_data.link_id, hl_root,&t_ID); //get target ID

			LmnWord source_atom;
			source_atom = LMN_SATOM_GET_LINK(atom, 0);
			proc_tbl_get_by_atom(hlground_data.atom_id, (LmnSAtom)source_atom, &s_ID);
		}
		else
		{
			m_hlink=FALSE;
			proc_tbl_get_by_atom(hlground_data.atom_id, (LmnSAtom)atom, &t_ID);

			LmnWord source_atom;
			source_atom = LMN_SATOM_GET_LINK(atom, attr);
			proc_tbl_get_by_atom(hlground_data.atom_id, (LmnSAtom)source_atom, &s_ID);
		}

		// set capacity to zero
		Vector *vec_node_source = (Vector*)vec_get(hlground_data.graph,s_ID);
		Vector *vec_node_target = (Vector*)vec_get(hlground_data.graph,t_ID);


		for(j=0;j<vec_num(vec_node_source);j++)
		{
			Edge temp=(Edge)vec_get(vec_node_source,j);
			if(temp->to == t_ID)
			{
				temp->cap=0;
				temp->f=0;
				vec_set(vec_node_source,j,(LmnWord)temp);
				break;
			}
		}

		for(j=0;j<vec_num(vec_node_target);j++)
		{
			Edge temp=(Edge)vec_get(vec_node_target,j);
			if(temp->to == s_ID)
			{
				temp->cap=0;
				temp->f=0;
				vec_set(vec_node_target,j,(LmnWord)temp);
				break;
			}
		}
		// set capacity to zero

		Edge edge1,edge2; //edges between super source to target
		Edge edge3,edge4;
		if(m_hlink)
		{
			edge1 =make_Edge(t_ID,1);
			edge2 =make_Edge(source,1);

			edge1->rev = vec_num((Vector*)vec_get(hlground_data.graph,t_ID));
			edge2->rev = vec_num((Vector*)vec_get(hlground_data.graph,source));

			vec_push((Vector*)vec_get(hlground_data.graph,source),(LmnWord)edge1);
			vec_push((Vector*)vec_get(hlground_data.graph,t_ID),(LmnWord)edge2);

			///////////////////////
			edge3 =make_Edge(s_ID,INT_MAX/2);
			edge4 =make_Edge(target,INT_MAX/2);

			edge3->rev = vec_num((Vector*)vec_get(hlground_data.graph,s_ID));
			edge4->rev = vec_num((Vector*)vec_get(hlground_data.graph,target));

			vec_push((Vector*)vec_get(hlground_data.graph,target),(LmnWord)edge3);
			vec_push((Vector*)vec_get(hlground_data.graph,s_ID),(LmnWord)edge4);
		}
		else
		{
			edge1 =make_Edge(t_ID,INT_MAX/2);
			edge2 =make_Edge(source,INT_MAX/2);

			edge1->rev = vec_num((Vector*)vec_get(hlground_data.graph,t_ID));
			edge2->rev = vec_num((Vector*)vec_get(hlground_data.graph,source));

			vec_push((Vector*)vec_get(hlground_data.graph,source),(LmnWord)edge1);
			vec_push((Vector*)vec_get(hlground_data.graph,t_ID),(LmnWord)edge2);

			///////////////////////
			edge3 =make_Edge(s_ID,INT_MAX/2);
			edge4 =make_Edge(target,INT_MAX/2);

			edge3->rev = vec_num((Vector*)vec_get(hlground_data.graph,s_ID));
			edge4->rev = vec_num((Vector*)vec_get(hlground_data.graph,target));

			vec_push((Vector*)vec_get(hlground_data.graph,target),(LmnWord)edge3);
			vec_push((Vector*)vec_get(hlground_data.graph,s_ID),(LmnWord)edge4);

		}
	}

	hlground_data.source=target;
	hlground_data.target=source;
	//assert (hlground_data.counter == vec_num(hlground_data.graph));

}

/*add an edge to adjacency list
 * */
void addEdge_hlink_hlink(HyperLink * s_hlink, HyperLink * t_hlink,int cap)
{
	unsigned long s_ID;
	unsigned long t_ID;

	if(!proc_tbl_get_by_hlink(hlground_data.link_id, s_hlink,&s_ID))
	{	//not existed
		s_ID=hlground_data.counter++;    					     //assign ID
		proc_tbl_put_new_hlink(hlground_data.link_id,s_hlink,(LmnWord)s_ID);         //record new ID
		unsigned long link_root_id=LMN_HL_ID(s_hlink);
		proc_tbl_put(hlground_data.id_link,(LmnWord)s_ID,(LmnWord)link_root_id);        //save (id,hlink) pair
		Vector * new_vector=vec_make(5);
		vec_push(hlground_data.graph,(LmnWord)new_vector); //add a vector to store edges of new node
	}

	if(!proc_tbl_get_by_hlink(hlground_data.link_id, t_hlink, &t_ID))
	{  //not existed
		t_ID=hlground_data.counter++;    					     //assign ID
		proc_tbl_put_new_hlink(hlground_data.link_id,t_hlink,(LmnWord)t_ID);         //record new ID
		unsigned long link_root_id=LMN_HL_ID(t_hlink);
		proc_tbl_put(hlground_data.id_link,(LmnWord)t_ID,(LmnWord)link_root_id);        //save (id,hlink) pair
		Vector * new_vector=vec_make(5);
		vec_push(hlground_data.graph,(LmnWord)new_vector); //add a vector to store edges of new node
	}

	if(s_ID == t_ID)   //don't add self loops
		return;

	Edge s_t=make_Edge(t_ID,cap);
	Edge t_s=make_Edge(s_ID,cap);

	s_t->rev=vec_num((Vector*)vec_get(hlground_data.graph,t_ID));
	t_s->rev=vec_num((Vector*)vec_get(hlground_data.graph,s_ID));
	vec_push((Vector*)vec_get(hlground_data.graph,s_ID),(LmnWord)s_t);	//add to adjancy list
	vec_push((Vector*)vec_get(hlground_data.graph,t_ID),(LmnWord)t_s);

}

void addEdge_atom_hlink(LmnWord s_atom, HyperLink * t_hlink,int cap)
{

	unsigned long s_ID;
	unsigned long t_ID;


	if(!proc_tbl_get_by_atom(hlground_data.atom_id, (LmnSAtom)s_atom, &s_ID))
	{	//not existed

		s_ID=hlground_data.counter++;    					     //assign ID
		proc_tbl_put_new_atom(hlground_data.atom_id,(LmnSAtom)s_atom,(LmnWord)s_ID);
		proc_tbl_put(hlground_data.id_atom,(LmnWord)s_ID,s_atom);                     //save (id,atom) pair
		Vector * new_vector=vec_make(5);
		vec_push(hlground_data.graph,(LmnWord)new_vector); //add a vector to store edges of new node

	}

	if(!proc_tbl_get_by_hlink(hlground_data.link_id, t_hlink, &t_ID))
	{  //not existed
		t_ID=hlground_data.counter++;    					     //assign ID
		proc_tbl_put_new_hlink(hlground_data.link_id,t_hlink,(LmnWord)t_ID);         //record new ID
		unsigned long link_root_id=LMN_HL_ID(t_hlink);
		proc_tbl_put(hlground_data.id_link,(LmnWord)t_ID,(LmnWord)link_root_id);        //save (id,hlink) pair

		Vector * new_vector=vec_make(5);
		vec_push(hlground_data.graph,(LmnWord)new_vector); //add a vector to store edges of new node

	}

	if(s_ID == t_ID)   //don't add self loops
		return;

	Edge s_t=make_Edge(t_ID,cap);
	Edge t_s=make_Edge(s_ID,cap);

	s_t->rev=vec_num((Vector*)vec_get(hlground_data.graph,t_ID));
	t_s->rev=vec_num((Vector*)vec_get(hlground_data.graph,s_ID));
	vec_push((Vector*)vec_get(hlground_data.graph,s_ID),(LmnWord)s_t);	//add to adjancy list
	vec_push((Vector*)vec_get(hlground_data.graph,t_ID),(LmnWord)t_s);

}

void addEdge_atom_atom(LmnWord s_atom, LmnWord t_atom ,int cap)
{

	unsigned long s_ID;
	unsigned long t_ID;

	/*examine nodes */
	//check if nodes are added
	if(!proc_tbl_get_by_atom(hlground_data.atom_id, (LmnSAtom)s_atom, &s_ID))
	{	//not existed
		s_ID=hlground_data.counter++;    					     //assign ID
		proc_tbl_put_new_atom(hlground_data.atom_id,(LmnSAtom)s_atom,(LmnWord)s_ID);
		proc_tbl_put(hlground_data.id_atom,(LmnWord)s_ID,s_atom);                     //save (id,atom) pair

		Vector * new_vector=vec_make(5);
		vec_push(hlground_data.graph,(LmnWord)new_vector); //add a vector to store edges of new node

	}

	if(!proc_tbl_get_by_atom(hlground_data.atom_id, (LmnSAtom)t_atom, &t_ID))
	{  //not existed
		t_ID=hlground_data.counter++;    					     //assign ID
		proc_tbl_put_new_atom(hlground_data.atom_id,(LmnSAtom)t_atom,(LmnWord)t_ID);         //record new ID
		proc_tbl_put(hlground_data.id_atom,(LmnWord)t_ID,(LmnWord)t_atom);        //save (id,hlink) pair

		Vector * new_vector=vec_make(5);
		vec_push(hlground_data.graph,(LmnWord)new_vector); //add a vector to store edges of new node

	}

	if(s_ID == t_ID)   //don't add self loops
		return;
	Edge s_t=make_Edge(t_ID,cap);
	Edge t_s=make_Edge(s_ID,cap);

	s_t->rev=vec_num((Vector*)vec_get(hlground_data.graph,t_ID));
	t_s->rev=vec_num((Vector*)vec_get(hlground_data.graph,s_ID));
	vec_push((Vector*)vec_get(hlground_data.graph,s_ID),(LmnWord)s_t);	//add to adjancy list
	vec_push((Vector*)vec_get(hlground_data.graph,t_ID),(LmnWord)t_s);
}

/* collect edges:
 * hyperlinks, source and target atoms(note: not all atoms)
 * considered as nodes and represented by numbers
 * */
void collect_edges_dfs(LinkObj source,             //current link
        			ProcessTbl  *visited_atoms,
        			ProcessTbl  *visited_hlinks,
        			ProcessTbl *attr_functors,
        			Vector *attr_dataAtoms,
        			Vector *attr_dataAtom_attrs)
{

	LmnAtom pre_source;
	LmnLinkAttr pre_source_attr;

	HyperLink * source_root_hl=NULL;
	if(LMN_ATTR_IS_DATA(source->pos))
	{	//current atom is hlink atom
		source_root_hl = lmn_hyperlink_get_root(lmn_hyperlink_at_to_hl(LMN_SATOM(source->ap)));

		//therefore, previous atom is a symbol atom
		pre_source = LMN_SATOM_GET_LINK(source->ap, 0);          //the atom source is coming from
		pre_source_attr = LMN_SATOM_GET_ATTR(source->ap, 0);
	}
	else
	{	//current atom is a symbol atom
		pre_source = LMN_SATOM_GET_LINK(source->ap, source->pos);
		pre_source_attr = LMN_SATOM_GET_ATTR(source->ap, source->pos);

	}

	Vector * neighbours=vec_make(5);
	get_neighbours(NULL,neighbours,source->ap,source->pos,attr_functors,attr_dataAtoms,attr_dataAtom_attrs);

	//without this one neighbour, some edge will be missed
	vec_push(neighbours,(LmnWord)LinkObj_make(pre_source,pre_source_attr));

	while(!vec_is_empty(neighbours))
	{
		LinkObj next=vec_pop(neighbours);
		BOOL visited=FALSE;
		HyperLink * next_root_hl=NULL;

		if(LMN_ATTR_IS_DATA(next->pos))
		{
			next_root_hl = lmn_hyperlink_get_root(lmn_hyperlink_at_to_hl(LMN_SATOM(next->ap)));

			if(proc_tbl_get_by_hlink(*visited_hlinks,next_root_hl,NULL))
				visited=TRUE;
			else
				proc_tbl_put_new_hlink(*visited_hlinks,next_root_hl,(LmnWord)next_root_hl); //set visited
		}
		else
		{
			if(proc_tbl_get_by_atom(*visited_atoms,(LmnSAtom)next->ap,NULL))
				visited=TRUE;
			else
			{
				proc_tbl_put_atom(*visited_atoms, (LmnSAtom)next->ap, (LmnWord)next->ap);
			}
		}

		unsigned long a,b;
		if(source_root_hl)
		{ //source node is an hlink
			a=LMN_HL_ID(source_root_hl);
			if(next_root_hl)
			{	//next node is an hlink  LMN_HL_ID
				b=LMN_HL_ID(next_root_hl);
				if(a < b)  //to get rid of adding same edge twice
				{
					addEdge_hlink_hlink(source_root_hl,next_root_hl,1);
				}

			}
			else
			{  //next node is an atom
				b=(int)next->ap;
				if(a < b)
				{
					addEdge_atom_hlink(next->ap,source_root_hl,1);
				}
			}
		}
		else
		{ //source node is an atom
			a=(int)source->ap;
			if(next_root_hl)
			{	//next node is an hlink
				b=LMN_HL_ID(next_root_hl);
				if(a < b)
				{
					addEdge_atom_hlink(source->ap,next_root_hl,1);
				}

			}
			else
			{  //next node is an atom
				b=(int)next->ap;
				if(a < b)
				{
					addEdge_atom_atom(source->ap,next->ap,INT_MAX/2);
				}

			}
		}

		if(!visited)
		{
			collect_edges_dfs(LinkObj_make(next->ap,next->pos),
								visited_atoms,
								visited_hlinks,
								attr_functors,
								attr_dataAtoms,
								attr_dataAtom_attrs);
		}
		LMN_FREE(next);
	}
	LMN_FREE(source);
	vec_free(neighbours);
}

void print_graph()
{
	if(!(hlground_data.graph))
		return;

	int node_num=vec_num(hlground_data.graph);
	printf("\n");
	printf("--hlground-data: total %d nodes---------------------\n",node_num);

	unsigned long edge_num=0;
	int i;
	for(i=0; i<node_num;i++)
	{
		Vector *head_node=(Vector*)vec_get(hlground_data.graph,i);
		int size=vec_num(head_node);
		printf("-- %dth node has %d edges---\n",i,size);
		printf("       (%d)-->  ",i);
		edge_num +=size;
		int j;
		for(j=0;j<size;j++)
		{
			Edge edge=(Edge)vec_get(head_node,j);

			printf("[%lu, %lu, %d, %d]     ",edge->to,edge->rev,edge->f,edge->cap);
		}

		printf("\n");
	}
	printf("--hlground-data: total %d edges---------------------\n",edge_num/2);
	printf("\n");
}

int nodenum_graph()
{
	//assert (hlground_data.counter == vec_num(hlground_data.graph));
	return vec_num(hlground_data.graph);
}

unsigned long edgenum_graph()
{
	//assert (hlground_data.counter == vec_num(hlground_data.graph));
	int node_num=vec_num(hlground_data.graph);
	unsigned long edge_num=0;
	int i;
	for(i=0; i<node_num;i++)
	{
		Vector *head_node=vec_get(hlground_data.graph,i);
		int size=vec_num(head_node);
		edge_num +=size;
	}
	return edge_num/2;
}
/*
 * search for a pure cycle paths, if find such path return true
 * */
BOOL purecycle_exit(Vector *srcvec, Vector *avovec)
{
	BOOL m_find=FALSE;

	int m=vec_num(srcvec);
	int i;
	for(i=0;i<m;i++)
	{
		Vector * stack=vec_make(10);
		LinkObj root = LinkObj_make(((LinkObj)vec_get(srcvec,i))->ap,((LinkObj)vec_get(srcvec,i))->pos);
		ProcessTbl atoms=proc_tbl_make_with_size(64);
		BOOL m_first=TRUE;

		vec_push(stack,(LmnWord)LinkObj_make(root->ap,root->pos));

		while(!vec_is_empty(stack))
		{
			LinkObj cur_link=(LinkObj)vec_pop(stack);

			//printf(" cur_link =%d , %d --------\n",cur_link->ap,cur_link->pos);
			if(cur_link->ap == root->ap  && cur_link->pos ==root->pos && !m_first)
			{
				m_find= TRUE;
				break;
			}

			if(LMN_ATTR_IS_DATA(cur_link->pos))
				continue;			//only go on regular links

			 if (proc_tbl_get_by_atom(atoms, (LmnSAtom)cur_link->ap, NULL))
			 {
			        continue;
			 }

			proc_tbl_put_atom(atoms, (LmnSAtom)cur_link->ap, (LmnWord)cur_link->ap);

			Vector * neighbours = vec_make(4);
			get_neighbours(NULL,
					neighbours,
					cur_link->ap,
					cur_link->pos,
					NULL,
					NULL,
					NULL);
			LMN_FREE(cur_link);

			while(!vec_is_empty(neighbours))
			{
				LinkObj Link=vec_pop(neighbours);
				//printf(" next_link =%d , %d --------\n",Link->ap,Link->pos);
				vec_push(stack,(LmnWord)Link);
			}
			vec_free(neighbours);
			m_first=FALSE;
		}

		LMN_FREE(root);
		proc_tbl_free(atoms);

		if(!stack)
		{
			while(!vec_is_empty(stack) )
			{
				LinkObj Link=(LinkObj)vec_pop(stack);
				LMN_FREE(Link);
			}
			vec_free(stack);
		}
		if(m_find)
			break;
	}

	return m_find;
}

/*check if there is a cycle, if yes return true
 * */
BOOL cycle_exist(Vector *srcvec,
			Vector *avovec,
			ProcessTbl  *attr_functors,
			Vector   *attr_dataAtoms,
			Vector   *attr_dataAtom_attrs)
{
	BOOL m_find=FALSE;

	int m=vec_num(srcvec);
	int i;
	for(i=0;i<m;i++)
	{
		Vector * stack=vec_make(10);
		LinkObj root = LinkObj_make(((LinkObj)vec_get(srcvec,i))->ap,((LinkObj)vec_get(srcvec,i))->pos);
		ProcessTbl atoms=proc_tbl_make_with_size(64);
		ProcessTbl hlinks=proc_tbl_make_with_size(64);
		BOOL m_first=TRUE;
		BOOL m_tempfind=FALSE;
		vec_push(stack,(LmnWord)LinkObj_make(root->ap,root->pos));

		while(!vec_is_empty(stack))
		{
			LinkObj cur_link=(LinkObj)vec_pop(stack);
			//printf(" cur_link =%d , %d --------\n",cur_link->ap,cur_link->pos);
			if(cur_link->ap == root->ap  && cur_link->pos ==root->pos && !m_first )
			{
				m_find= TRUE;
				//printf(" find a cycle --------\n");
				break;
			}

			if(LMN_ATTR_IS_DATA(cur_link->pos))
			{
				HyperLink *hl = lmn_hyperlink_at_to_hl(LMN_SATOM(cur_link->ap));
				if(proc_tbl_get_by_hlink(hlinks, lmn_hyperlink_get_root(hl), NULL))
					continue;
				else
					proc_tbl_put_new_hlink(hlinks, lmn_hyperlink_get_root(hl), (LmnWord)hl);
			}
			else
			{
				if (proc_tbl_get_by_atom(atoms, (LmnSAtom)cur_link->ap, NULL))
					continue;
				else
					proc_tbl_put_atom(atoms, (LmnSAtom)cur_link->ap, (LmnWord)cur_link->ap);
			}



			Vector * neighbours = vec_make(4);
			get_neighbours(NULL,
						neighbours,
						cur_link->ap,
						cur_link->pos,
						attr_functors,
						attr_dataAtoms,
						attr_dataAtom_attrs);
			LMN_FREE(cur_link);

			while(!vec_is_empty(neighbours))
			{
				LinkObj Link=vec_pop(neighbours);
				vec_push(stack,(LmnWord)Link);
			}
			vec_free(neighbours);
			m_first=FALSE;

			if(m_find)
				break;
		}

		if(!stack)
		{
			while(!vec_is_empty(stack) )
			{
				LinkObj Link=(LinkObj)vec_pop(stack);
				LMN_FREE(Link);
			}
			vec_free(stack);
		}

		LMN_FREE(root);
		proc_tbl_free(atoms);
		proc_tbl_free(hlinks);

		if(m_find)
			break;
	}

	return m_find;
}

/*a path is made of several LinkObj*/
void free_path(Vector * path)
{
	if(path == NULL)
		return;
	//printf("to free %d linkobj \n",vec_num(path));
	//print_path(path);
	while(!vec_is_empty(path))
	{
		LinkObj Link=(LinkObj)vec_pop(path);
		LMN_FREE(Link);
	}
	vec_free(path);
}

/* clean multiple paths */
void free_paths(Vector * paths)
{
	if(paths == NULL)
			return;
	//printf("to free %d paths \n",vec_num(paths));

	while(!vec_is_empty(paths))
	{
		Vector * m_vec=(Vector *)vec_pop(paths);
		if(m_vec !=NULL)
		{
			free_path(m_vec);
		}
	}
	vec_free(paths);
}

/* return the local atoms which are within hlground
 * parameters: root link,
 *             other links of source atom,
 *             global hyperlinks,
 *	           get the number of local atoms +  number of hyperlinks(no idea why, just to keep it consistent with previous work )
 *	           hyperlinks attribute related,
 *	           hyperlinks attribute related,
 *	           hyperlinks attribute related,
 * */
void   get_local_atoms(LinkObj root_link,
							  Vector       * avovec,
							  ProcessTbl    *global_links,
							  ProcessTbl    *atoms,
							  unsigned long * natoms,
							  ProcessTbl  *attr_functors,
							  Vector   *attr_dataAtoms,
							  Vector   *attr_dataAtom_attrs)
{

	*natoms=0;
	ProcessTbl visited_hl=proc_tbl_make_with_size(10);

	Vector *stack=vec_make(10);
	vec_push(stack,(LmnWord)LinkObj_make(root_link->ap,root_link->pos));

	while(!vec_is_empty(stack))
	{
		LinkObj cur_link=vec_pop(stack);
		LmnAtom m_atom=cur_link->ap;
		LmnLinkAttr m_pos=cur_link->pos;
		LMN_FREE(cur_link);

		Vector * neighbours=vec_make(4);
		if(LMN_ATTR_IS_DATA(m_pos)) // current link is a hyperlink
		{
			if(m_pos == LMN_HL_ATTR)
			{
				HyperLink * hl = lmn_hyperlink_at_to_hl(LMN_SATOM(m_atom));
				if(!proc_tbl_get_by_hlink(visited_hl, lmn_hyperlink_get_root(hl), NULL))
				{ // if not visited
					proc_tbl_put_new_hlink(visited_hl, lmn_hyperlink_get_root(hl), (LmnWord)hl);

					if(!proc_tbl_get_by_hlink(*global_links, lmn_hyperlink_get_root(hl), NULL))
					{//if current hyperlink is not global hyperlink

						get_neighbours(avovec,
								neighbours,
								m_atom,
								m_pos,
								attr_functors,
								attr_dataAtoms,
								attr_dataAtom_attrs);

					}
				}
			}
			else
			{
				//printf("what else it could be?:error \n");
			}
			(*natoms)++; // count just like previous version of hlground
		}
		else                        //current link is a hyperlink
		{
			if (proc_tbl_get_by_atom(*atoms, (LmnSAtom)m_atom, NULL))
			{
				continue;
			}
			proc_tbl_put_atom(*atoms, (LmnSAtom)m_atom, (LmnWord)m_atom);
			(*natoms)++;

			get_neighbours(avovec,
					neighbours,
					m_atom,
					m_pos,
					attr_functors,
					attr_dataAtoms,
					attr_dataAtom_attrs);

		}


		while(!vec_is_empty(neighbours))
		{
			LinkObj temp_link=(LinkObj)vec_pop(neighbours);
			LmnAtom next_ap=temp_link->ap;
			LmnLinkAttr next_pos=temp_link->pos;
			vec_push(stack,(LmnWord)LinkObj_make(next_ap,next_pos));
			LMN_FREE(temp_link);
		}
		vec_free(neighbours);
	}

	proc_tbl_free(visited_hl);
	vec_free(stack);

}


/* get_neighbours: finds neighbors of current link object
 * neighbors do not include the atom from which the link is coming out
 * */
void get_neighbours(Vector  *avovec,
					    Vector *neighbours,
						LmnAtom atom,
						LmnLinkAttr pos,
						ProcessTbl  *attr_functors,
						Vector   *attr_dataAtoms,
						Vector   *attr_dataAtom_attrs)
{
	if (LMN_ATTR_IS_DATA(pos))
	{
		HyperLink *hl = lmn_hyperlink_at_to_hl(LMN_SATOM(atom));
		BOOL flg_search_hl = FALSE;  //True is hlink attribute matches with hlground attribute

		if (!LMN_HL_HAS_ATTR(hl)) //if hlink has no attribute
		{
			if(vec_num(attr_dataAtoms) == 0)  //hlground has no attribute
			{//means,  matched
				flg_search_hl=TRUE;
			}
		}

		{   // check if hlink attribute matches with hlground attribute
			LmnAtom attrAtom = LMN_HL_ATTRATOM(hl);
			LmnLinkAttr attr = LMN_HL_ATTRATOM_ATTR(hl);
			int i;
			for (i = 0; i < vec_num(attr_dataAtoms); i++)
			{
				if (lmn_eq_func(attrAtom, attr, vec_get(attr_dataAtoms, i), vec_get(attr_dataAtom_attrs, i)))
				{
					flg_search_hl = TRUE;
					break;
				}
				else
				{
					continue;
				}
			}
		}

		/*
		if (!LMN_HL_HAS_ATTR(hl))
		{	// if attribute is not specified
			flg_search_hl = TRUE;
		}
		else
		{
			LmnAtom attrAtom = LMN_HL_ATTRATOM(hl);
			LmnLinkAttr attr = LMN_HL_ATTRATOM_ATTR(hl);
			if (LMN_ATTR_IS_DATA(attr))
			{
				int i;
				for (i = 0; i < vec_num(attr_dataAtoms); i++)
				{
					if (lmn_eq_func(attrAtom, attr, vec_get(attr_dataAtoms, i), vec_get(attr_dataAtom_attrs, i)))
					{
						flg_search_hl = TRUE;
						break;
					}
					else
					{
						continue;
					}
				}
			}
			else
			{
				if (proc_tbl_get(*attr_functors, LMN_SATOM_GET_FUNCTOR(LMN_SATOM(attrAtom)), NULL))
				{
					flg_search_hl = TRUE;
				}
			}
		}
		*/

		//explore this hyperlink sublinks
		if (flg_search_hl)
		{
			Vector * hl_childs = vec_make(16);

			lmn_hyperlink_get_elements(hl_childs, hl);
			int child_num = vec_num(hl_childs) - 1;
			LmnMembrane *mem = hl->mem;

			int i;
			for (i = 0; i < child_num; i++)
			{
				if (mem!=((HyperLink *)vec_get(hl_childs, i))->mem)
				{
					continue;
				}
				/*   a(!H),b(!H).
				 *   a--->!---H--->!--->b
				 *   symbol atoms : a,b
				 *   data atoms   : !
				 *   'hlAtom' is a part of linkobject       : H--->!
				 *   'linked_hlAtom' is a part of linkobject: !--->b
				 * */
				LmnSAtom hlAtom = ((HyperLink *)vec_get(hl_childs, i))->atom; /* from hyperlink core points to ! atom  */
				LmnAtom linked_hlAtom;
				LmnLinkAttr linked_attr;

				if(hlAtom != NULL)
				{
					linked_hlAtom = LMN_SATOM_GET_LINK(hlAtom, 0);
					linked_attr = LMN_SATOM_GET_ATTR(hlAtom, 0);
					vec_push(neighbours,(LmnWord)LinkObj_make(linked_hlAtom,linked_attr));
				}

			}
			vec_free(hl_childs);
		}
	}
	else
	{  //get regular link children
		int i;
			for (i = 0; i < LMN_SATOM_GET_ARITY(atom); i++)
			{
				if (i == pos)	//don't add this link itself
					continue;

				LinkObj new_obj;
				new_obj=LinkObj_make(LMN_SATOM_GET_LINK(atom, i),LMN_SATOM_GET_ATTR(atom, i));

				vec_push(neighbours,(LmnWord)LinkObj_make(new_obj->ap,new_obj->pos));

			}
	}
}


/* x縺ｨy縺�1縺､縺ｮ繝ｪ繝ｳ繧ｯ縺ｮ騾�蜷代″陦ｨ迴ｾ縺九←縺�縺� */
#define IS_REVERSE_LINK(xap, xattr, yap, yattr)                                       \
      ( (LMN_SATOM_GET_LINK(xap, xattr) == yap) &&                             \
        (LMN_SATOM_GET_ATTR(xap, xattr) == yattr) )



/* x縺ｨy縺�1縺､縺ｮ繝ｪ繝ｳ繧ｯ縺ｮ騾�蜷代″陦ｨ迴ｾ縺九←縺�縺� */
#define IS_BUDDY(xap, xattr, yap, yattr)                                       \
      ( !LMN_ATTR_IS_DATA(xattr)                &&                             \
        !LMN_ATTR_IS_DATA(yattr)                &&                             \
        (LMN_SATOM_GET_LINK(xap, xattr) == yap) &&                             \
        (LMN_SATOM_GET_ATTR(xap, xattr) == yattr) )

/* srcvec縺九ｉ蜃ｺ繧九Μ繝ｳ繧ｯ縺ｮ繝ｪ繧ｹ繝医′蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｫ蛻ｰ驕斐＠縺ｦ縺�繧句�ｴ蜷医��
 * avovec縺ｫ蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｫ蟄伜惠縺吶ｋ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縲］atoms縺ｫ繧｢繝医Β縺ｮ謨ｰ縲∵綾繧雁�､縺ｫ逵溘ｒ霑斐☆縲�
 * 繝ｪ繧ｹ繝医′蝓ｺ蠎暮��繝励Ο繧ｻ繧ｹ縺ｫ蛻ｰ驕斐＠縺ｦ縺�縺ｪ縺�蝣ｴ蜷医↓縺ｯatoms縺ｯNULL縺ｨ縺ｪ繧翫�∝⊃繧定ｿ斐☆縲�
 * 縺溘□縺励�√Μ繝ｳ繧ｯ縺径vovec縺ｫ蛻ｰ驕斐＠縺ｦ縺�繧句�ｴ蜷医↓縺ｯ縲∝渕蠎暮��繝励Ο繧ｻ繧ｹ縺ｨ縺ｯ縺ｪ繧峨↑縺�縲�
 * hlinks縺君ULL縺ｧ縺ｪ縺代ｌ縺ｰhlground縺ｨ縺励※謗｢邏｢
 * attr_functors, attr_dataAtoms, attr_dataAtoms_attr縺ｯhlground縺ｮ螻樊�ｧ */
BOOL ground_atoms(Vector        *srcvec,
                  Vector        *avovec,
                  ProcessTbl    *atoms,/* ground蜀�縺ｮ逋ｺ隕区ｸ医∩縺ｮ繧ｷ繝ｳ繝懊Ν繧｢繝医Β collects atom within hlground */
                  unsigned long *natoms,/* ground蜀�縺ｮ繧｢繝医Β縺ｮ蛟区焚  number of atoms within hlground */
                  ProcessTbl    *hlinks,/* hlinks!=NULL縺ｪ繧峨�”lground縺ｨ縺励※謗｢邏｢ collects hyperlinks local to hlground  */
                  ProcessTbl    *attr_functors,/* hlground縺ｮ螻樊�ｧ�ｼ�unary atom�ｼ� used when attribute is specified */
                  Vector        *attr_dataAtoms,/* hlground縺ｮ螻樊�ｧ�ｼ�data atom�ｼ� used when attribute is specified */
                  Vector        *attr_dataAtom_attrs/* hlground縺ｮ螻樊�ｧ(data atom縺ｮ螻樊�ｧ) used when attribute is specified */
                  )
{

  Vector *unsearched_link_stack;         /* 謗｢邏｢蠕�縺｡繝ｪ繝ｳ繧ｯ */
  //ProcessTbl found_ground_symbol_atoms;  /* ground蜀�縺ｮ逋ｺ隕区ｸ医∩縺ｮ繧ｷ繝ｳ繝懊Ν繧｢繝医Β */
  //unsigned long count_of_ground_atoms;   /* ground蜀�縺ｮ繧｢繝医Β縺ｮ蛟区焚 */
  int i;
  int reached_root_count; /* 蛻ｰ驕斐＠縺滓�ｹ縺ｮ蛟区焚(1縺､縺ｯ蟋狗せ) */
  BOOL result;
  HyperLink *hl;
  LmnMembrane *mem;
  Vector *hl_childs;
  int element_num;

  *natoms = 0;
  //*atoms = proc_tbl_make_with_size(64);
  unsearched_link_stack = vec_make(16);
  reached_root_count = 1;
  BOOL pure_path=TRUE;
  result = TRUE;

  if (hlinks!=NULL)
  {
    *hlinks = proc_tbl_make_with_size(64);
  }



  /* ground縺ｯ縺､縺ｪ縺後▲縺溘げ繝ｩ繝輔↑縺ｮ縺ｧ1縺､縺ｮ譬ｹ縺九ｉ縺�縺代◆縺ｩ繧後�ｰ繧医＞ */
  /* since ground is a graph which can be traced from one root*/
  {
    LinkObj l = (LinkObj)vec_get(srcvec, 0);
    vec_push(unsearched_link_stack, (LmnWord)LinkObj_make(l->ap, l->pos));
  }


  int k=1;
  int atom_num=0;

  while (!vec_is_empty(unsearched_link_stack))
  {

    LinkObj l;
    LmnAtom l_ap;
    LmnLinkAttr l_pos;

    l = (LinkObj)vec_pop(unsearched_link_stack); /*a link object pointing to an atom*/
    l_ap = l->ap;    /*the atom pointed by the link object*/
    l_pos = l->pos;  /*link position of the atom pointed by the link object*/
    LMN_FREE(l);


    if (LMN_ATTR_IS_DATA(l_pos))   /*if data atom*/
    { /* l縺後ョ繝ｼ繧ｿ縺ｪ繧芽｡後″豁｢縺ｾ繧撹lground縺ｮ蝣ｴ蜷医�ｯ繝上う繝代�ｼ繝ｪ繝ｳ繧ｯ繧｢繝医Β繧呈爾邏｢*/
      //printf("*** link is pointing  a hyperlink internal atom  *** \n");
      if (lmn_data_atom_is_ground(l_ap, l_pos, hlinks))
      {
        if (l_pos == LMN_HL_ATTR) /*l_pos is a hyperlink attribute*/
        {

          hl = lmn_hyperlink_at_to_hl(LMN_SATOM(l_ap));

          if (hlinks != NULL && !proc_tbl_get_by_hlink(*hlinks, lmn_hyperlink_get_root(hl), NULL))
          { /* checking if hl is stored in hlinks */
            BOOL flg_search_hl = FALSE;
            BOOL continue_flag = FALSE;
            pure_path=FALSE;
            /* l縺径vovec縺ｫ縺､縺ｪ縺後▲縺ｦ縺�繧後�ｰ */
            for (i = 0; avovec != NULL && i < vec_num(avovec); i++)
            {
              LinkObj a = (LinkObj)vec_get(avovec, i);
              if (IS_REVERSE_LINK(l_ap, 0, a->ap, a->pos))
              {

            	result = FALSE;
                goto returning;
              }
            }

            /* l縺茎rcvec縺ｫ縺､縺ｪ縺後▲縺ｦ縺�繧後�ｰ */
            for (i = 0; i < vec_num(srcvec); i++)
            {
              LinkObj a = (LinkObj)vec_get(srcvec, i);
              if (IS_REVERSE_LINK(l_ap, 0, a->ap, a->pos))
              {

                reached_root_count++;
                continue_flag = TRUE;
                break;
              }
            }
            if (!continue_flag)
            {
              if (!LMN_HL_HAS_ATTR(hl))
              {//螻樊�ｧ繧呈戟縺｣縺ｦ縺�縺ｪ縺�蝣ｴ蜷医�ｯ辟｡譚｡莉ｶ縺ｫ謗｢邏｢
                flg_search_hl = TRUE;
              }
              else
              {
                LmnAtom attrAtom = LMN_HL_ATTRATOM(hl);
                LmnLinkAttr attr = LMN_HL_ATTRATOM_ATTR(hl);
                if (LMN_ATTR_IS_DATA(attr))
                {

                  for (i = 0; i < vec_num(attr_dataAtoms); i++)
                  {
                    if (lmn_eq_func(attrAtom, attr, vec_get(attr_dataAtoms, i), vec_get(attr_dataAtom_attrs, i)))
                    {
                      flg_search_hl = TRUE;
                      break;
                    }
                    else
                    {
                      continue;
                    }
                  }
                }
                else
                {
                  if (proc_tbl_get(*attr_functors, LMN_SATOM_GET_FUNCTOR(LMN_SATOM(attrAtom)), NULL))
                  {
                    flg_search_hl = TRUE;
                  }
                } 
              }
            }

            if (flg_search_hl)
            {

              proc_tbl_put_new_hlink(*hlinks, lmn_hyperlink_get_root(hl), (LmnWord)hl);
              hl_childs = vec_make(16);

              lmn_hyperlink_get_elements(hl_childs, hl);
              element_num = vec_num(hl_childs) - 1;
              mem = hl->mem;


              for (i = 0; i < element_num; i++)
              {
                if (mem!=((HyperLink *)vec_get(hl_childs, i))->mem)
                {//蛻･縺ｮ閹懊↓遘ｻ蜍輔＠縺溘ｉFALSE縺ｫ
                  // 蠖灘�昴＠縺ｦ縺�縺溘′�ｼ後ワ繧､繝代�ｼ繝ｪ繝ｳ繧ｯ謗･邯壹�ｮ蝣ｴ蜷医�ｯ蜷御ｸ�閹懷��縺ｮ讒矩��縺ｨ繝槭ャ繝√☆繧九ｈ縺�縺ｫ螟画峩
                  // result = FALSE;
                  // goto returning;
                  continue;
                }
                /*   a(!H),b(!H).
                 *   a--->!---H--->!--->b
                 *   symbol atoms : a,b
                 *   data atoms   : !
                 *   hlAtom       : H--->!
                 *   linked_hlAtom: !--->b
                 * */
                LmnSAtom hlAtom = ((HyperLink *)vec_get(hl_childs, i))->atom; /* from hyperlink core points to ! atom  */
                LmnAtom linked_hlAtom = LMN_SATOM_GET_LINK(hlAtom, 0);        /* sub-link;from ! atom pointing to a symbol atom */
                LmnLinkAttr linked_attr = LMN_SATOM_GET_ATTR(hlAtom, 0);


                vec_push(unsearched_link_stack,
                    (LmnWord)LinkObj_make(LMN_SATOM_GET_LINK(hlAtom, 0),
                    					  LMN_SATOM_GET_ATTR(hlAtom, 0)));

                if (!LMN_ATTR_IS_DATA(linked_attr))
                {

                  int j;
                  /* l縺径vovec縺ｫ縺､縺ｪ縺後▲縺ｦ縺�繧後�ｰ */
                  for (j = 0; avovec != NULL && j < vec_num(avovec); j++)
                  {
                    LinkObj a = (LinkObj)vec_get(avovec, j);
                    if (IS_REVERSE_LINK(linked_hlAtom, linked_attr, a->ap, a->pos))
                    {
                      result = FALSE;
                      goto returning;
                    }
                  }

                  /* l縺茎rcvec縺ｫ縺､縺ｪ縺後▲縺ｦ縺�繧後�ｰ */
                  continue_flag = FALSE;
                  for (j = 0; j < vec_num(srcvec); j++)
                  {

                    LinkObj a = (LinkObj)vec_get(srcvec, j);
                    if (IS_REVERSE_LINK(linked_hlAtom, linked_attr, a->ap, a->pos))
                    {
                      reached_root_count++;
                      continue_flag = TRUE;
                      break;
                    }
                  }
                  if (continue_flag)  /*DFS returned to root link, remove twin link of that root link from stack, keep searching */
                	  vec_pop(unsearched_link_stack);
                }

              }

              vec_free(hl_childs);
            }
          }
        }
        (*natoms)++;

        continue;
      }
      else
      {
        result = FALSE; /* ground縺ｧ縺ｪ縺�繝�繝ｼ繧ｿ縺悟�ｺ迴ｾ縺励◆繧臥ｵゆｺ� */
        goto returning;
      }
    }
    else
    { /* l縺後す繝ｳ繝懊Ν繧｢繝医Β繧呈欠縺励※縺�繧後�ｰ l is pointing to a symbol atom */
      //printf("*** link is pointing a regular atom *** \n");
      /* l縺径vovec縺ｫ縺､縺ｪ縺後▲縺ｦ縺�繧後�ｰ */
      for (i = 0; avovec != NULL && i < vec_num(avovec); i++)
      {
        LinkObj a = (LinkObj)vec_get(avovec, i);

        if (IS_REVERSE_LINK(l_ap, l_pos, a->ap, a->pos))
        {
          result = FALSE;
          goto returning;
        }

      }

      /* l縺茎rcvec縺ｫ縺､縺ｪ縺後▲縺ｦ縺�繧後�ｰ */
      {
        if (LMN_SATOM_GET_ATTR(l_ap, l_pos)!=LMN_HL_ATTR)
        {

          BOOL continue_flag = FALSE;
          for (i = 0; i < vec_num(srcvec); i++)
          {

            LinkObj a = (LinkObj)vec_get(srcvec, i);
            if (IS_REVERSE_LINK(l_ap, l_pos, a->ap, a->pos))
            {
              reached_root_count++;
              continue_flag = TRUE;
              break;
            }
          }
        if (continue_flag) /*DFS returned to root link, remove twin link of that root link from stack, keep searching */
        	continue;
        }
      }

      /* l縺後�励Ο繧ｭ繧ｷ繧呈欠縺励※縺�繧後�ｰ if l pointing to proxy atom*/
      if (LMN_SATOM_IS_PROXY(l_ap))
      {
        result = FALSE;
        goto returning;


      }

      /* l縺ｮ謖�縺吶い繝医Β縺瑚ｨｪ蝠乗ｸ医∩縺ｪ繧� */
      /*if the atom pointed by l is already visited*/
      if (proc_tbl_get_by_atom(*atoms, (LmnSAtom)l_ap, NULL))
      {
        continue;
      }


      /* l縺ｯ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺ｧ蛻晏�ｺ縺ｮ繧｢繝医Β繧呈欠縺吶◆繧�,縺昴�ｮ蜈医ｒ謗｢邏｢縺吶ｋ蠢�隕√′縺ゅｋ */
      /*since l points to the first symbol atom, should traverse (what??)*/
      /*storing visited atoms (considered within hlground) to atoms */
      proc_tbl_put_atom(*atoms, (LmnSAtom)l_ap, (LmnWord)l_ap);
      (*natoms)++;
      atom_num++;

      for (i = 0; i < LMN_SATOM_GET_ARITY(l_ap); i++)
      {
        if (i == l_pos)
        {
        	if(LMN_SATOM_GET_ARITY(l_ap)==1)
        	{
        		pure_path=TRUE;
        		//printf(" ***    a local path is ended ***\n");
        	}
        	continue;
        }
        vec_push(unsearched_link_stack,
        	                 (LmnWord)LinkObj_make(LMN_SATOM_GET_LINK(l_ap, i),
        	                                       LMN_SATOM_GET_ATTR(l_ap, i)));

      }

    }
  }



  //printf("                                                                            \n");
  /* 繧ゅ＠譛ｪ蛻ｰ驕斐�ｮ譬ｹ縺後≠繧後�ｰ邨仙粋繧ｰ繝ｩ繝輔↓縺ｪ縺｣縺ｦ縺�縺ｪ縺�縺ｮ縺ｧground縺ｧ縺ｪ縺� */
  result = (reached_root_count == vec_num(srcvec));

 returning:


  for (i = 0; i < vec_num(unsearched_link_stack); i++)
  {
    LMN_FREE(vec_get(unsearched_link_stack, i));
  }
  vec_free(unsearched_link_stack);



  if (result)
  {
    //*atoms = found_ground_symbol_atoms;
    //*natoms += count_of_ground_atoms;
  }
  else
  {
	/*
    if (hlinks !=NULL)
    {
      proc_tbl_free(*hlinks);
      *hlinks = NULL;
    }
    proc_tbl_free(*atoms);
    *atoms = NULL;
    *natoms = 0;
  */
  }
  //printf("               natom=%d \n",(int)(*natoms));
  return result;
}

/* 蜑阪�ｮ螳溯｣�.縺励�ｰ繧峨￥谿九＠縺ｦ縺翫￥ */
BOOL ground_atoms_old(Vector *srcvec,
                      Vector *avovec,
                      HashSet **atoms,
                      unsigned long *natoms)
{
  HashSet *visited = hashset_make(16);
  SimpleHashtbl *guard = NULL;
  HashSet *root = hashset_make(16);
  Vector *stack = vec_make(16);
  unsigned int i;
  unsigned int n; /* 蛻ｰ驕斐＠縺溘い繝医Β縺ｮ謨ｰ */
  unsigned int n_root_data;   /* 譬ｹ縺ｫ縺ゅｋ繝�繝ｼ繧ｿ繧｢繝医Β縺ｮ謨ｰ */
  unsigned int n_reach_root;  /* 蛻ｰ驕斐＠縺滓�ｹ縺ｮ謨ｰ */
  BOOL ok;

  n = 0;
  n_reach_root = 0;
  n_root_data = 0;
  ok = TRUE;
  for (i = 0; i < vec_num(srcvec); i++) {
    LinkObj l = (LinkObj)vec_get(srcvec, i);
    if (LMN_ATTR_IS_DATA(l->pos)) {
      if (lmn_data_atom_is_ground(l->ap, l->pos, NULL)) {
        n_reach_root++;
        n++;
        n_root_data++;
        continue;
      } else {
        ok = FALSE;
        break;
      }
    }

    /* 閾ｪ蟾ｱ繝ｫ繝ｼ繝励＠縺ｦ縺�繧九Μ繝ｳ繧ｯ縺ｯ辟｡隕悶☆繧� */
    if (l->ap == LMN_SATOM_GET_LINK(l->ap, l->pos)) {
      n_reach_root++;
      continue;
    }

    hashset_add(root, (HashKeyType)LMN_SATOM_GET_LINK(l->ap, l->pos));
    if (vec_num(stack) == 0) {
      vec_push(stack, l->ap);
    }
  }

  if (ok) {
    if (avovec != NULL && vec_num(avovec) > 0) {
      guard = hashtbl_make(16);
      for (i = 0; i < vec_num(avovec); i++) {
        LinkObj l = (LinkObj)vec_get(avovec, i);
        if (!LMN_ATTR_IS_DATA(l->pos)) {
          hashtbl_put(guard, l->ap, (HashValueType)l->pos);
        }
      }
    }

    if (n_root_data == 0) {
      while (vec_num(stack) > 0) {
        LmnSAtom src_atom = LMN_SATOM(vec_pop(stack));
        if (LMN_SATOM_IS_PROXY(src_atom)) { ok = FALSE; break; }
        if (hashset_contains(visited, (HashKeyType)src_atom)) continue;
        if (hashset_contains(root, (HashKeyType)src_atom)) {n_reach_root++; continue; }
        hashset_add(visited, (HashKeyType)src_atom);
        n++;
        for (i = 0; i < LMN_SATOM_GET_ARITY(src_atom); i++) {
          LmnAtom next_src = LMN_SATOM_GET_LINK(src_atom, i);
          LmnLinkAttr next_attr = LMN_SATOM_GET_ATTR(src_atom, i);

          if (!LMN_ATTR_IS_DATA(next_attr)) {
            if (guard) {
              int t = hashtbl_get_default(guard, (HashKeyType)next_src, -1);
              if (t >= 0 && t == LMN_ATTR_GET_VALUE(next_attr)) {
                ok = FALSE;
                break;
              }
            }
            vec_push(stack, next_src);
          } else if (lmn_data_atom_is_ground(next_src, next_attr, NULL)) {
            n++;
          } else {
            ok = FALSE;
            break;
          }
        }
      }
    } else if (vec_num(stack) >= 2 && n_root_data > 0) {
      ok = FALSE;
    } else if (vec_num(stack) >= 3) {
      ok = FALSE;
    }
  }

  vec_free(stack);
  hashset_free(root);
  if (guard) hashtbl_free(guard);

  if (ok && n_reach_root == vec_num(srcvec)) {
    *natoms = n;
    *atoms = visited;
    return TRUE;
  }
  else {
    hashset_free(visited);
    *atoms = NULL;
    return FALSE;
  }
}

int mem_remove_symbol_atom_with_buddy_data_f(LmnWord _k,
                                             LmnWord _v,
                                             LmnWord _arg)
{
  mem_remove_symbol_atom_with_buddy_data((LmnMembrane *)_arg, (LmnSAtom)_v);
  return 1;
}

void lmn_mem_remove_ground(LmnMembrane *mem, Vector *srcvec)
{
  ProcessTbl atoms=proc_tbl_make_with_size(64);
  unsigned long i, t;

  ground_atoms(srcvec, NULL, &atoms, &t, NULL, NULL, NULL, NULL);
  proc_tbl_foreach(atoms, mem_remove_symbol_atom_with_buddy_data_f, (LmnWord)mem);

  /* atoms縺ｯ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺励°蜷ｫ縺ｾ縺ｪ縺�縺ｮ縺ｧ縲�
   * srcvec縺ｮ繝ｪ繝ｳ繧ｯ縺檎峩謗･繝�繝ｼ繧ｿ繧｢繝医Β縺ｫ謗･邯壹＠縺ｦ縺�繧句�ｴ蜷医�ｮ蜃ｦ逅�繧偵☆繧� */
  for (i = 0; i < vec_num(srcvec); i++) {
    LinkObj l = (LinkObj)vec_get(srcvec, i);
    if (LMN_ATTR_IS_DATA_WITHOUT_EX(l->pos)) {
      lmn_mem_remove_data_atom(mem, l->ap, l->pos);
    } else if (LMN_ATTR_IS_EX(l->pos)) {
      mem_remove_symbol_atom(mem, LMN_SATOM(l->ap));
    }
  }
  proc_tbl_free(atoms);
}

void lmn_mem_remove_hlground(LmnMembrane *mem,
                             Vector *srcvec,
                             ProcessTbl *attr_sym,
                             Vector *attr_data,
                             Vector *attr_data_at)
{
  ProcessTbl atoms=hlground_data.local_atoms;

  unsigned long i, t;

  //ground_atoms(srcvec, NULL, &atoms, &t, &hlinks, attr_sym, attr_data, attr_data_at);

  proc_tbl_foreach(atoms, mem_remove_symbol_atom_with_buddy_data_f, (LmnWord)mem);


  /* atoms縺ｯ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺励°蜷ｫ縺ｾ縺ｪ縺�縺ｮ縺ｧ縲�
   * srcvec縺ｮ繝ｪ繝ｳ繧ｯ縺檎峩謗･繝�繝ｼ繧ｿ繧｢繝医Β縺ｫ謗･邯壹＠縺ｦ縺�繧句�ｴ蜷医�ｮ蜃ｦ逅�繧偵☆繧� */
  for (i = 0; i < vec_num(srcvec); i++)
  {
    LinkObj l = (LinkObj)vec_get(srcvec, i);
    if (LMN_ATTR_IS_DATA_WITHOUT_EX(l->pos))
    {
      //printf(" removed data atom: atom= %d, pos=%d \n",LMN_SATOM(l->ap),l->pos);
      lmn_mem_remove_data_atom(mem, l->ap, l->pos);
    }
    else if (LMN_ATTR_IS_EX(l->pos))
    {
      //printf(" removed symbol atom: atom= %d, pos=%d \n",LMN_SATOM(l->ap),l->pos);
      mem_remove_symbol_atom(mem, LMN_SATOM(l->ap));
    }
  }

}

int free_symbol_atom_with_buddy_data_f(LmnWord _k, LmnWord _v, LmnWord _arg)
{
  free_symbol_atom_with_buddy_data((LmnSAtom)_v);
  return 1;
}

void lmn_mem_free_ground(Vector *srcvec)
{
  ProcessTbl atoms=proc_tbl_make_with_size(64);
  unsigned long i, t;

  if (ground_atoms(srcvec, NULL, &atoms, &t, NULL, NULL, NULL, NULL)) {
    proc_tbl_foreach(atoms, free_symbol_atom_with_buddy_data_f, (LmnWord)0);
   // proc_tbl_free(atoms);
  }

  /* atoms縺ｯ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺励°蜷ｫ縺ｾ縺ｪ縺�縺ｮ縺ｧ縲《rcvec縺ｮ繝ｪ繝ｳ繧ｯ縺檎峩謗･繝�繝ｼ繧ｿ
     繧｢繝医Β縺ｫ謗･邯壹＠縺ｦ縺�繧句�ｴ蜷医�ｮ蜃ｦ逅�繧偵☆繧� */
  for (i = 0; i < vec_num(srcvec); i++) {
    LinkObj l = (LinkObj)vec_get(srcvec, i);
    if (LMN_ATTR_IS_DATA(l->pos)) lmn_free_atom(l->ap, l->pos);
  }

  proc_tbl_free(atoms);
}

void lmn_mem_free_hlground(Vector *srcvec,
                           ProcessTbl *attr_sym,
                           Vector *attr_data,
                           Vector *attr_data_at)
{

  ProcessTbl atoms=hlground_data.local_atoms;

  unsigned long i, t;

  proc_tbl_foreach(atoms, free_symbol_atom_with_buddy_data_f, (LmnWord)0);

  /* release these two ProcessTbl*/
  proc_tbl_free(hlground_data.global_hlinks);
  proc_tbl_free(hlground_data.local_atoms);

  /* atoms縺ｯ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺励°蜷ｫ縺ｾ縺ｪ縺�縺ｮ縺ｧ縲《rcvec縺ｮ繝ｪ繝ｳ繧ｯ縺檎峩謗･繝�繝ｼ繧ｿ
     繧｢繝医Β縺ｫ謗･邯壹＠縺ｦ縺�繧句�ｴ蜷医�ｮ蜃ｦ逅�繧偵☆繧� */
  for (i = 0; i < vec_num(srcvec); i++)
  {
    LinkObj l = (LinkObj)vec_get(srcvec, i);
    if (LMN_ATTR_IS_DATA(l->pos))
    	lmn_free_atom(l->ap, l->pos);
  }
}

void lmn_mem_delete_ground(LmnMembrane *mem, Vector *srcvec)
{
  ProcessTbl atoms;
  unsigned long i, t;

  if (!ground_atoms(srcvec, NULL, &atoms, &t, NULL, NULL, NULL, NULL)) {
    fprintf(stderr, "remove ground false\n");
  }

  proc_tbl_foreach(atoms, mem_remove_symbol_atom_with_buddy_data_f, (LmnWord)mem);
  proc_tbl_foreach(atoms, free_symbol_atom_with_buddy_data_f, (LmnWord)0);

  /* atoms縺ｯ繧ｷ繝ｳ繝懊Ν繧｢繝医Β縺励°蜷ｫ縺ｾ縺ｪ縺�縺ｮ縺ｧ縲《rcvec縺ｮ繝ｪ繝ｳ繧ｯ縺檎峩謗･繝�繝ｼ繧ｿ
     繧｢繝医Β縺ｫ謗･邯壹＠縺ｦ縺�繧句�ｴ蜷医�ｮ蜃ｦ逅�繧偵☆繧� */
  for (i = 0; i < vec_num(srcvec); i++) {
    LinkObj l = (LinkObj)vec_get(srcvec, i);
    if (LMN_ATTR_IS_DATA(l->pos)) {
      lmn_mem_remove_data_atom(mem, l->ap, l->pos);
      lmn_free_atom(l->ap, l->pos);
    }
  }

  proc_tbl_free(atoms);
}

/** ===========================
 *  閹懊�ｮ蜷悟梛諤ｧ蛻､螳�
 *  ---------------------------
 *  created  by Takayuki Sasaki
 *  modified by Masato Gocho
 *  ---------------------------
 */
#ifndef TIME_OPT
/* 髱杁IME-OPT縺ｧ縺ｯTraceLog縺ｮ繧ｹ繝医Λ繧ｯ繝√Ε縺ｧ繝医Ξ繝ｼ繧ｹ縺吶ｋ縺薙→縺後〒縺阪↑縺�ｼ亥ｮ溯｣�縺励※縺ｪ縺�ｼ峨�ｮ縺ｧ,
 * 繝舌げ縺ｮ縺ゅｋ譌ｧ蜷悟ｽ｢謌仙愛螳壹さ繝ｼ繝峨〒蜍穂ｽ懊＆縺帙ｋ.
 * TODO:
 *   髱杁IME-OPT繧ｳ繝ｼ繝峨�ｯ莉雁ｾ御ｿ晏ｮ医＠縺ｦ縺�縺丞ｿ�隕√′縺ゅｋ縺具ｼ�
 */
# define LMN_MEMEQ_OLD
#endif

#define CHECKED_MEM_DEPTH (0)
static BOOL mem_equals_rec(LmnMembrane *mem1, TraceLog  log1,
                           LmnMembrane *mem2, SimplyLog log2,
                           int current_depth);


/* 髫主ｱ､繧ｰ繝ｩ繝墓ｧ矩��mem1, mem2縺悟酔蝙九↑繧峨�ｰ逵溘ｒ霑斐☆. */
BOOL lmn_mem_equals(LmnMembrane *mem1, LmnMembrane *mem2)
{
  BOOL t;

#ifdef PROFILE
  if (lmn_env.profile_level >= 3) {
    profile_start_timer(PROFILE_TIME__STATE_COMPARE_MEQ);
  }
#endif

#ifdef LMN_MEMEQ_OLD
  t = mem_equals_rec(mem1, NULL,  mem2, NULL, CHECKED_MEM_DEPTH);
#else
  struct TraceLog log1;
  struct SimplyTraceLog log2;

  tracelog_init(&log1);
  simplylog_init(&log2);

  t = mem_equals_rec(mem1, &log1, mem2, &log2, CHECKED_MEM_DEPTH);

  simplylog_destroy(&log2);
  tracelog_destroy(&log1);
#endif

#ifdef PROFILE
  if (lmn_env.profile_level >= 3) {
    profile_finish_timer(PROFILE_TIME__STATE_COMPARE_MEQ);
  }
#endif

  return t;
}


static BOOL mem_equals_atomlists(LmnMembrane *mem1, LmnMembrane *mem2);
static BOOL mem_equals_isomorphism(LmnMembrane *mem1, TraceLog log1,
                                   LmnMembrane *mem2, SimplyLog log2,
                                   int current_depth);

/* 閹徇em1縺九ｉ蛻ｰ驕泌庄閭ｽ縺ｪ蜈ｨ繝励Ο繧ｻ繧ｹ縺ｨ
 * 閹徇em2縺九ｉ蛻ｰ驕泌庄閭ｽ縺ｪ蜈ｨ繝励Ο繧ｻ繧ｹ縺�
 * 莠偵＞縺ｫ驕惹ｸ崎ｶｳ縺ｪ縺上�槭ャ繝√Φ繧ｰ縺吶ｋ(蜷悟梛縺ｮ髫主ｱ､繧ｰ繝ｩ繝墓ｧ矩��)縺ｪ繧峨�ｰ逵溘ｒ霑斐☆. */
static BOOL mem_equals_rec(LmnMembrane *mem1, TraceLog  log1,
                           LmnMembrane *mem2, SimplyLog log2,
                           int current_depth)
{
#ifndef LMN_MEMEQ_OLD
  /* mem1, mem2縺梧悴險ｪ蝠上�ｮ蝣ｴ蜷� */
  if (tracelog_contains_mem(log1, mem1)) {
    if (!simplylog_contains_mem(log2, mem2)) {
      return FALSE;
    }
    else {
      return lmn_mem_id(mem2) == tracelog_get_memMatched(log1, mem1);
    }
  }
  else if (simplylog_contains_mem(log2, mem2)) {
    return FALSE;
  }

  tracelog_put_mem(log1, mem1, lmn_mem_id(mem2));
  simplylog_put_mem(log2, mem2);
#endif

  /*  繧ｰ繝ｩ繝募酔蠖｢謌仙愛螳壹�ｮ蜷ТTEP:
   *  Step ID | Complexity  | Description
   *  ==========================================================================
   *      1.1 | O(1)        | 譁�蟄怜�励↓蟇ｾ蠢懊＠縺滓紛謨ｰID(LmnMembrane縺ｮ繝｡繝ｳ繝�)蜷悟｣ｫ縺ｮ豈碑ｼ�
   *      1.2 | O(1)        | 繧ｷ繝ｳ繝懊Ν繧｢繝医Β謨ｰ(LmnMembrane縺ｮ繝｡繝ｳ繝�)蜷悟｣ｫ縺ｮ豈碑ｼ�
   *      1.3 | O(1)        | 繝�繝ｼ繧ｿ繧｢繝医Β謨ｰ(LmnMembrane縺ｮ繝｡繝ｳ繝�)蜷悟｣ｫ縺ｮ豈碑ｼ�
   *      1.4 | O(N)        | N譛ｬ縺ｮAtomListEntry縺ｮ遞ｮ鬘槭→蜷�繧｢繝医Β謨ｰ縺ｮ豈碑ｼ�
   *      1.5 | O(N)        | 蟄占�懈焚縺ｮ豈碑ｼ�(N蛟九�ｮ隕∫ｴ�繧呈戟縺｣縺溘Μ繧ｹ繝郁ｵｰ譟ｻ)
   *      1.6 | O(N*M)      | 蟄仙ｭｫ閹懈焚縺ｮ豈碑ｼ�(N:蟄占�懈焚, M:髫主ｱ､謨ｰ: 髫主ｱ､謨ｰ縺�縺大ｭ占�懊き繧ｦ繝ｳ繝�)
   *      2.0 | O(N)        | 騾壼ｸｸ : N譛ｬ縺ｮRuleSet ID豈碑ｼ�(ID縺ｯ謨ｴ謨ｰ縺九▽謨ｴ蛻玲ｸ医∩謨�縺ｫ邱壼ｽ｢)
   *          | O(N*M)      | uniq: N譛ｬ縺ｮRuleSet ID縺翫ｈ縺ｳ螻･豁ｴ縺ｮ豈碑ｼ�
   *          |             | 蜷�螻･豁ｴ縺ｮ豈碑ｼ�縺ｯO(1), 螻･豁ｴ縺ｯ繝上ャ繧ｷ繝･陦ｨ邂｡逅�->M蛟九�ｮ螻･豁ｴ豈碑ｼ�:O(M)
   *      3.1 | O(?)        | 繧｢繝医Β襍ｷ轤ｹ縺ｮ繧ｰ繝ｩ繝墓ｧ矩��縺ｮ蜷悟ｽ｢謌仙愛螳�
   *      3.2 | O(?)        | 蟄占�懆ｵｷ轤ｹ縺ｮ繧ｰ繝ｩ繝墓ｧ矩��縺ｮ蜷悟ｽ｢謌仙愛螳�
   *  --------------------------------------------------------------------------
   *  Step 1.X| O(N + M)    | N:蟄占�懈焚, M;繧｢繝医Β繝ｪ繧ｹ繝域焚. Step 1.6蟒�豁｢縺ｫ縺､縺�,邱壼ｽ｢縺ｮ險育ｮ鈴㍼
   *  Step 2.X| O(N)        | 讓呎ｺ紋ｻ墓ｧ�(uniq譛ｪ菴ｿ逕ｨ)縺ｪ繧峨�ｰ邱壼ｽ｢縺ｮ險育ｮ鈴㍼
   *  Step 3.X| O(?)        | TODO: 隱ｿ譟ｻ
   *
   *   蟄仙ｭｫ閹懊�ｮ豈碑ｼ�(Step 1.6)縺ｯ繧ｳ繧ｹ繝医′鬮倥＞縺溘ａ蟒�豁｢.
   *     螳滄圀縺ｫ縺ｯ繝上ャ繧ｷ繝･髢｢謨ｰmhash縺ｨ縺ｮ邨�蜷医ｏ縺帙〒蜷悟ｽ｢謌仙愛螳壹ｒ螳滓命縺励※縺翫ｊ,
   *     繝上ャ繧ｷ繝･髢｢謨ｰmhash縺ｯ, 髫主ｱ､縺ｮ豺ｱ縺輔↓髢｢縺吶ｋ繝上ャ繧ｷ繝･繧ｳ繝ｳ繝輔Μ繧ｯ繝医ｒ諡帙″繧�縺吶＞繧ゅ�ｮ縺ｧ縺ｯ縺ｪ縺�.
   */
  if (/* 1.1 */ LMN_MEM_NAME_ID(mem1) != LMN_MEM_NAME_ID(mem2)                ||
      /* 1.2 */ lmn_mem_symb_atom_num(mem1) != lmn_mem_symb_atom_num(mem2)    ||
      /* 1.3 */ lmn_mem_data_atom_num(mem1) != lmn_mem_data_atom_num(mem2)    ||
      /* 1.4 */ !mem_equals_atomlists(mem1, mem2)                             ||
      /* 1.5 */ lmn_mem_child_mem_num(mem1) != lmn_mem_child_mem_num(mem2)    ||
      /* 1.6 lmn_mem_count_descendants(mem1) != lmn_mem_count_descendants(mem2)) ||*/
      /* 2.0 */ !lmn_rulesets_equals(lmn_mem_get_rulesets(mem1),
                                     lmn_mem_get_rulesets(mem2))              ||
      /* 3.X */ !mem_equals_isomorphism(mem1, log1, mem2, log2, current_depth))
  {
    return FALSE;
  }
  else {
    return TRUE;
  }
}


/* 閹徇em1, mem2逶ｴ荳九�ｮ繧｢繝医Β繝ｪ繧ｹ繝医ｒ豈碑ｼ�(繧｢繝医Β縺ｮ遞ｮ鬘樊ｯ弱↓繧｢繝医Β謨ｰ繧呈ｯ碑ｼ�)縺励◆邨先棡縺ｮ逵溷⊃蛟､繧定ｿ斐☆ */
static BOOL mem_equals_atomlists(LmnMembrane *mem1, LmnMembrane *mem2)
{
  AtomListEntry *ent1;
  LmnFunctor f;

  EACH_ATOMLIST_WITH_FUNC(mem1, ent1, f, ({
    AtomListEntry *ent2 = lmn_mem_get_atomlist(mem2, f);
    if (atomlist_get_entries_num(ent1) != atomlist_get_entries_num(ent2)) {
      return FALSE;
    }
  }));

  return TRUE;
}


/** ---------------------------
 *  Step 3.X
 */


#define ISOMOR_PHASE_ATOM   (0U)
#define ISOMOR_PHASE_CHILD  (1U)
#define ISOMOR_PHASE_END    (2U)

typedef struct MemIsomorIter MemIsomorIter;
struct MemIsomorIter {
  BYTE          phase;
  AtomListIter  pos;
  LmnMembrane   *mem;
  LmnSAtom      atom;
  LmnMembrane   *child;
};


static inline BOOL mem_equals_molecules(LmnMembrane *mem1, LmnMembrane *mem2, int current_depth);
static inline BOOL mem_equals_children(LmnMembrane *mem1, LmnMembrane *mem2, int current_depth);

static inline void memIsomorIter_init(MemIsomorIter *ma_iter, LmnMembrane *mem);
static inline void memIsomorIter_destroy(MemIsomorIter *iter);
static inline LmnSAtom     memIsomorIter_atom_traversed(MemIsomorIter *iter);
static inline LmnMembrane *memIsomorIter_child_traversed(MemIsomorIter *iter);
static BOOL mem_isomor_mols(LmnMembrane *mem1, TraceLog  log1,
                            LmnMembrane *mem2, SimplyLog log2,
                            MemIsomorIter *iter);

/* 譛ｪ險ｪ蝠上�ｮ閹徇em1縺翫ｈ縺ｳ閹徇em2繧貞ｯｾ雎｡縺ｫ, 繧｢繝医Β襍ｷ轤ｹ縺翫ｈ縺ｳ蟄占�懆ｵｷ轤ｹ縺ｧ繧ｰ繝ｩ繝墓ｧ矩��繧偵ヨ繝ｬ繝ｼ繧ｹ縺吶ｋ.
 * 閹徇em1縺ｨmem2莉･荳九�ｮ髫主ｱ､繧ｰ繝ｩ繝輔′蜷悟梛縺ｨ蛻､螳壹＆繧後◆縺ｪ繧後�ｰ逵溘ｒ霑斐＠, 逡ｰ蠖｢縺ｪ縺ｰ繧牙⊃繧定ｿ斐☆. */
static BOOL mem_equals_isomorphism(LmnMembrane *mem1, TraceLog  log1,
                                   LmnMembrane *mem2, SimplyLog log2,
                                   int current_depth)
{
  BOOL ret;
#ifdef LMN_MEMEQ_OLD
  /* !!CAUTION!!:
   *   譌ｧ蜷悟ｽ｢謌仙愛螳壹い繝ｫ繧ｴ繝ｪ繧ｺ繝�縺ｯ, 迚ｹ螳壹�ｮ繧ｱ繝ｼ繧ｹ縺ｧ豁｣縺励＞邨先棡繧定ｿ斐☆縺薙→縺後〒縺阪↑縺�.
   */
  ret = /* Step 3.1 */ mem_equals_molecules(mem1, mem2, current_depth) &&
        /* Step 3.2 */ mem_equals_children(mem1, mem2, current_depth);
#else
  MemIsomorIter iter;

  memIsomorIter_init(&iter, mem1);
  ret = /* Step 3.X */ mem_isomor_mols(mem1, log1, mem2, log2, &iter);
  memIsomorIter_destroy(&iter);
#endif

  return ret;
}

static inline void memIsomorIter_init(MemIsomorIter *ma_iter, LmnMembrane *mem)
{
  ma_iter->pos   = atomlist_iter_initializer(mem->atomset);
  ma_iter->phase = ISOMOR_PHASE_ATOM;
  ma_iter->mem   = mem;
  ma_iter->atom  = NULL;
  ma_iter->child = NULL;
}

static inline void memIsomorIter_destroy(MemIsomorIter *iter) {}

static inline BOOL memIsomorIter_is_root_atom(LmnSAtom atom)
{
  LmnFunctor f;
  f = LMN_SATOM_GET_FUNCTOR(atom);
  if (f == LMN_RESUME_FUNCTOR     ||
      f == LMN_OUT_PROXY_FUNCTOR  ||
      (f == LMN_IN_PROXY_FUNCTOR &&
       !LMN_ATTR_IS_DATA(LMN_SATOM_GET_ATTR(atom, 1)))) {
    /* 繝�繝ｼ繧ｿ繧｢繝医Β縺ｨ謗･邯壹☆繧喫nside proxy縺�縺代�ｯ繝ｫ繝ｼ繝医い繝医Β縺ｨ縺吶ｋ */
    return FALSE;
  }

  return TRUE;
}

/* 繧､繝�繝ｬ繝ｼ繧ｿiter縺ｫ險倬鹸縺励◆諠�蝣ｱ繧貞渕縺ｫ,
 * 蜑榊屓險ｪ蝠上＠縺溘い繝医Βiter->atom縺九ｉ谺｡縺ｫ險ｪ蝠上☆繧九い繝医Β繧呈ｱゅａ縺ｦ霑斐＠, 縺ｪ縺�蝣ｴ蜷医�ｯNULL霑斐☆.
 *
 * 蜃ｺ蜉�: iter->pos, iter->atom (iter->mem縺ｯRead Only) */
static inline LmnSAtom memIsomorIter_atom_traversed(MemIsomorIter *iter)
{
  BOOL changed_lists;

  if (iter->phase != ISOMOR_PHASE_ATOM) {
    return NULL;
  }

  changed_lists = FALSE;
  for ( ;
       atomlist_iter_condition(iter->mem, iter->pos);
       atomlist_iter_next(iter->pos), changed_lists = TRUE, iter->atom = NULL)
       /* OUTER LOOP */
  {
    AtomListEntry *ent;
    LmnFunctor f;
    f   = atomlist_iter_get_functor(iter->pos);
    ent = atomlist_iter_get_entry(iter->mem, iter->pos);

    if (!ent || atomlist_is_empty(ent) || f == LMN_OUT_PROXY_FUNCTOR) {
      /* 繧｢繝医Β繝ｪ繧ｹ繝医′遨ｺ縺ｮ蝣ｴ蜷�, 谺｡縺ｮ蛟呵｣懊Μ繧ｹ繝医ｒ蜿門ｾ励☆繧�.
       * outside proxy縺ｯ蛟呵｣懊→縺励↑縺� */
      continue; /* OUTER LOOP */
    }
    else {
      BOOL cur_is_tail;

      if (changed_lists || !iter->atom) {
        /* 襍ｰ譟ｻ蜈医�ｮ繧｢繝医Β繝ｪ繧ｹ繝医′螟画峩縺輔ｌ縺溷�ｴ蜷� or 繧､繝�繝ｬ繝ｼ繧ｿ繧｢繝医Β縺梧悴險ｭ螳壹�ｮ蝣ｴ蜷�:
         *   繝ｪ繧ｹ繝医�ｮ蜈磯�ｭ繧｢繝医Β繧団ur縺ｨ縺吶ｋ */
        iter->atom = atomlist_head(ent);
      }
      else if (iter->atom == lmn_atomlist_end(ent)) {
        /* 荳�蠢�, 諠ｳ螳壼､� */
        continue;
      }
      else {
        /* 譌｢縺ｫ繧､繝�繝ｬ繝ｼ繧ｿ繧｢繝医Β縺瑚ｨｭ螳壹＆繧後※縺�繧句�ｴ蜷医�ｯ, 繝ｪ繧ｹ繝医°繧画ｬ｡縺ｮ繧｢繝医Β繧堤｢ｺ菫昴☆繧� */
        iter->atom = LMN_SATOM_GET_NEXT_RAW(iter->atom);
        if (iter->atom == lmn_atomlist_end(ent)) {
          /* 邨先棡, cur縺梧忰蟆ｾ縺ｫ蛻ｰ驕斐＠縺溘↑繧峨�ｰ谺｡縺ｮ繧｢繝医Β繝ｪ繧ｹ繝医ｒ豎ゅａ繧�. */
          continue; /* OUTER LOOP */
        }
      }

      /* 繝ｪ繧ｹ繝医ｒ霎ｿ繧�, 譬ｹ縺ｮ蛟呵｣懊→縺吶ｋ繧｢繝医Β繧呈ｱゅａ繧� */
      cur_is_tail = FALSE;
      while (!memIsomorIter_is_root_atom(iter->atom)) { /* INNER LOOP */
        /* Resume繧｢繝医Β繧定ｪｭ縺ｿ鬟帙�ｰ縺� */
        iter->atom = LMN_SATOM_GET_NEXT_RAW(iter->atom);

        if (iter->atom == lmn_atomlist_end(ent)) {
          /* tail縺ｫ蛻ｰ驕斐＠縺ｦ縺励∪縺｣縺溷�ｴ蜷医�ｯ繝輔Λ繧ｰ繧堤ｫ九※縺ｦ縺九ｉ繝ｫ繝ｼ繝励ｒ謚懊￠繧� */
          cur_is_tail = TRUE;
          break; /* INNER LOOP */
        }
      } /* INNER LOOP END */

      if (!cur_is_tail) {
        /* atomlistentry縺ｮtail縺ｸ縺ｮ蛻ｰ驕比ｻ･螟悶〒繝ｫ繝ｼ繝励ｒ謚懊￠縺溷�ｴ蜷�:
         *   繧｢繝医Β縺ｸ縺ｮ蜿ら�ｧ蜿門ｾ励↓謌仙粥縺励◆縺ｮ縺ｧ繝ｫ繝ｼ繝励ｒ謚懊￠繧� */
        break; /* OUTER LOOP */
      }
    }
  } /* OUTER LOOP END */

  if (iter->atom) {
    return iter->atom;
  } else {
    iter->phase = ISOMOR_PHASE_CHILD;
    return NULL;
  }
}


static inline LmnMembrane *memIsomorIter_child_traversed(MemIsomorIter *iter)
{
  if (iter->phase != ISOMOR_PHASE_CHILD) {
    return NULL;
  }
  else {
    if (!iter->child) {
      iter->child = lmn_mem_child_head(iter->mem);
    } else {
      iter->child = lmn_mem_next(iter->child);
    }

    if (!iter->child) {
      iter->phase = ISOMOR_PHASE_END;
      return NULL;
    } else {
      return iter->child;
    }
  }
}

#define MEM_ISOMOR_FIRST_TRACE          (-1)
#define MEM_ISOMOR_MATCH_WITHOUT_MEM    (1U)
#define MEM_ISOMOR_MATCH_WITHIN_MEM     (2U)


static inline BOOL mem_isomor_mol_atoms(LmnMembrane *mem1, TraceLog  log1,
                                        LmnMembrane *mem2, SimplyLog log2,
                                        MemIsomorIter *iter, LmnSAtom root1);
static inline BOOL mem_isomor_mol_mems(LmnMembrane *mem1, TraceLog  log1,
                                       LmnMembrane *mem2, SimplyLog log2,
                                       MemIsomorIter *iter, LmnMembrane *root1);


/* mem1縺翫ｈ縺ｳmem2逶ｴ荳九°繧臥ｭ我ｾ｡縺ｪ蛻�蟄先ｧ矩��繧貞�榊ｸｰDFS縺ｧ謗｢邏｢縺吶ｋ.
 * 蜈ｨ縺ｦ縺ｮ蛻�蟄舌′驕惹ｸ崎ｶｳ縺ｪ縺�1蟇ｾ1縺ｫ縺励◆蝣ｴ蜷�, 逵溘ｒ霑斐☆. */
static BOOL mem_isomor_mols(LmnMembrane *mem1, TraceLog  log1,
                            LmnMembrane *mem2, SimplyLog log2,
                            MemIsomorIter *iter)
{
  LmnSAtom root1;

  /* 繧｢繝医Β襍ｷ轤ｹ縺ｮ謗｢邏｢縺ｮ縺溘ａ縺ｮ譬ｹ繧貞叙蠕� */
  do {
    root1 = memIsomorIter_atom_traversed(iter);
    /* 譛ｪ繝医Ξ繝ｼ繧ｹ縺ｮ繧｢繝医Β縺檎樟繧後ｋ縺�, 譛ｫ蟆ｾ(NULL)縺ｫ蛻ｰ驕斐☆繧九∪縺ｧ譬ｹ縺ｮ蛟呵｣懈爾邏｢繧堤ｹｰ繧願ｿ斐☆. */
  } while (root1 && tracelog_contains_atom(log1, root1));

  if (root1) {
    /* 繧｢繝医Β縺ｮ譬ｹ縺後≠繧句�ｴ蜷�: 繧｢繝医Β襍ｷ轤ｹ縺ｮ繧ｰ繝ｩ繝募酔蠖｢謌仙愛螳� */
    return mem_isomor_mol_atoms(mem1, log1,
                                mem2, log2, iter, root1);
  }
  else {
    LmnMembrane *child;
    do {
      child = memIsomorIter_child_traversed(iter);
    } while (child && tracelog_contains_mem(log1, child));

    if (child) {
      /* 蟄占�懊�ｮ譬ｹ縺後≠繧句�ｴ蜷�: 蟄占�懆ｵｷ轤ｹ縺ｮ繧ｰ繝ｩ繝募酔蠖｢謌仙愛螳� */
      return mem_isomor_mol_mems(mem1, log1,
                                 mem2, log2,
                                 iter, child);
    }
    else {
      /* 繧｢繝医Β縺ｮ譬ｹ繧ょｭ占�懊�ｮ譬ｹ縺後↑縺�蝣ｴ蜷�:
       *   蜈ｨ縺ｦ縺ｮ譬ｹ繧貞�呵｣懊→縺励◆繧ｰ繝ｩ繝募酔蠖｢謌仙愛螳壹′邨ゆｺ�.
       *   豈碑ｼ�貂医�励Ο繧ｻ繧ｹ謨ｰ縺ｨ謇�謖√�励Ο繧ｻ繧ｹ謨ｰ繧呈ｯ碑ｼ�縺�, 蟇ｾ蠢懈ｼ上ｌ縺後↑縺�縺区､懈渊縺吶ｋ. */

      return tracelog_eq_traversed_proc_num(log1,
                                            mem1,
                                            lmn_mem_get_atomlist(mem1, LMN_IN_PROXY_FUNCTOR),
                                            NULL);
    }
  }
}


static inline int mem_isomor_trace(LmnAtom cur1, LmnMembrane *mem1, TraceLog log1,
                                   LmnFunctor f,  int from_i,
                                   LmnAtom cur2, LmnMembrane *mem2, SimplyLog log2);

static inline BOOL mem_isomor_mol_atoms(LmnMembrane *mem1, TraceLog  log1,
                                        LmnMembrane *mem2, SimplyLog log2,
                                        MemIsomorIter *iter, LmnSAtom root1)
{
  MemIsomorIter current; /* for keeping current state (snap shot) */
  AtomListEntry *ent2;
  LmnSAtom      root2;
  LmnFunctor    f;

  f    = LMN_SATOM_GET_FUNCTOR(root1);
  ent2 = lmn_mem_get_atomlist(mem2, f);
  if (!ent2) return FALSE;
  current = (*iter); /* shallow copy */

  EACH_ATOM(root2, ent2, ({
    if (simplylog_contains_atom(log2, root2)) continue;
    tracelog_set_btpoint(log1);
    simplylog_set_btpoint(log2);

    /* compare a molecule of root1 with a molecule of root2 */
    switch (mem_isomor_trace((LmnAtom)root1, mem1, log1,
                             f, MEM_ISOMOR_FIRST_TRACE,
                             (LmnAtom)root2, mem2, log2))
    {
      case MEM_ISOMOR_MATCH_WITHOUT_MEM:
      {
        tracelog_continue_trace(log1);
        simplylog_continue_trace(log2);
        return mem_isomor_mols(mem1, log1, mem2, log2, iter);
      }
      case MEM_ISOMOR_MATCH_WITHIN_MEM:
        /* keep this backtrack point */
        if (mem_isomor_mols(mem1, log1, mem2, log2, iter)) {
          tracelog_continue_trace(log1);
          simplylog_continue_trace(log2);
          return TRUE;
        } /*
        else FALL THROUGH */
      case FALSE:
        /*   FALL THROUGH */
      default:
        break;
    }

    (*iter) = current;
    tracelog_backtrack(log1);
    simplylog_backtrack(log2);
  }));

  /* matching縺吶ｋ蛻�蟄舌′縺ｪ縺九▲縺�: FALSE */

  return FALSE;
}


static inline BOOL mem_isomor_mol_mems(LmnMembrane *mem1, TraceLog  log1,
                                       LmnMembrane *mem2, SimplyLog log2,
                                       MemIsomorIter *iter, LmnMembrane *root1)
{
  LmnMembrane *root2;
  for (root2 = lmn_mem_child_head(mem2); root2; root2 = lmn_mem_next(root2)) {
    if (simplylog_contains_mem(log2, root2)) continue;
    else {
      tracelog_set_btpoint(log1);
      simplylog_set_btpoint(log2);
      if (!mem_equals_rec(root1, log1, root2, log2, CHECKED_MEM_DEPTH) ||
          !mem_isomor_mols(mem1, log1, mem2, log2, iter)) {
        tracelog_backtrack(log1);
        simplylog_backtrack(log2);
      }
      else {
        tracelog_continue_trace(log1);
        simplylog_continue_trace(log2);
        return TRUE;
      }
    }
  }

  return FALSE;
}



static inline BOOL mem_isomor_trace_proxies(LmnAtom cur1, LmnMembrane *mem1, TraceLog log1,
                                            LmnFunctor f,
                                            LmnAtom cur2, LmnMembrane *mem2, SimplyLog log2);
static inline int mem_isomor_trace_symbols(LmnAtom cur1, LmnMembrane *mem1, TraceLog log1,
                                           LmnFunctor f, int from_i,
                                           LmnAtom cur2, LmnMembrane *mem2, SimplyLog log2);


/* from link逡ｪ蜿ｷ縺�縺代�ｯ蜿励￠蜿悶ｋ.
 * 遲我ｾ｡縺ｪ繧｢繝医Βcur1, cur2縺九ｉ1step蜈医�ｮ讒矩��繧呈ｯ碑ｼ�縺励※蜀榊ｸｰ縺吶ｋ. */
static inline int mem_isomor_trace(LmnAtom cur1, LmnMembrane *mem1, TraceLog log1,
                                   LmnFunctor f,  int from_i,
                                   LmnAtom cur2, LmnMembrane *mem2, SimplyLog log2)
{
  if (LMN_IS_PROXY_FUNCTOR(f)) {
    /* proxy atom縺ｮ蝣ｴ蜷� */
    if (!mem_isomor_trace_proxies(cur1, mem1, log1, f,
                                  cur2, mem2, log2)) {
      return FALSE;
    } else {
      return MEM_ISOMOR_MATCH_WITHIN_MEM;
    }
  }
  else {
    /* 髱柝roxy(symbol)繧｢繝医Β縺ｮ蝣ｴ蜷� */
    return mem_isomor_trace_symbols(cur1, mem1, log1, f, from_i,
                                    cur2, mem2, log2);
  }
}

static inline BOOL mem_isomor_trace_proxies(LmnAtom cur1, LmnMembrane *mem1, TraceLog log1,
                                            LmnFunctor f,
                                            LmnAtom cur2, LmnMembrane *mem2, SimplyLog log2)
{
  LmnAtom pair1, pair2, atom1, atom2;
  LmnLinkAttr attr1, attr2;
  LmnMembrane *nxtM1, *nxtM2;

  /* proxy atom縺ｮ蝣ｴ蜷�
   * -----------------+
   * ...-0--1-[in]-0--|--0-[out]-1--..
   * -----------------+
   */
  pair1 = LMN_SATOM_GET_LINK(cur1, 0);
  pair2 = LMN_SATOM_GET_LINK(cur2, 0);
  nxtM1 = LMN_PROXY_GET_MEM(pair1);
  nxtM2 = LMN_PROXY_GET_MEM(pair2);

  atom1 = LMN_SATOM_GET_LINK(pair1, 1);
  atom2 = LMN_SATOM_GET_LINK(pair2, 1);
  attr1 = LMN_SATOM_GET_ATTR(pair1, 1);
  attr2 = LMN_SATOM_GET_ATTR(pair2, 1);

  /* 1. inside proxy atom縺ｫ蟇ｾ縺吶ｋ險ｪ蝠城未菫ゅｒ繝√ぉ繝�繧ｯ */
  {
    LmnAtom in1, in2;
    if (f == LMN_OUT_PROXY_FUNCTOR) {
      in1   = pair1;
      in2   = pair2;
    }
    else if (f == LMN_IN_PROXY_FUNCTOR) {
      in1   = cur1;
      in2   = cur2;
    }
    else {
      lmn_fatal("sorry, unrecognized proxy functor found: unrecognized proxy");
      return FALSE;
    }

    if (tracelog_contains_atom(log1, (LmnSAtom)in1)) {
      if (!simplylog_contains_atom(log2, (LmnSAtom)in2) ||
          LMN_SATOM_ID(in2) != tracelog_get_atomMatched(log1, (LmnSAtom)in1)) {
        return FALSE;
      } else {
        return TRUE;
      }
    }
    else if (simplylog_contains_atom(log2, (LmnSAtom)in2)) {
      return FALSE;
    }
    else {
      /* in1縺ｯ莠偵＞縺ｫ譛ｪ險ｪ蝠上〒縺ゅｋ縺溘ａ, 繝医Ξ繝ｼ繧ｹ縺吶ｋ蠢�隕√′縺ゅｋ */
      tracelog_put_atom(log1, (LmnSAtom)in1, LMN_SATOM_ID(in2), LMN_PROXY_GET_MEM(in1));
      simplylog_put_atom(log2, (LmnSAtom)in2);
    }
  }

  /* 2. 閹懊�ｮ險ｪ蝠城未菫ゅｒ繝√ぉ繝�繧ｯ */
  if (!tracelog_contains_mem(log1, nxtM1)) {
    if (simplylog_contains_mem(log2, nxtM2)) {
      /* nxtM1縺梧悴險ｪ蝠上〒nxtM2縺瑚ｨｪ蝠乗ｸ医↑繧峨�ｰ逡ｰ蠖｢ */
      return FALSE;
    }
  }
  else if (!simplylog_contains_mem(log2, nxtM2) ||
           lmn_mem_id(nxtM2) != tracelog_get_memMatched(log1, nxtM1)) {
    /* nxtM1縺瑚ｨｪ蝠乗ｸ医〒, { nxtM2縺梧悴險ｪ蝠� || 蟇ｾ蠢懊☆繧玖�懊�ｮ髢｢菫ゅ′蜷医ｏ縺ｪ縺� } 縺ｪ繧峨�ｰ逡ｰ蠖｢ */
    return FALSE;
  } /*
  else {
    閹從xtM1, nxtM2縺ｯ莠偵＞縺ｫ險ｪ蝠乗ｸ� or 譛ｪ險ｪ蝠�
  }
  */

  /* 3. 豈碑ｼ� */
  if ((LMN_ATTR_IS_DATA(attr1) || LMN_ATTR_IS_DATA(attr2))) {
    /* proxy繧｢繝医Β縺後ョ繝ｼ繧ｿ繧｢繝医Β縺ｫ謗･邯壹＠縺ｦ縺�繧句�ｴ蜷� */
    if (!lmn_data_atom_eq(atom1, attr1, atom2, attr2)) {
      return FALSE;
    }
    else {
      BOOL ret = TRUE;
      if (f == LMN_OUT_PROXY_FUNCTOR && !tracelog_contains_mem(log1, nxtM1)) {
        /* 蛻晏�ｺ縺ｮ蟄占�懊↓蜈･繧�(out proxy縺九ｉ繝医Ξ繝ｼ繧ｹ縺吶ｋ)蝣ｴ蜷医�ｯ, 蟄占�懊�ｮ蜷悟ｽ｢謌仙愛螳壹ｒ陦後≧. */
        ret = mem_equals_rec(nxtM1, log1, nxtM2, log2, CHECKED_MEM_DEPTH);
      }
      return ret;
    }
  }
  else { /* proxy繧｢繝医Β縺後す繝ｳ繝懊Ν繧｢繝医Β縺ｫ謗･邯壹＠縺ｦ縺�繧句�ｴ蜷� */
    LmnFunctor f_new = LMN_SATOM_GET_FUNCTOR(atom1);
    if (f_new != LMN_SATOM_GET_FUNCTOR(atom2) || attr1 != attr2) {
      return FALSE;
    }
    else {
      BOOL ret = mem_isomor_trace(atom1, nxtM1, log1,
                                  f_new, LMN_ATTR_GET_VALUE(attr1),
                                  atom2, nxtM2, log2);
      if (ret &&
          f == LMN_OUT_PROXY_FUNCTOR && !tracelog_contains_mem(log1, nxtM1)) {
        /* 蛻晏�ｺ縺ｮ蟄占�懊↓蜈･繧�(out proxy縺九ｉ繝医Ξ繝ｼ繧ｹ縺吶ｋ)蝣ｴ蜷医�ｯ, 蟄占�懊�ｮ蜷悟ｽ｢謌仙愛螳壹ｒ陦後≧. */
        ret = mem_equals_rec(nxtM1, log1,
                             nxtM2, log2, CHECKED_MEM_DEPTH);
      }

      return ret;
    }
  }
}


static inline int mem_isomor_trace_symbols(LmnAtom cur1, LmnMembrane *mem1, TraceLog log1,
                                           LmnFunctor f, int from_i,
                                           LmnAtom cur2, LmnMembrane *mem2, SimplyLog log2)
{
  int to_i, global_ret;

  tracelog_put_atom(log1, (LmnSAtom)cur1, LMN_SATOM_ID(cur2), mem1);
  simplylog_put_atom(log2, (LmnSAtom)cur2);

  global_ret = MEM_ISOMOR_MATCH_WITHOUT_MEM;

  /* 繝ｪ繝ｳ繧ｯ蜈医ｒ霎ｿ繧� */
  for (to_i = 0; to_i < LMN_FUNCTOR_ARITY(f); to_i++) {
    LmnLinkAttr attr1, attr2;
    LmnAtom atom1, atom2;

    /* 霎ｿ縺｣縺ｦ縺阪◆繝ｪ繝ｳ繧ｯ縺ｯ讀懈渊貂医∩: skip */
    if (to_i == from_i) continue;

    attr1 = LMN_SATOM_GET_ATTR(cur1, to_i);
    attr2 = LMN_SATOM_GET_ATTR(cur2, to_i);
    atom1 = LMN_SATOM_GET_LINK(cur1, to_i);
    atom2 = LMN_SATOM_GET_LINK(cur2, to_i);

    if (LMN_ATTR_IS_DATA(attr1) ||
        LMN_ATTR_IS_DATA(attr2)) { /* data atom縺ｮ蝣ｴ蜷� */
      if (!lmn_data_atom_eq(atom1, attr1, atom2, attr2)) {
        return FALSE;
      }/*
      else continue
      */
    }
    else { /* symbol atom縺ｮ蝣ｴ蜷� */
      LmnFunctor f_new = LMN_SATOM_GET_FUNCTOR(atom1);

      if (f_new != LMN_SATOM_GET_FUNCTOR(atom2) ||
          attr1 != attr2)
      {
        /* 謗･邯壼�医い繝医Β縺ｮ遞ｮ鬘�(functor)繧�謗･邯壼�医Μ繝ｳ繧ｯ縺ｮ逡ｪ蜿ｷ縺檎焚縺ｪ繧句�ｴ蜷医�ｯ逡ｰ蠖｢ */
        return FALSE;
      }

      if (tracelog_contains_atom(log1, (LmnSAtom)atom1)) {
        /* 謗･邯壼�医�ｮ繧｢繝医Β縺梧里縺ｫ險ｪ蝠乗ｸ医∩縺ｮ蝣ｴ蜷� */
        if (!simplylog_contains_atom(log2, (LmnSAtom)atom2) ||
            LMN_SATOM_ID(atom2) != tracelog_get_atomMatched(log1, (LmnSAtom)atom1))
        {
          /* 莠偵＞縺ｫ險ｪ蝠乗ｸ医∩髢｢菫ゅ〒縺ｪ縺�蝣ｴ蜷医ｄ險ｪ蝠乗ｸ医∩繧｢繝医Β縺ｸ縺ｮ蟇ｾ蠢憺未菫ゅ′荳�閾ｴ縺励※縺�縺ｪ縺�蝣ｴ蜷医�ｯ逡ｰ蠖｢ */
          return FALSE;
        }/*
        else continue
        */
      }
      else if (simplylog_contains_atom(log2, (LmnSAtom)atom2)) {
        /* 謗･邯壼�医�ｮ繧｢繝医Β縺ｸ蛻昴ａ縺ｦ險ｪ蝠上☆繧句�ｴ蜷�: 蟇ｾ蠢懊☆繧九い繝医Β縺瑚ｨｪ蝠乗ｸ医∩縺ｮ縺溘ａ逡ｰ蠖｢ */
        return FALSE;
      }
      else {
        /* 莠偵＞縺ｫ謗･邯壼�医�ｮ繧｢繝医Β縺ｸ蛻昴ａ縺ｦ險ｪ蝠上☆繧句�ｴ蜷� */
        switch (mem_isomor_trace(atom1, mem1, log1,
                                 f_new, LMN_ATTR_GET_VALUE(attr1),
                                 atom2, mem2, log2)) {
        case FALSE:
          return FALSE;
        case MEM_ISOMOR_MATCH_WITHIN_MEM:
          global_ret = MEM_ISOMOR_MATCH_WITHIN_MEM;
          break;
        case MEM_ISOMOR_MATCH_WITHOUT_MEM:
          /* FALLTHROUTH */
        default:
          break;
        }
      }
    }
  }

  /* cur1, cur2繧定ｵｷ轤ｹ縺ｫ蛻ｰ驕泌庄閭ｽ縺ｪLMNtal繝励Ο繧ｻ繧ｹ縺ｮ謗･邯夐未菫ゅ′蜈ｨ縺ｦ荳�閾ｴ縺励◆
   * 逵溘ｒ霑斐☆. */

  return global_ret;
}


/** -------------------
 *  Step 3.2: 蟄占�懆ｵｷ轤ｹ縺ｮ繧ｰ繝ｩ繝輔ヨ繝ｬ繝ｼ繧ｹ (old)
 */

static void mem_mk_sorted_children(Vector *vec);
static inline BOOL mem_equals_children_inner(Vector *v_mems_children1,
                                             Vector *v_mems_children2,
                                             int current_depth);

/* 縺薙�ｮ谿ｵ髫弱〒譛ｬ閹懷��縺ｮ縲後い繝医Β繧定ｵｷ轤ｹ縺ｨ縺吶ｋ襍ｰ譟ｻ縲阪�ｯ縺吶∋縺ｦ螳御ｺ�縺�, 荳｡閹懃峩荳九�ｮ繧ｰ繝ｩ繝墓ｧ矩��縺ｯ蜷悟梛縺ｧ縺ゅｋ.
 * 縺薙％縺九ｉ縺ｯ, 荳｡閹懷��縺ｫ蟄伜惠縺吶ｋ縺吶∋縺ｦ縺ｮ蟄占�懊↓縺､縺�縺ｦ, 縺昴�ｮ讒矩��縺御ｸ�閾ｴ縺吶ｋ縺九←縺�縺九↓縺､縺�縺ｦ隱ｿ縺ｹ縺ｦ縺�縺�.
 * 莉･髯阪�［em1蜀�縺ｮ蟄占�懊ｒ1縺､蝗ｺ螳壹＠縲［em2逶ｴ荳九�ｮ蟄占�懊°繧牙ｯｾ蠢懊☆繧九ｂ縺ｮ繧堤音螳壹☆繧倶ｽ懈･ｭ縺ｫ遘ｻ縺｣縺ｦ縺�縺上′,
 * 邨先棡縺悟⊃縺ｨ縺ｪ繧九こ繝ｼ繧ｹ縺ｫ縺翫￠繧句�ｦ逅�騾溷ｺｦ繧貞髄荳翫＆縺帙ｋ縺溘ａ, 蟄仙ｭｫ閹懈焚縺悟ｰ代↑縺�蟄占�懊°繧牙━蜈育噪縺ｫ蝗ｺ螳壹☆繧� */
static inline BOOL mem_equals_children(LmnMembrane *mem1, LmnMembrane *mem2, int current_depth)
{
  int child_n;

  child_n = lmn_mem_count_children(mem1);
  if (child_n != lmn_mem_count_children(mem2)) {
    return FALSE;
  }
  else if (child_n == 0) {
    return TRUE;
  }
  else {
    Vector *v_mems_children1, *v_mems_children2; /* 譛ｬ閹懃峩荳九�ｮ蟄占�懊ｒ邂｡逅�縺吶ｋVector (縺薙�ｮVector縺檎ｩｺ縺ｫ縺ｪ繧九∪縺ｧ蟄占�懊ｒ襍ｷ轤ｹ縺ｨ縺吶ｋ襍ｰ譟ｻ縺檎ｶ壹￥) */
    BOOL matched;

    v_mems_children1 = vec_make(child_n);
    v_mems_children2 = vec_make(child_n);
    memset(v_mems_children1->tbl, 0, sizeof(vec_data_t) * vec_cap(v_mems_children1));
    memset(v_mems_children2->tbl, 0, sizeof(vec_data_t) * vec_cap(v_mems_children2));

    matched = TRUE;

    /* mem1, mem2逶ｴ荳九�ｮ蟄占�懊ｒ蜿門ｾ励＠縲√◎縺ｮ蛟区焚縺檎ｭ峨＠縺�縺薙→繧堤｢ｺ隱� */
    {
      LmnMembrane *m;
      for (m = mem1->child_head; m; m = m->next) vec_push(v_mems_children1, (LmnWord)m);
      for (m = mem2->child_head; m; m = m->next) vec_push(v_mems_children2, (LmnWord)m);

      if (vec_num(v_mems_children1) != vec_num(v_mems_children2)) {
        /* 譛ｬ閹懃峩荳九�ｮ蟄占�懈焚縺御ｺ偵＞縺ｫ荳�閾ｴ縺励↑縺�蝣ｴ蜷医�ｯ蛛ｽ */
        matched = FALSE;
      }
      else {
        /* 蟄仙ｭｫ閹懈焚縺ｮ螟壹＞鬆�縺ｫv_mems_children1, v_mems_children2繧偵た繝ｼ繝� */
        mem_mk_sorted_children(v_mems_children1);
        mem_mk_sorted_children(v_mems_children2);
      }
    }

    if (matched) {
      matched = mem_equals_children_inner(v_mems_children1,
                                          v_mems_children2,
                                          current_depth);
    }


    vec_free(v_mems_children1);
    vec_free(v_mems_children2);

    return matched;
  }
}

static inline BOOL mem_equals_children_inner(Vector *v_mems_children1,
                                             Vector *v_mems_children2,
                                             int current_depth)
{
  int i, j;
  BOOL matched;

  /* 蟄占�懊ｒ襍ｷ轤ｹ縺ｨ縺吶ｋ襍ｰ譟ｻ */
  matched = TRUE;
  while (matched && !vec_is_empty(v_mems_children1)) {
    LmnMembrane *cm1 = (LmnMembrane *)vec_pop(v_mems_children1);

    /* fprintf(stderr, "\t-- start to test a descendant membrane --\n\t\t# of descendants of mem(%u): %u\n"
     *               , (unsigned int)cm1
     *               , lmn_mem_count_descendants(cm1)); */

    for (i = vec_num(v_mems_children2); i > 0; i--) {
      LmnMembrane *cm2 = (LmnMembrane *)vec_get(v_mems_children2, i-1);

      matched = mem_equals_rec(cm1, NULL, cm2, NULL, current_depth + 1);
      if (!matched) {
        continue; /* INNER1 LOOP */
      }
      else {
        /* cm1縺ｨ蜷悟梛縺ｮ閹�(=cm2)縺計_mems_children2蜀�縺ｫ隕九▽縺九▲縺溷�ｴ蜷医↓縺薙％縺ｫ蜈･繧九��
         * v_mems_children2縺九ｉcm2繧貞叙繧企勁縺上�� */
        for (j = 0; j < vec_num(v_mems_children2); j++) {
          if (cm2 == (LmnMembrane *)vec_get(v_mems_children2, j)) {
            vec_pop_n(v_mems_children2, j);
            break; /* INNER2 LOOP */
          }
        }
        break; /* INNER1 LOOP */
      }
    }
  }

  return matched;
}

/* 縺ゅｋ閹懊�ｮ逶ｴ荳九�ｮ縺吶∋縺ｦ縺ｮ蟄占�懊∈縺ｮ繝昴う繝ｳ繧ｿ繧剃ｿ晄戟縺吶ｋ繝吶け繧ｿ繝ｼvec繧�
 * 蟄仙ｭｫ閹懈焚縺ｮ螟壹＞鬆�縺ｫ繧ｽ繝ｼ繝医☆繧九�ゅた繝ｼ繝医＆繧後◆vec蜀�縺ｮ隕∫ｴ�縺ｯ蠕後�ｮ繧ｹ繝�繝�繝励〒
 * POP縺輔ｌ繧九◆繧√�∝ｭ仙ｭｫ閹懈焚縺ｮ蟆代↑縺�蟄占�懊°繧蛾��縺ｫ繝槭ャ繝√Φ繧ｰ縺ｮ蟇ｾ雎｡縺ｨ縺ｪ繧九％縺ｨ縺ｫ縺ｪ繧九�� */
static void mem_mk_sorted_children(Vector *vec)
{
  unsigned int num_descendants_max;
  unsigned int i, n;
  Vector *v_mems_tmp;

  LMN_ASSERT(vec_num(vec));

  num_descendants_max = 0;
  for (i = 0; i < vec_num(vec); i++) {
    n = lmn_mem_count_descendants((LmnMembrane *)vec_get(vec, i));
    if (n > num_descendants_max) {
      num_descendants_max = n;
    }
  }
  v_mems_tmp = vec_make(vec_num(vec));
  for (n = 0; n <= num_descendants_max; n++) {
    for (i = 0; i < vec_num(vec); i++) {
      if (n == lmn_mem_count_descendants((LmnMembrane *)vec_get(vec, i))) {
        vec_push(v_mems_tmp, vec_get(vec, i));
      }
    }
  }
  vec_clear(vec);
  while (!vec_is_empty(v_mems_tmp)) {
    vec_push(vec, vec_pop(v_mems_tmp));
  }
  vec_free(v_mems_tmp);
}




/** -------------------
 * Step 3.1: 繧｢繝医Β襍ｷ轤ｹ縺ｮ繧ｰ繝ｩ繝輔ヨ繝ｬ繝ｼ繧ｹ (old)
 */

typedef struct AtomVecData {
  LmnFunctor fid;
  Vector *atom_ptrs;
} atomvec_data;


static Vector *mem_mk_matching_vec(LmnMembrane *mem);
static void free_atomvec_data(Vector *vec);
static inline BOOL mem_equals_molecules_inner(Vector *v_log1, Vector *v_atoms_not_checked1,
                                              Vector *v_log2, Vector *v_atoms_not_checked2,
                                              int current_depth);

/* 縺薙�ｮ谿ｵ髫弱〒荳｡閹懊�ｯ莠偵＞縺ｫ遲峨＠縺�謨ｰ縺ｮ蟄仙ｭｫ閹懊ｒ謖√■縲∽ｸ｡閹懷��縺ｮ繧｢繝医Β縺ｮ繝輔ぃ繝ｳ繧ｯ繧ｿ縺ｮ遞ｮ鬘�
 * 縺翫ｈ縺ｳ縺昴�ｮ蛟区焚縺悟ｮ悟�ｨ縺ｫ荳�閾ｴ縺吶ｋ縺薙→縺檎｢ｺ隱阪＆繧後※縺�繧九��
 * (i.e. 邨先棡縺後�悟酔蝙九〒縺ｪ縺�(蛛ｽ)縲阪↓縺ｪ繧九↑繧峨�ｰ縲∵悽閹懊↓縺翫￠繧九Μ繝ｳ繧ｯ縺ｮ謗･邯夐未菫� or 蟄仙ｭｫ閹懊′逡ｰ縺ｪ縺｣縺ｦ縺�繧九％縺ｨ繧呈э蜻ｳ縺吶ｋ)
 * 莉･髯阪�∝ｰ第焚豢ｾ縺ｮ繧｢繝医Β縺九ｉ鬆�縺ｫ譬ｹ縺ｫ螳壹ａ縺ｦ縺�縺阪�√い繝医Β繧定ｵｷ轤ｹ縺ｨ縺吶ｋ襍ｰ譟ｻ縺ｮ螳溯｡後↓遘ｻ縺｣縺ｦ縺�縺上�� */
static inline BOOL mem_equals_molecules(LmnMembrane *mem1, LmnMembrane *mem2, int current_depth)
{
  unsigned int atom_n = lmn_mem_atom_num(mem1);
  LMN_ASSERT(atom_n == lmn_mem_atom_num(mem2));

  if (atom_n != lmn_mem_atom_num(mem2)) {
    return FALSE;
  }
  else if (atom_n == 0) {
    return TRUE;
  }
  else {
    Vector *v_log1, *v_log2; /* 襍ｰ譟ｻ荳ｭ縺ｫ騾夐℃縺励◆繧｢繝医Β縺ｮ繝ｭ繧ｰ繧堤ｮ｡逅�縺吶ｋVector */
    Vector *v_atoms_not_checked1, *v_atoms_not_checked2; /* 蜷悟梛諤ｧ縺ｮ蛻､螳壹�ｮ蛻､螳壹′貂医ｓ縺ｧ縺�縺ｪ縺�繧｢繝医Β縺ｮ髮�蜷医ｒ邂｡逅�縺吶ｋVector (蜷�繧｢繝医Β縺ｸ縺ｮ繝昴う繝ｳ繧ｿ繧剃ｿ晏ｭ�) */
    int i, j;
    BOOL ret;

    /* 莉･髯阪�∵悴襍ｰ譟ｻ�ｼ剰ｵｰ譟ｻ貂医い繝医Β繧堤ｮ｡逅�縺吶ｋvector縺ｮ蛻晄悄蛹� */

    v_atoms_not_checked1 = vec_make(atom_n);
    v_atoms_not_checked2 = vec_make(atom_n);
    memset(v_atoms_not_checked1->tbl, 0, sizeof(vec_data_t) * vec_cap(v_atoms_not_checked1));
    memset(v_atoms_not_checked2->tbl, 0, sizeof(vec_data_t) * vec_cap(v_atoms_not_checked2));

    {
      Vector *atomvec_mem1, *atomvec_mem2;

      /* atomvec_memX (X = 1,2)縺ｯ縲∬�徇emX 逶ｴ荳九�ｮ繧｢繝医Β縺ｮ諠�蝣ｱ繧剃ｿ晄戟縺吶ｋVector縲�
       * 閹懷��縺ｮ繧｢繝医Β繧偵ヵ繧｡繝ｳ繧ｯ繧ｿ豈弱↓謨ｴ逅�縺励�∝ｰ第焚豢ｾ縺ｮ繧｢繝医Β縺九ｉ繝槭ャ繝√Φ繧ｰ繧帝幕蟋九〒縺阪ｋ繧医≧縺ｫ縺吶ｋ逶ｮ逧�縺ｧ菴ｿ逕ｨ縺吶ｋ縲� */
      atomvec_mem1 = mem_mk_matching_vec(mem1);
      atomvec_mem2 = mem_mk_matching_vec(mem2);

      LMN_ASSERT(vec_num(atomvec_mem1) == vec_num(atomvec_mem2));

      /* 繝吶け繧ｿ繝ｼatomvec_mem{1,2}縺ｫ縺ｯ螟壽焚豢ｾ縺ｮ繧｢繝医Β縺九ｉ鬆�縺ｫ謾ｾ繧翫％縺ｾ繧後※縺�繧九◆繧√��
       * 繝吶け繧ｿ繝ｼv_atoms_not_checked{1,2}縺ｫ縺ｯ螟壽焚豢ｾ縺ｮ繧｢繝医Β縺ｮ繝昴う繝ｳ繧ｿ縺九ｉ鬆�縺ｫ
       * 謾ｾ繧翫％縺ｾ繧後※縺�縺上％縺ｨ縺ｫ縺ｪ繧九�ゅｆ縺医↓縲」_atoms_not_checked{1,2}繧単OP縺励※縺�縺�
       * 縺薙→縺ｧ蟆第焚豢ｾ縺ｮ繧｢繝医Β縺九ｉ鬆�縺ｫ蜿悶ｊ蜃ｺ縺励※縺�縺上％縺ｨ縺後〒縺阪ｋ繧医≧縺ｫ縺ｪ繧九�� */
      for (i = 0; i < vec_num(atomvec_mem1); i++) {
        for (j = 0; j < vec_num(((atomvec_data *)vec_get(atomvec_mem1, i))->atom_ptrs); j++) {
          vec_push(v_atoms_not_checked1,
                   vec_get(((atomvec_data *)vec_get(atomvec_mem1, i))->atom_ptrs, j));
          vec_push(v_atoms_not_checked2,
                   vec_get(((atomvec_data *)vec_get(atomvec_mem2, i))->atom_ptrs, j));
        }
      }

      free_atomvec_data(atomvec_mem1);
      free_atomvec_data(atomvec_mem2);
    }

    LMN_ASSERT(vec_num(v_atoms_not_checked1) == vec_num(v_atoms_not_checked2));

    v_log1 = vec_make(atom_n);
    v_log2 = vec_make(atom_n);

    ret = mem_equals_molecules_inner(v_log1, v_atoms_not_checked1,
                                     v_log2, v_atoms_not_checked2,
                                     current_depth);
    vec_free(v_log1);
    vec_free(v_log2);
    vec_free(v_atoms_not_checked1);
    vec_free(v_atoms_not_checked2);

    return ret;
  }
}

/* 讒矩��菴�'atomvec_data'縺ｫ髢｢菫ゅ☆繧九Γ繝｢繝ｪ繝ｼ縺ｮ鬆伜沺繧定ｧ｣謾ｾ縺吶ｋ */
static void free_atomvec_data(Vector *vec)
{
  unsigned int i;

  for (i = 0; i < vec_num(vec); ++i) {
    atomvec_data *ad = (atomvec_data *)vec_get(vec, i);
    if (ad) {
      vec_free(ad->atom_ptrs);
      LMN_FREE(ad);
    }
  }
  vec_free(vec);
}


static BOOL mem_trace_links(LmnSAtom a1,
                            LmnSAtom a2,
                            Vector *v_log1,
                            Vector *v_log2,
                            int current_depth,
                            BOOL need_to_check_this_membrane_processes);

static inline BOOL mem_equals_molecules_inner(Vector *v_log1, Vector *v_atoms_not_checked1,
                                              Vector *v_log2, Vector *v_atoms_not_checked2,
                                              int current_depth)
{
  BOOL matched = TRUE;
  while (matched && !vec_is_empty(v_atoms_not_checked1)) {
    LmnSAtom a1;
    int i, j, k;

    /* 閹�1蜀�縺九ｉ1縺､繧｢繝医Β繧貞叙繧雁�ｺ縺励％繧後ｒ譬ｹ縺ｨ縺吶ｋ縲�
     * 閹�1蜀�縺ｮ繧｢繝医Β縺ｮ蜀�縲�(繝輔ぃ繝ｳ繧ｯ繧ｿ縺�)蟆第焚豢ｾ縺ｮ繧ゅ�ｮ縺九ｉ鬆�縺ｫ譬ｹ縺ｫ螳壹ａ繧峨ｌ縺ｦ縺�縺上％縺ｨ縺ｫ豕ｨ諢上○繧医�� */
    a1 = (LmnSAtom)vec_pop(v_atoms_not_checked1);

    /*fprintf(stdout, "fid(a1):%u\n", (unsigned int)LMN_SATOM_GET_FUNCTOR(a1));*/

    for (i = vec_num(v_atoms_not_checked2); i > 0; i--) {
      LmnSAtom a2 = (LmnSAtom)vec_get(v_atoms_not_checked2, i-1); /* 閹�2蜀�縺九ｉ譬ｹa1縺ｫ蟇ｾ蠢懊☆繧九い繝医Β縺ｮ蛟呵｣懊ｒ蜿門ｾ� (豕ｨ: 縺薙％縺ｮ螳溯｣�縺ｫvec_pop縺ｯ菴ｿ逕ｨ荳榊庄!!) */

      vec_clear(v_log1);
      vec_clear(v_log2);
      memset(v_log1->tbl, 0, sizeof(vec_data_t) * vec_cap(v_log1));
      memset(v_log2->tbl, 0, sizeof(vec_data_t) * vec_cap(v_log2));

      /* a2縺梧悽蠖薙↓a1縺ｫ蟇ｾ蠢懊☆繧九い繝医Β縺ｧ縺ゅｋ縺句凄縺九ｒ螳滄圀縺ｫ繧ｰ繝ｩ繝墓ｧ矩��繧偵ヨ繝ｬ繝ｼ繧ｹ縺励※遒ｺ隱阪☆繧九��
       * a2縺ｨa1縺ｨ縺�1:1縺ｫ蟇ｾ蠢懊☆繧句�ｴ蜷医↓髯舌▲縺ｦ matched 縺ｫ逵溘′霑斐ｊ縲�
       * v_log{1,2}蜀�縺ｫ縺ｯa{1,2}繧定ｵｷ轤ｹ縺ｨ縺吶ｋ蛻�蟄仙��縺ｮ蜈ｨ繧｢繝医Β縺ｮ繧｢繝峨Ξ繧ｹ縺瑚ｵｰ譟ｻ繝ｭ繧ｰ縺ｨ縺励※險倬鹸縺輔ｌ繧九�� */
      matched = mem_trace_links(a1, a2, v_log1, v_log2, current_depth, FALSE);
      if (!matched) {
        continue;
      }
      else {
        /*fprintf(stdout, "fid(a2):%u\n", (unsigned int)LMN_SATOM_GET_FUNCTOR(a2));*/

        /* 荳｡閹懷��縺ｫ蟄伜惠縺吶ｋ縺ゅｋ蛻�蟄仙酔螢ｫ縺ｮ繝槭ャ繝√Φ繧ｰ縺ｫ謌仙粥縺励◆蝣ｴ蜷医↓縺薙％縺ｫ蜈･繧九��
         * 閹�2蜀�縺ｮ譛ｪ繝槭ャ繝√Φ繧ｰ縺ｮ繧｢繝医Β繧堤ｮ｡逅�縺励※縺�縺溘�吶け繧ｿ繝ｼ(v_atoms_not_checked2)
         * 縺九ｉ譬ｹa1縺ｫ蟇ｾ蠢懊☆繧九い繝医Βa2繧帝勁蜴ｻ縺吶ｋ縲� */
        LMN_ASSERT(vec_num(v_log1) == vec_num(v_log2));
        for (j = 0; j < vec_num(v_atoms_not_checked2); j++) {
          if (LMN_SATOM(vec_get(v_atoms_not_checked2, j)) == a2) {
            vec_pop_n(v_atoms_not_checked2, j);
            break;
          }
        }
        LMN_ASSERT(vec_num(v_atoms_not_checked1) == vec_num(v_atoms_not_checked2));

        /* 繝ｭ繧ｰ荳翫↓蟄伜惠縺吶ｋ縺吶∋縺ｦ縺ｮ繧｢繝医Β繧偵�∵悴繝√ぉ繝�繧ｯ繧｢繝医Β縺ｮ繝ｪ繧ｹ繝医°繧臼OP縺吶ｋ */
        for (j = 0; j < vec_num(v_log1); j++) {
          for (k = 0; k < vec_num(v_atoms_not_checked1); k++) {
            if (LMN_SATOM(vec_get(v_log1, j)) == LMN_SATOM(vec_get(v_atoms_not_checked1, k))) {
              vec_pop_n(v_atoms_not_checked1, k);
              break;
            }
          }
        }

        for (j = 0; j < vec_num(v_log2); j++) {
          for (k = 0; k < vec_num(v_atoms_not_checked2); k++) {
            if (LMN_SATOM(vec_get(v_log2, j)) == LMN_SATOM(vec_get(v_atoms_not_checked2, k))) {
              vec_pop_n(v_atoms_not_checked2, k);
              break;
            }
          }
        }

        LMN_ASSERT(vec_num(v_atoms_not_checked1) == vec_num(v_atoms_not_checked2));
        break;
      }
    }
  }

  return matched;
}


/* 繧｢繝医Βa1縲∥2繧定ｵｷ轤ｹ縺ｨ縺吶ｋ蛻�蟄�(= 縺ゅｋ繧｢繝医Β縺九ｉ繝ｪ繝ｳ繧ｯ縺ｫ繧医▲縺ｦ逶ｴ謗･霎ｿ繧九％縺ｨ縺ｮ縺ｧ縺阪ｋ繝励Ο繧ｻ繧ｹ縺ｮ髮�蜷�)
 * 縺ｮ讒矩��縺御ｺ偵＞縺ｫ荳�閾ｴ縺吶ｋ縺句凄縺九ｒ蛻､螳壹☆繧九�ょ酔蝙区�ｧ蛻､螳壹ｒ陦後≧荳翫〒縺ｮ荳ｭ蠢�逧�蠖ｹ蜑ｲ繧呈球縺｣縺ｦ縺�繧九��
 * 荳｡蛻�蟄先ｧ矩��縺悟ｮ悟�ｨ縺ｫ荳�閾ｴ縺吶ｋ蝣ｴ蜷医�ｯ縲∬ｵｰ譟ｻ荳ｭ縺ｫ騾夐℃縺励◆蜈ｨ繧｢繝医Β(i.e. 蛻�蟄仙��縺ｮ蜈ｨ繧｢繝医Β)縺ｮ繧｢繝峨Ξ繧ｹ縺�
 * 繝ｭ繧ｰ逕ｨ縺ｮ繝吶け繧ｿ繝ｼ(v_log1縲」_log2)縺ｫ菫晏ｭ倥＆繧後ｋ縲�
 *
 * 縺ｪ縺翫�∫ｬｬ5蠑墓焚縺ｮ 'current_depth' 縺ｯ縲∝酔蝙区�ｧ蛻､螳壼ｯｾ雎｡縺ｨ縺ｪ縺｣縺ｦ縺�繧玖�懊�ｮ隕ｪ閹懊↓縺ｾ縺ｧ
 * 襍ｰ譟ｻ縺悟所縺ｶ縺ｮ繧帝亟縺舌◆繧√�ｮ繧ゅ�ｮ縲ょｭ占�懷��縺ｮ繝励Ο繧ｻ繧ｹ縺ｫ襍ｰ譟ｻ蟇ｾ雎｡縺檎ｧｻ繧矩圀縺ｫ current_depth 縺ｯ1蠅怜刈縺励��
 * 騾�縺ｫ隕ｪ閹懷��縺ｮ繝励Ο繧ｻ繧ｹ縺ｫ遘ｻ繧矩圀縺ｯ1貂帛ｰ代☆繧九�ょ酔蝙区�ｧ蛻､螳壼ｯｾ雎｡縺ｮ閹懊�ｮ豺ｱ縺輔�ｯ0縺ｫ縺ｪ縺｣縺ｦ縺�繧九◆繧√��
 * 0譛ｪ貅�縺ｮ豺ｱ縺輔↓蟄伜惠縺吶ｋ繝励Ο繧ｻ繧ｹ縺ｯ襍ｰ譟ｻ縺励↑縺�繧医≧縺ｫ縺吶ｋ縲� */
static BOOL mem_trace_links(LmnSAtom a1,
                                LmnSAtom a2,
                                Vector *v_log1,
                                Vector *v_log2,
                                int current_depth,
                                BOOL need_to_check_this_membrane_processes)
{
  unsigned int i;
  int next_depth;

  /* 譛ｬ繝｡繧ｽ繝�繝峨ｒ蜀榊ｸｰ逧�縺ｫ蜻ｼ縺ｳ蜃ｺ縺励※縺�縺城℃遞九〒�ｼ径1(,a2)縺ｸ縺ｮ繝ｪ繝ｳ繧ｯ繧定ｾｿ繧九％縺ｨ縺瑚ｦｪ閹懊°繧牙ｭ占�懊∈縺ｮ驕ｷ遘ｻ繧呈э蜻ｳ縺吶ｋ蝣ｴ蜷茨ｼ�
   * a1, a2縺ｮ謇�螻櫁�懊ｒ蟇ｾ雎｡縺ｨ縺励◆蜷悟梛諤ｧ蛻､螳壹ｒ陦後≧蠢�隕√′縺ゅｋ */
  if (need_to_check_this_membrane_processes) {
    LmnMembrane *mem1, *mem2;

    /* 縺薙％縺ｮ蜃ｦ逅�繧偵☆繧矩圀�ｼ径1縺翫ｈ縺ｳa2縺ｯ蜈ｱ縺ｫ繝励Ο繧ｭ繧ｷ繧｢繝医Β縺ｮ縺ｯ縺� */
    LMN_ASSERT(LMN_IS_PROXY_FUNCTOR(LMN_SATOM_GET_FUNCTOR(a1)) &&
               LMN_IS_PROXY_FUNCTOR(LMN_SATOM_GET_FUNCTOR(a2)));

    mem1 = LMN_PROXY_GET_MEM(a1);
    mem2 = LMN_PROXY_GET_MEM(a2);

    if (!mem_equals_rec(mem1, NULL, mem2, NULL, CHECKED_MEM_DEPTH)) {
      return FALSE;
    }
  }

  vec_push(v_log1, (LmnWord)a1);
  vec_push(v_log2, (LmnWord)a2);

  /* a1縲∥2縺ｮ繝輔ぃ繝ｳ繧ｯ繧ｿ縺御ｸ�閾ｴ縺吶ｋ縺薙→繧堤｢ｺ隱�(荳堺ｸ�閾ｴ縺ｮ蝣ｴ蜷医�ｯ辟｡譚｡莉ｶ縺ｫ蛛ｽ繧定ｿ斐☆) */
  if (LMN_SATOM_GET_FUNCTOR(a1) != LMN_SATOM_GET_FUNCTOR(a2)) {
    return FALSE;
  }

  /* 090808蜷悟梛諤ｧ蛻､螳壹ヰ繧ｰ2蟇ｾ蠢�
   * 隕ｪ閹懷��縺ｮ繝励Ο繧ｻ繧ｹ縺ｮ縺�縺｡縲�$out縺ｫ縺､縺ｪ縺後▲縺ｦ縺�繧九ｂ縺ｮ縺ｫ縺､縺�縺ｦ縺ｯ隱ｿ縺ｹ繧句ｿ�隕√′縺ゅｋ */
  if (LMN_SATOM_GET_FUNCTOR(a1) == LMN_IN_PROXY_FUNCTOR                &&
      current_depth == CHECKED_MEM_DEPTH                               &&
      LMN_SATOM_GET_ATTR(LMN_SATOM(LMN_SATOM_GET_LINK(a1, 0U)), 1U) !=
          LMN_SATOM_GET_ATTR(LMN_SATOM(LMN_SATOM_GET_LINK(a2, 0U)), 1U)) {
    return FALSE;
  }

  /* a1(= a2) = $in 縺九▽a1縺ｮ謇�螻櫁�懊′蜷悟梛諤ｧ蛻､螳壼ｯｾ雎｡閹懊〒縺ゅｋ蝣ｴ蜷茨ｼ径1縺ｮ隨ｬ0繝ｪ繝ｳ繧ｯ縺ｮ謗･邯壼�医〒縺ゅｋ$out縺ｯ蜷悟梛諤ｧ蛻､螳壼ｯｾ雎｡閹懊�ｮ隕ｪ閹懷��縺ｮ
   * 繝励Ο繧ｻ繧ｹ縺ｨ縺�縺�縺薙→縺ｫ縺ｪ繧奇ｼ後ヨ繝ｬ繝ｼ繧ｹ縺ｮ蟇ｾ雎｡縺ｫ蜷ｫ繧√※縺ｯ縺ｪ繧峨↑縺�ｼ弱％縺ｮ蝓ｺ貅悶↓蝓ｺ縺･縺搾ｼ梧ｬ｡縺ｮ螟画焚i縺ｮ蛻晄悄蛟､�ｼ�0 or 1�ｼ峨′豎ｺ螳壹＆繧後ｋ�ｼ� */
  if (LMN_SATOM_GET_FUNCTOR(a1) == LMN_IN_PROXY_FUNCTOR &&
      current_depth == CHECKED_MEM_DEPTH) {
    i = 1;
  } else {
    i = 0;
  }

  for (; i < LMN_FUNCTOR_GET_LINK_NUM(LMN_SATOM_GET_FUNCTOR(a1)); i++) {
    LmnLinkAttr attr1, attr2;

    attr1 = LMN_SATOM_GET_ATTR(a1, i);
    attr2 = LMN_SATOM_GET_ATTR(a2, i);

    if (LMN_ATTR_IS_DATA(attr1) && LMN_ATTR_IS_DATA(attr2)) {
      /* 繧｢繝医Βa1縲∥2縺ｮ隨ｬi繝ｪ繝ｳ繧ｯ縺ｮ謗･邯壼�医′蜈ｱ縺ｫ繝�繝ｼ繧ｿ繧｢繝医Β縺ｮ繧ｱ繝ｼ繧ｹ:
       *   謗･邯壼�医ョ繝ｼ繧ｿ繧｢繝医Β縺ｮ蛟､縺檎ｭ峨＠縺�縺区､懈渊.
       *   繝�繝ｼ繧ｿ繧｢繝医Β縺ｯa1, a2縺ｮ縺ｿ縺梧磁邯壼�医→縺ｪ繧九◆繧�, a1, a2縺ｮ谺｡縺ｮ繝ｪ繝ｳ繧ｯ讀懈渊縺ｫ遘ｻ陦� */
      if (LMN_SATOM_GET_LINK(a1, i) != LMN_SATOM_GET_LINK(a2, i)) {
        return FALSE;
      }
    }
    else if (!LMN_ATTR_IS_DATA(attr1) && !LMN_ATTR_IS_DATA(attr2)) {
      LmnSAtom l1, l2;

      /* 繧｢繝医Βa1縲∥2縺ｮ隨ｬi繝ｪ繝ｳ繧ｯ縺ｮ謗･邯壼�医′蜈ｱ縺ｫ繧ｷ繝ｳ繝懊Ν(or 繝励Ο繧ｭ繧ｷ)繧｢繝医Β縺ｮ繧ｱ繝ｼ繧ｹ:
       *  1. 荳｡繧｢繝医Β縺ｮ隨ｬi繝ｪ繝ｳ繧ｯ縺ｨ謗･邯壹☆繧九Μ繝ｳ繧ｯ逡ｪ蜿ｷ繧偵メ繧ｧ繝�繧ｯ
       *  2. 謗･邯壼�医す繝ｳ繝懊Ν(or 繝励Ο繧ｭ繧ｷ)繧｢繝医Β繧貞叙蠕励＠, 繝ｭ繧ｰ荳翫↓蟄伜惠縺吶ｋ縺九←縺�縺九ｒ繝√ぉ繝�繧ｯ
       *     繝ｭ繧ｰ荳翫↓縺ｾ縺�蟄伜惠縺励↑縺�譁ｰ隕上い繝医Β縺ｮ蝣ｴ蜷医�ｯ, 譛ｬ繝｡繧ｽ繝�繝峨ｒ蜀榊ｸｰ蜻ｼ縺ｳ蜃ｺ縺励＠縺ｦ
       *     a1縺翫ｈ縺ｳa2繧定ｵｷ轤ｹ縺ｨ縺吶ｋ蛻�蟄仙�ｨ菴薙�ｮ繝槭ャ繝√Φ繧ｰ繧定｡後≧) */

      if (attr1 != attr2) {
        /* {c(X,Y), c(X,Y)} vs. {c(X,Y), c(Y,X)}
         * 縺ｮ萓九�ｮ繧医≧縺ｫ縲�2繧｢繝医Β髢薙�ｮ繝ｪ繝ｳ繧ｯ縺ｮ謗･邯夐��蠎上′逡ｰ縺ｪ縺｣縺ｦ縺�繧句�ｴ蜷医�ｯFALSE繧定ｿ斐☆ */
        return FALSE;
      }

      l1 = LMN_SATOM(LMN_SATOM_GET_LINK(a1, i));
      l2 = LMN_SATOM(LMN_SATOM_GET_LINK(a2, i));

      if ((vec_contains(v_log1, (LmnWord)l1) != vec_contains(v_log2, (LmnWord)l2))) {
         /* 迚�譁ｹ縺ｮ閹懊↓縺翫＞縺ｦ縺ｯ縲√％繧後∪縺ｧ縺ｮ繝医Ξ繝ｼ繧ｹ縺ｧ騾夐℃貂医∩縺ｮ繧｢繝医Β縺ｫ驍�縺｣縺ｦ縺阪◆ (i.e. 蛻�蟄仙��縺ｫ迺ｰ迥ｶ縺ｮ讒矩��(= 髢芽ｷｯ)縺悟ｭ伜惠縺励◆)
          * 繧ゅ�ｮ縺ｮ縲√ｂ縺�迚�譁ｹ縺ｮ閹懊〒縺ｯ蜷梧ｧ倥�ｮ髢芽ｷｯ縺檎｢ｺ隱阪〒縺阪★縲∵ｧ矩��縺ｮ荳堺ｸ�閾ｴ縺瑚ｪ阪ａ繧峨ｌ縺溘◆繧√↓蛛ｽ繧定ｿ斐☆ */
        return FALSE;
      } else if (vec_contains(v_log1, (LmnWord)l1) && vec_contains(v_log2, (LmnWord)l2)) {
         /* 閹�1縲�2蜀�縺ｮ蟇ｾ蠢懊☆繧句�蟄舌′蜈ｱ縺ｫ髢芽ｷｯ繧貞ｽ｢謌舌＠縺溷�ｴ蜷医�ｯ縲∫ｬｬ(i+1)繝ｪ繝ｳ繧ｯ縺ｮ謗･邯壼�医�ｮ繝√ぉ繝�繧ｯ縺ｫ遘ｻ繧� */
        continue;
      }

      /* a1-->l1 (, a2-->l2) 縺ｪ繧九Μ繝ｳ繧ｯ縺ｮ繝医Ξ繝ｼ繧ｹ縺瑚ｦｪ閹�-->蟄占�� or 蟄占��-->隕ｪ閹懊↑繧�
       * 繝医Ξ繝ｼ繧ｹ蟇ｾ雎｡繝励Ο繧ｻ繧ｹ縺ｮ謇�螻櫁�懊�ｮ蛻�繧頑崛縺医ｒ諢丞袖縺吶ｋ髫帙↓�ｼ檎樟蝨ｨ繝医Ξ繝ｼ繧ｹ縺励※縺�繧九�励Ο繧ｻ繧ｹ縺ｮ豺ｱ縺輔ｒ陦ｨ縺吝､画焚縺ｮ蛟､繧呈峩譁ｰ縺吶ｋ�ｼ�
       * (蟄占�懊∈縺ｮ驕ｷ遘ｻ縺ｮ髫帙↓蛟､縺�1蠅怜刈縺暦ｼ碁��縺ｫ隕ｪ閹懷��縺ｫ謌ｻ繧矩圀縺ｯ蛟､縺�1貂帛ｰ代☆繧�)
       *
       * (豕ｨ) "a1=$out 縺九▽ l1=$in 縺ｪ繧峨�ｰ蟄占�懊∈縺ｮ驕ｷ遘ｻ" 縺ｨ縺励※縺ｯ縺�縺代↑縺�ｼ�
       *      蠢�縺啾1, l1縺ｮ謇�螻櫁�懊′逡ｰ縺ｪ縺｣縺ｦ縺�繧九％縺ｨ繧ょｿ�隕∵擅莉ｶ縺ｫ蜷ｫ繧√ｋ繧医≧縺ｫ縺吶ｋ�ｼ�
       *      "{{'+'(L)}}, a(L)."縺ｮ繧医≧縺ｪ繧ｱ繝ｼ繧ｹ縺ｧ縺ｯ�ｼ碁俣縺ｫ閹懊�ｮ蠅�逡後ｒ蜷ｫ縺ｾ縺ｪ縺�縺ｫ繧る未繧上ｉ縺�$in縺ｨ$out縺碁團謗･縺吶ｋ�ｼ�
       */
      next_depth = current_depth;
      if (LMN_SATOM_GET_FUNCTOR(l1) == LMN_IN_PROXY_FUNCTOR   &&
          LMN_SATOM_GET_FUNCTOR(a1) == LMN_OUT_PROXY_FUNCTOR  &&
          LMN_PROXY_GET_MEM(l1)    != LMN_PROXY_GET_MEM(a1))
      {
        next_depth++;
      }
      else if (LMN_SATOM_GET_FUNCTOR(l1) == LMN_OUT_PROXY_FUNCTOR &&
               LMN_SATOM_GET_FUNCTOR(a1) == LMN_IN_PROXY_FUNCTOR  &&
               LMN_PROXY_GET_MEM(l1) != LMN_PROXY_GET_MEM(a1))
      {
        next_depth--;
      }

      /* "i = ((LMN_SATOM_GET_FUNCTOR(a1) == LMN_IN_PROXY_FUNCTOR && current_depth == CHECKED_MEM_DEPTH) ? 1U : 0U)"
       * 縺ｪ繧区悽for繝ｫ繝ｼ繝励↓縺翫￠繧喫縺ｮ蛻晄悄蛹門�ｦ逅�縺ｫ繧医ｊ�ｼ檎峩蠕後�ｮ謗｢邏｢蟇ｾ雎｡繧｢繝医Β(l1, l2)縺ｮ謇�螻櫁�懊′蜷悟梛諤ｧ蛻､螳壼ｯｾ雎｡螟悶�ｮ閹懊↓縺ｪ繧九％縺ｨ縺ｯ縺ｪ縺�縺ｯ縺� */
      LMN_ASSERT(next_depth >= CHECKED_MEM_DEPTH);

      /*
       * a1-->l1 (, a2-->l2) 縺ｪ繧九Μ繝ｳ繧ｯ縺悟ｭ伜惠縺暦ｼ後°縺､l1縺ｮ謇�螻櫁�懊′a1縺ｮ謇�螻櫁�懊�ｮ蟄占�懊〒縺ゅｋ蝣ｴ蜷茨ｼ�
       * 驕ｷ遘ｻ蜈医�ｮ髫主ｱ､(i.e. l1縺翫ｈ縺ｳl2縺昴ｌ縺槭ｌ縺ｮ謇�螻櫁��)逶ｴ荳九↓蜷ｫ縺ｾ繧後ｋ繝励Ο繧ｻ繧ｹ縺ｮ髮�蜷医′莠偵＞縺ｫ荳�閾ｴ縺励※縺�繧九％縺ｨ繧堤｢ｺ隱阪☆繧句ｿ�隕√′逕溘§繧具ｼ�
       *   萓�)
       *   mem1 = {p(L1, R1), {-L1, +R2}, p(L2, R2), {+L2, +R3}, p(L3, R3), {-L3, +R4}, p(L4, R4), {+L4, +R5}, p(L5, R5), {+L5, -R1}.}
       *   mem2 = {p(L1, R1), {-L1, +R2}, p(L2, R2), {+L2, +R3}, p(L3, R3), {+L3, +R4}, p(L4, R4), {-L4, +R5}, p(L5, R5), {+L5, -R1}.}
       *   縺ｮ繧医≧縺ｪmem1縺ｨmem2縺ｮ豈碑ｼ�繧呈ｭ｣縺励￥陦後≧荳翫〒蠢�隕√↑謗ｪ鄂ｮ�ｼ�
       *
       * (豕ｨ) "縺薙％繧誕1縺ｮ謇�螻櫁�懊→l1縺ｮ謇�螻櫁�懊′逡ｰ縺ｪ繧句�ｴ蜷�" 縺ｪ繧句愛螳壹ｒ陦後≧縺ｨ蜃ｦ逅�縺檎┌髯舌Ν繝ｼ繝励☆繧句庄閭ｽ諤ｧ縺後≠繧九�ｮ縺ｧ豕ｨ諢上☆繧九％縺ｨ�ｼ�
       *   辟｡髯舌Ν繝ｼ繝励☆繧倶ｾ�)
       *   mem1 = mem2 = {r(lambda(cp(cp(L3,L4,L0),cp(L5,L6,L1),L2),lambda(L7,apply(L3,apply(L5,apply(L4,apply(L6,L7))))))). {top. '+'(L0). '+'(L1). '+'(L2). }.}
       */
      if (!mem_trace_links(l1, l2, v_log1, v_log2, next_depth, (next_depth > current_depth))) {
        return FALSE;
      }
    }
    else {
       /* 繧｢繝医Βa1縲∥2縺ｮ隨ｬi繝ｪ繝ｳ繧ｯ縺ｮ謗･邯壼�医い繝医Β縺ｮ遞ｮ鬘�(symbol or data)縺御ｸ�閾ｴ縺励↑縺�繧ｱ繝ｼ繧ｹ */
       return FALSE;
    }
  }

  return TRUE;
}


/* 譛ｬ閹懃峩荳九�ｮ縺吶∋縺ｦ縺ｮ繧｢繝医Β(蟄仙ｭｫ閹懷��縺ｮ繧｢繝医Β縺ｯ蜷ｫ縺ｾ縺ｪ縺�)縺ｫ縺､縺�縺ｦ縲√∪縺壹ヵ繧｡繝ｳ繧ｯ繧ｿ縺斐→縺ｫ繧ｰ繝ｫ繝ｼ繝怜�縺代ｒ陦後≧縲�
 * 繧ｰ繝ｫ繝ｼ繝怜�縺代↓縺ｯ讒矩��菴�'atomvec_data'繧堤畑縺�繧九�よｧ矩��菴�'atomvec_data'縺ｯ謨ｴ逅�縺輔ｌ繧九い繝医Β縺ｮ繝輔ぃ繝ｳ繧ｯ繧ｿ縺ｮID(= 0,1,2,...)縺ｨ縲�
 * 縺昴�ｮ繝輔ぃ繝ｳ繧ｯ繧ｿ繧呈戟縺､蜈ｨ繧｢繝医Β縺ｮ繧｢繝峨Ξ繧ｹ繧堤ｮ｡逅�縺吶ｋ繝吶け繧ｿ繝ｼ'atom_ptrs'縺ｨ繧呈ュ蝣ｱ縺ｨ縺励※謖√▽縲�
 * 譛ｬ閹懃峩荳九�ｮ縺吶∋縺ｦ縺ｮ繧｢繝医Β縺ｮ繧｢繝峨Ξ繧ｹ繧偵�√◎繧後◇繧悟ｯｾ蠢懊☆繧区ｧ矩��菴�'atomvec_data'蜀�縺ｫ謨ｴ逅�縺励��
 * 縺薙�ｮ讒矩��菴薙ｒ譛ｬ繝｡繧ｽ繝�繝峨�ｮ謌ｻ繧雁�､縺ｨ縺ｪ繧九�吶け繧ｿ繝ｼ縺ｮ隕∫ｴ�縺ｨ縺励※譬ｼ邏阪＠縺ｦ縺�縺上��
 *
 * 譛ｬ蜷悟梛諤ｧ蛻､螳壹�ｮ繧｢繝ｫ繧ｴ繝ｪ繧ｺ繝�縺ｧ縺ｯ縲√�悟ｰ第焚豢ｾ縺ｮ繧｢繝医Β縲阪°繧峨�槭ャ繝√Φ繧ｰ繧帝幕蟋九〒縺阪ｋ繧医≧縺ｫ縺吶ｋ縺薙→繧�
 * 逶ｮ讓吶→縺励※縺�繧九◆繧√�∝�育ｨ区ｧ矩��菴薙ｒ謨ｴ逅�縺励◆繝吶け繧ｿ繝ｼ縺ｫ縺､縺�縺ｦ縲∵ｧ矩��菴灘��縺ｮ繧｢繝医Β謨ｰ縺後�悟､壹＞鬆�縲阪↓繧ｽ繝ｼ繝医＠縺ｦ繧�繧句ｿ�隕√′縺ゅｋ縲�
 * 縺薙％縺ｧ縲悟､壹＞鬆�縲阪→縺励◆逅�逕ｱ縺ｯ縲∝ｾ後�ｮ繧ｹ繝�繝�繝励〒譛ｬ繝吶け繧ｿ繝ｼ繧�(邨先棡逧�縺ｫ)POP縺励↑縺後ｉ繧｢繝医Β縺ｮ繧｢繝峨Ξ繧ｹ諠�蝣ｱ繧貞叙繧雁�ｺ縺吶％縺ｨ縺ｫ
 * 縺ｪ繧九◆繧√〒縺ゅｋ縲�(蟆第焚豢ｾ縺ｮ繧｢繝医Β縺九ｉ蜿悶ｊ蜃ｺ縺輔ｌ繧九％縺ｨ縺ｫ縺ｪ繧九％縺ｨ縺ｫ豕ｨ諢上○繧�) */
static Vector *mem_mk_matching_vec(LmnMembrane *mem)
{
  Vector *vec, *v_tmp;
  LmnFunctor f;
  LmnSAtom a;
  AtomListEntry *ent;
  unsigned int anum_max; /* 閹懷��縺ｫ蟄伜惠縺吶ｋ繧｢繝医Β繧偵ヵ繧｡繝ｳ繧ｯ繧ｿ豈弱↓繧ｰ繝ｫ繝ｼ繝怜喧縺励◆髫帙�ｮ縲�髮�蜷医�ｮ螟ｧ縺阪＆縺ｮ譛�螟ｧ蛟､ */
  unsigned int i, j;

  vec = vec_make(1);
  memset(vec->tbl, 0, sizeof(atomvec_data *) * vec->cap);
  anum_max = 0;

  EACH_ATOMLIST_WITH_FUNC(mem, ent, f, ({
    atomvec_data *ad;

    ad = LMN_MALLOC(atomvec_data);
    ad->fid = f;
    ad->atom_ptrs = vec_make(1);

    /* 譛ｬ閹懃峩荳九�ｮ繧｢繝医Β縺ｮ蜀�縲√ヵ繧｡繝ｳ繧ｯ繧ｿ縺掲縺ｧ縺ゅｋ繧ゅ�ｮ縺ｮ繧｢繝峨Ξ繧ｹ繧偵�吶け繧ｿ繝ｼatom_ptrs蜀�縺ｫ謨ｴ逅�縺吶ｋ縲�
     * 蠕後〒繧ｽ繝ｼ繝医☆繧矩未菫ゅ〒縲∵怙繧ょ､壹￥縺ｮ繧｢繝医Β縺ｮ繧｢繝峨Ξ繧ｹ繧堤ｮ｡逅�縺吶ｋ讒矩��菴�(atomvec_data)蜀�縺ｮ繧｢繝医Β謨ｰ繧呈ｱゅａ縺ｦ縺�繧九�� */
    EACH_ATOM(a, ent, ({
      vec_push(ad->atom_ptrs, (LmnWord)a);
      if (vec_num(ad->atom_ptrs) > anum_max) {
        anum_max = vec_num(ad->atom_ptrs);
      }
    }));
    /* 繝輔ぃ繝ｳ繧ｯ繧ｿf繧呈戟縺､繧｢繝医Β縺梧悽閹懷��縺ｫ1縺､繧ょｭ伜惠縺励↑縺�蝣ｴ蜷医�√％縺ｮ繝輔ぃ繝ｳ繧ｯ繧ｿ縺ｮ縺溘ａ縺ｫ蜑ｲ縺�縺溘Γ繝｢繝ｪ繝ｼ縺ｮ鬆伜沺繧定ｧ｣謾ｾ縺吶ｋ縲�
     * 縺薙ｌ繧呈��繧九→繝｡繝｢繝ｪ繝ｪ繝ｼ繧ｯ縺瑚ｵｷ縺薙ｋ縺ｮ縺ｧ豕ｨ諢�!! */
    if (vec_is_empty(ad->atom_ptrs)) {
      vec_free(ad->atom_ptrs);
      LMN_FREE(ad);
    }
    else {
      vec_push(vec, (vec_data_t)ad);
    }
  }));

  /* sort */
  if (anum_max > 0) {
    v_tmp = vec_make(vec_num(vec));

    for (i = 1; i <= anum_max; i++) {
      for (j = 0; j < vec_num(vec); j++) {
        atomvec_data *ad = (atomvec_data *)vec_get(vec, j);
        if (vec_num(ad->atom_ptrs) == i) {
          vec_push(v_tmp, (vec_data_t)ad);
        }
      }
    }
    vec_clear(vec);
    /* 讒矩��菴灘��縺ｮ繧｢繝医Β謨ｰ縺後�悟､壹＞鬆�縲阪↓繧ｽ繝ｼ繝� */
    while (!vec_is_empty(v_tmp)) {
      vec_push(vec, vec_pop(v_tmp));
    }
    vec_free(v_tmp);
  }

  return vec;
}


/* 譛ｪ菴ｿ逕ｨ繧ｳ繝ｼ繝�:
 * mem_mk_matching_vec/1 縺ｮ謌ｻ繧雁�､縺ｧ縺ゅｋ2縺､縺ｮVector縺ｮ豈碑ｼ�繧定｡後≧
 * 2縺､縺ｮ閹懷��縺ｫ蟄伜惠縺吶ｋ繝励Ο繧ｻ繧ｹ縺ｮ遞ｮ鬘槭♀繧医�ｳ蛟区焚縺悟ｮ悟�ｨ縺ｫ荳�閾ｴ縺吶ｋ縺ｪ繧峨�ｰTRUE繧定ｿ斐☆ */
BOOL mem_is_the_same_matching_vec(Vector *vec1, Vector *vec2)
{
  unsigned int i, j;

  for (i = 0; i < vec_num(vec1); i++) {
    BOOL is_the_same_functor;

    is_the_same_functor = FALSE;
    for (j = 0; j < vec_num(vec2); j++) {
      if (((atomvec_data *)vec_get(vec1, i))->fid ==
           ((atomvec_data *)vec_get(vec2, j))->fid) {
        is_the_same_functor = TRUE;
        break;
      }
    }

    if (!is_the_same_functor ||
        vec_num(((atomvec_data *)vec_get(vec1, i))->atom_ptrs) !=
        vec_num(((atomvec_data *)vec_get(vec2, j))->atom_ptrs)) {
      return FALSE;
    }
  }

  return TRUE;
}


